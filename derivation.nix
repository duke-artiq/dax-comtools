{ callPackage
, buildPythonPackage
, nix-gitignore
, lib
, numpy
, influxdb
, pyserial
, pyvisa
, pyvisa-py
, sipyco
, pymoku ? callPackage ./vendor/pymoku/default.nix { }
, pytestCheckHook
, version ? "0.0.0"
}:

buildPythonPackage {
  pname = "dax-comtools";
  inherit version;
  src = nix-gitignore.gitignoreSource [ "*.nix" "/*.lock" ] ./.;

  propagatedBuildInputs = [
    numpy
    influxdb
    pyserial
    pyvisa
    pyvisa-py
    sipyco
    pymoku
  ];

  checkInputs = [
    pytestCheckHook
  ];

  condaDependencies = [
    "python>=3.7"
    "numpy"
    "influxdb"
    "pyserial"
    "pyvisa"
    "pyvisa-py"
    "sipyco"
    "pymoku" # Not available in conda-forge
  ];

  meta = with lib; {
    description = "Lightweight DAX communication tools";
    maintainers = [ "Duke University" ];
    homepage = "https://gitlab.com/duke-artiq/dax-comtools";
    license = licenses.mit;
  };
}
