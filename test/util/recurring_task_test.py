import unittest
import time

import dax_comtools.util.recurring_task


class _Task(dax_comtools.util.recurring_task.RecurringTaskBase):
    """A simple implementation of a task."""

    def __init__(self, *args, task_delay=0.0, **kwargs):
        super(_Task, self).__init__(*args, **kwargs)
        assert isinstance(task_delay, float)
        assert task_delay >= 0.0
        self._task_delay = task_delay
        self.count = 0

    def task(self):
        self.count += 1
        time.sleep(self._task_delay)

    def start(self):
        self.count = 0
        super(_Task, self).start()


class RecurringTaskTestCase(unittest.TestCase):
    def test_start_stop(self):
        interval = 0.1
        t = _Task(interval)

        self.assertTrue(t.is_running())
        t.start()  # Does nothing
        self.assertTrue(t.is_running())

        t.stop()
        self.assertFalse(t.is_running())
        t.stop()  # Does nothing
        self.assertFalse(t.is_running())

        for _ in range(10):
            t.start()
            self.assertTrue(t.is_running())
            t.stop()
            self.assertFalse(t.is_running())

    def test_manual_start(self):
        interval = 0.1
        t = _Task(interval, start=False)

        self.assertFalse(t.is_running())
        t.start()
        self.assertTrue(t.is_running())
        t.stop()
        self.assertFalse(t.is_running())

    def test_interval(self):
        interval = 0.1
        t = _Task(interval, start=False)
        self.assertEqual(interval, t.get_interval())

        for interval in [2.0, 3.0, 4.0]:
            t.set_interval(interval)
            self.assertEqual(interval, t.get_interval())

    def test_daemon(self):
        interval = 0.1
        daemon = True
        t = _Task(interval, start=False, daemon=daemon)
        self.assertEqual(daemon, t.get_daemon())

        for daemon in [True, False, True]:
            t.set_daemon(daemon)
            self.assertEqual(daemon, t.get_daemon())

    def test_run(self):
        interval = 0.2
        t = _Task(interval, start=False)

        for periods in [1, 2, 3]:
            t.start()
            time.sleep(interval * (periods + 0.5))
            t.stop()
            self.assertEqual(t.count, periods)

    def test_skewed_run(self):
        interval = 0.2
        t = _Task(interval, start=False, task_delay=interval * 1.5)

        for periods in [2, 3, 4]:
            t.start()
            time.sleep(interval * (periods + 0.5))
            t.stop()
            self.assertEqual(t.count, round(periods / 2))
