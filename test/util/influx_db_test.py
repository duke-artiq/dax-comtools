import unittest
from unittest.mock import create_autospec, call, ANY
import typing
import datetime
import influxdb

import dax_comtools.util.influx_db


class _NoClearList(list):
    """A list that does not clear."""

    def clear(self) -> None:
        # Do not clear this list
        pass


class _InfluxDBUtil(dax_comtools.util.influx_db.InfluxDBUtil):
    """Mock InfluxDB util class that does not connect"""

    def _connect(self, host: str, port: int, database: str, username: str, password: str,
                 timeout: typing.Optional[float], **kwargs: typing.Any) -> None:
        # Install a mock client
        self._client = create_autospec(spec=influxdb.InfluxDBClient)(
            host=host, port=port, database=database,
            username=username, password=password, timeout=timeout, **kwargs
        )
        self.init_kwargs = kwargs

    @property
    def client(self):
        # Expose the client for testing
        return self._client

    def flush(self) -> None:
        # Substitute buffer with a no clear buffer (required for testing a mutable object)
        self._buffer = _NoClearList(self._buffer)
        super(_InfluxDBUtil, self).flush()
        # Replace with an empty buffer
        self._buffer = []


class InfluxDBUtilTestCase(unittest.TestCase):
    _TAGS = {
        'source': 'temp_sensor',
        'device_type': 'rpiv3b',
        'building': 'tower of pisa',
        'room': 'level',
    }
    """Some standard tags to use."""
    _FIELDS = [
        {'foo0': 10, 'bar0': '0baz', },
        {'foo1': 11, 'bar1': '1baz', },
        {'foo2': 12, 'bar2': '2baz', },
        {'foo3': 13, 'bar3': '3baz', },
        {'foo4': 14, 'bar4': '4baz', 'foobar': 9.0},
    ]
    """A list of standard fields to use."""

    def test_iso_format(self):
        d = datetime.datetime.utcnow()
        self.assertEqual(d.isoformat(), d.isoformat('T'))

    def test_init(self):
        kwargs = {'gzip': True}
        influx = _InfluxDBUtil(**kwargs)
        self.assertDictEqual(influx.init_kwargs, kwargs)
        self.assertListEqual(influx.client.method_calls, [call.ping()])

        # Flushing will do nothing
        influx.client.reset_mock()
        influx.flush()
        self.assertFalse(influx.client.called)

    def test_time_precision(self):
        for time_precision in ['s', 'u', 'ms', 'm']:
            influx = _InfluxDBUtil(time_precision=time_precision)
            influx.client.reset_mock()
            influx.write_point_monitoring(**self._TAGS, **self._FIELDS[0])
            calls = [
                call.write_points(ANY, time_precision=time_precision)
            ]
            self.assertListEqual(influx.client.method_calls, calls)

    def test_write_point(self):
        influx = _InfluxDBUtil()

        for f in self._FIELDS:
            influx.client.reset_mock()
            influx.write_point_monitoring(**self._TAGS, **f)

            # Buffer is empty since there is no buffering
            self.assertEqual(len(influx._buffer), 0)

            # Test if the write_points() call was correct
            point = {
                'measurement': 'monitoring',
                'tags': self._TAGS,
                'fields': f,
            }
            calls = [
                call.write_points([point], time_precision=None)
            ]
            self.assertListEqual(influx.client.method_calls, calls)

        # Flushing will do nothing
        influx.client.reset_mock()
        influx.flush()
        self.assertFalse(influx.client.called)

    def test_write_point_fields(self):
        influx = _InfluxDBUtil()

        for f in self._FIELDS:
            influx.client.reset_mock()
            influx.write_point_monitoring(**self._TAGS, fields=f)

            # Buffer is empty since there is no buffering
            self.assertEqual(len(influx._buffer), 0)

            # Test if the write_points() call was correct
            point = {
                'measurement': 'monitoring',
                'tags': self._TAGS,
                'fields': f,
            }
            calls = [
                call.write_points([point], time_precision=None)
            ]
            self.assertListEqual(influx.client.method_calls, calls)

        # Flushing will do nothing
        influx.client.reset_mock()
        influx.flush()
        self.assertFalse(influx.client.called)

    def test_write_point_timestamp(self):
        influx = _InfluxDBUtil()

        for f in self._FIELDS:
            influx.client.reset_mock()
            timestamp = datetime.datetime.utcnow().isoformat()
            influx.write_point_monitoring(**self._TAGS, **f, timestamp=timestamp)

            # Buffer is empty since there is no buffering
            self.assertEqual(len(influx._buffer), 0)

            # Test if the write_points() call was correct
            point = {
                'measurement': 'monitoring',
                'time': timestamp,
                'tags': self._TAGS,
                'fields': f,
            }
            calls = [
                call.write_points([point], time_precision=None)
            ]
            self.assertListEqual(influx.client.method_calls, calls)

        # Flushing will do nothing
        influx.client.reset_mock()
        influx.flush()
        self.assertFalse(influx.client.called)

    def test_write_point_buffered_timestamp(self):
        buffer = []
        influx = _InfluxDBUtil(buffer_size=len(self._FIELDS))

        for f in self._FIELDS:
            influx.client.reset_mock()
            influx.write_point_monitoring(**self._TAGS, **f, timestamp=None)

            # Buffer point
            point = {
                'measurement': 'monitoring',
                'time': ANY,  # Include a time, which will be automatically added because of buffering
                'tags': self._TAGS,
                'fields': f,
            }
            buffer.append(point)

        # Verify all calls, which include a timestamp
        calls = [
            call.write_points(buffer, time_precision=None)
        ]
        self.assertListEqual(influx.client.method_calls, calls)

        # Flushing will do nothing
        influx.client.reset_mock()
        influx.flush()
        self.assertFalse(influx.client.called)

    def _test_write_point_buffered(self, *, buffer_size, time_precision=None):
        buffer = []
        influx = _InfluxDBUtil(buffer_size=buffer_size, time_precision=time_precision)

        for i, f in enumerate(self._FIELDS):
            influx.client.reset_mock()
            influx.write_point_monitoring(**self._TAGS, **f)

            # Check buffer fill
            self.assertEqual(len(influx._buffer), (1 + i) % buffer_size)

            point = {
                'measurement': 'monitoring',
                'tags': self._TAGS,
                'fields': f,
            }
            if buffer_size > 1:
                # With buffering enabled, a time will be inserted
                point['time'] = ANY
            buffer.append(point)

            if (i % buffer_size) == buffer_size - 1:
                # Test if the write_points() call was correct
                calls = [
                    call.write_points(buffer, time_precision=time_precision)
                ]
                self.assertListEqual(influx.client.method_calls, calls)
                buffer.clear()
            else:
                # Buffer not full, client is not called
                self.assertFalse(influx.client.called)

        # Flush
        influx.client.reset_mock()
        influx.flush()
        if buffer:
            # Test if the write_points() call was correct
            calls = [
                call.write_points(buffer, time_precision=time_precision)
            ]
            self.assertListEqual(influx.client.method_calls, calls)
            buffer.clear()
        else:
            self.assertFalse(influx.client.called)

    def test_write_point_buffered(self):
        for i in range(1, len(self._FIELDS) + 3):
            with self.subTest(buffer_size=i):
                self._test_write_point_buffered(buffer_size=i)

    def test_flush(self):
        influx = _InfluxDBUtil(buffer_size=2)
        influx.client.reset_mock()
        influx.write_point_monitoring(**self._TAGS, **self._FIELDS[0])

        # Client was not called yet
        self.assertFalse(influx.client.called)

        # Write will happen after flushing
        influx.flush()
        calls = [
            call.write_points(ANY, time_precision=None),
        ]
        self.assertListEqual(influx.client.method_calls, calls)

    def test_close(self):
        influx = _InfluxDBUtil(buffer_size=2)
        influx.client.reset_mock()
        influx.write_point_monitoring(**self._TAGS, **self._FIELDS[0])

        # Client was not called yet
        self.assertFalse(influx.client.called)

        # Write will happen after closing
        influx.close()
        calls = [
            call.write_points(ANY, time_precision=None),
            call.close(),
        ]
        self.assertListEqual(influx.client.method_calls, calls)

    def test_close_context(self):
        with _InfluxDBUtil(buffer_size=2) as influx:
            influx.client.reset_mock()

            # Write point
            influx.write_point_monitoring(**self._TAGS, **self._FIELDS[0])

            # Client was not called yet
            self.assertFalse(influx.client.called)
            # Write will happen after closing

        calls = [
            call.write_points(ANY, time_precision=None),
            call.close(),
        ]
        self.assertListEqual(influx.client.method_calls, calls)
