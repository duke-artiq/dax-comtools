{ pkgs ? import <nixpkgs> { }
, artiqpkgs ? import <artiq-full> { inherit pkgs; }
, pymoku ? pkgs.python3Packages.callPackage ../vendor/pymoku/default.nix { }
}:

let
  dax-comtools = pkgs.python3Packages.callPackage ../derivation.nix { inherit pymoku; inherit (artiqpkgs) sipyco; };
in
pkgs.mkShell {
  buildInputs = [
    (pkgs.python3.withPackages (ps: [
      # Packages required for testing
      ps.pytest
      ps.mypy
      ps.coverage
      ps.flake8
    ] ++ dax-comtools.propagatedBuildInputs))
  ];
}
