import unittest

from dax_comtools.devices.quantel_falcon.driver import QuantelFalconDriver


class QuantelFalconTestCase(unittest.TestCase):

    def setUp(self) -> None:
        self.driver = QuantelFalconDriver("/dev/null", simulation=True)

    def test_set(self):
        # Test sets
        self.driver.set_current(1)
        self.driver.set_pulse_freq(2)
        self.driver.set_burst_triggers(3)
        self.driver.set_trigger_mode(1)
        self.driver.set_all_parameters(49, 10, 0, 0)

    def test_query(self):
        # Test queries
        self.driver.query_current()
        self.driver.query_pulse_freq()
        self.driver.query_burst_triggers()
        self.driver.query_trigger_mode()
        self.driver.query_max_power()
        self.driver.query_all_parameters()

    def test_assert(self):
        # Test assertions
        with self.assertRaises(AssertionError):
            self.driver.set_burst_triggers(5000)
        with self.assertRaises(AssertionError):
            self.driver.set_trigger_mode(2)
        with self.assertRaises(AssertionError):
            self.driver.set_current(0)
        with self.assertRaises(AssertionError):
            self.driver.set_current(55)

    def test_update_max_current(self):
        # Update max current
        self.driver.set_max_current(55)

        # Verify no failure
        self.driver.set_current(55)
        # Verify failure for new value
        with self.assertRaises(AssertionError):
            self.driver.set_current(56)
