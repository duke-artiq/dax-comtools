import unittest
from dax_comtools.devices.influx_db.driver import InfluxDBDriver


class SimTestCase(unittest.TestCase):
    def test_output(self):
        message1 = "test"
        message2 = [{"measurement": "test", "tags": {"tag1": "test", "tag2": "test"},
                     "fields": {"field1": 1, "field2": "test"}}]
        # noinspection PyTypeChecker
        driver = InfluxDBDriver(None, None, None, None, None, True)

        with self.assertLogs(level="INFO") as cm:
            driver.write_points(message1)
            driver.write_points(message2)
        self.assertEqual(cm.records[0].getMessage(), message1)
        self.assertEqual(cm.records[1].getMessage(), str(message2))


class RealTestCase(unittest.TestCase):
    def test_bad_connection_params(self):
        # first exception raised is a regular ConnectionError, but on retry the exceptions raised are defined in urllib3
        # can't specifically catch those without adding an unnecessary dependency
        with self.assertRaises(Exception):
            InfluxDBDriver("dax_data_store", "localhost", 8087, "root", "root", False)
