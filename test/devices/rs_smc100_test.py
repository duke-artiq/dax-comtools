import unittest
import unittest.mock

from dax_comtools.devices.rs_smc100.driver import SMC100, SimSMC100

_ADDR = '192.168.1.1'


class SMC100TestCase(unittest.TestCase):

    def test_set_limit(self):
        with self.assertRaises(TypeError, msg='Using both Limit level and limit power did not raise'):
            SMC100(_ADDR, limit_level=0.0, limit_power=0.0)

        for limit, offset in [(20.0, 0.0), (17.1, 0.0), (17.0, -0.1), (-120.1, 0.0), (-130.0, 0.0), (-120.0, 0.1)]:
            with self.assertRaises(ValueError, msg='Out of range power limit level did not raise'):
                SimSMC100(_ADDR, limit_level=limit, offset=offset)
        for limit, offset in [(17.0, 0.0), (0.0, 0.0), (-80.0, 0.0), (18.0, 1.0)]:
            SimSMC100(_ADDR, limit_level=limit, offset=offset)

        for limit, offset in [(20.0, 0.0), (17.1, 0.0), (17.1, 0.1), (-120.1, 0.0), (-130.0, 0.0)]:
            with self.assertRaises(ValueError, msg='Out of range power limit did not raise'):
                SimSMC100(_ADDR, limit_power=limit, offset=offset)
        for limit, offset in [(17.0, 0.0), (0.0, 0.0), (-80.0, 0.0), (17.0, -1.0)]:
            SimSMC100(_ADDR, limit_power=limit, offset=offset)

    def test_set_cutoff(self):
        for cutoff in [20.0, 17.1, -120.1, -130.0]:
            with self.assertRaises(ValueError, msg='Out of range cutoff did not raise'):
                SimSMC100(_ADDR, cutoff=cutoff)
        for cutoff in [17.0, 0.0, -80.0]:
            SimSMC100(_ADDR, cutoff=cutoff)

    def test_limit_cutoff(self):
        for cutoff, limit in [(0.0, -1.0), (-70.0, -100.0)]:
            with self.assertRaises(ValueError, msg='Cutoff larger than limit did not raise'):
                SimSMC100(_ADDR, cutoff=cutoff, limit_level=limit)

    def test_set_power(self):
        driver = SimSMC100(_ADDR, cutoff=-80.0, limit_level=0.0)

        for level in [20.0, 17.1, -120.1, -130.0, 1.0, 0.1, -80.1, -100.0]:
            with self.assertRaises(ValueError, msg='Out of range level did not raise'):
                driver.set_power(level)

        for level in [0.0, -0.1, -70.0, -79.99, -80.0]:
            driver.set_power(level)
            self.assertEqual(level, driver.get_power())

    def test_set_power_min_step(self):
        driver = SimSMC100(_ADDR)

        for level in [0.003, 0.004, -0.003, -0.004]:
            driver.set_power(0.0)
            with unittest.mock.patch.object(driver, '_wait_for_ramp') as wait_for_ramp:
                driver.set_power(level)
                self.assertFalse(wait_for_ramp.called)

        for i, level in enumerate([0.005, 0.006, -0.005, -0.006]):
            driver.set_power(0.0)
            with unittest.mock.patch.object(driver, '_wait_for_ramp') as wait_for_ramp:
                driver.set_power(level)
                self.assertEqual(wait_for_ramp.call_count, 1)

    def test_wait_for_ramp(self):
        class TestDriver(SimSMC100):
            READ = '0'

            def _read(self) -> str:
                return self.READ

        driver = TestDriver(_ADDR)
        for ramp_time in [0.1, 0.0]:
            SMC100._wait_for_ramp(driver, ramp_time)

        class TestDriverRunning(TestDriver):
            READ = '1'

        driver = TestDriverRunning(_ADDR)
        for ramp_time in [0.1, 0.0]:
            with self.assertRaises(RuntimeError, msg='Infinite wait for ramp did not raise'):
                SMC100._wait_for_ramp(driver, ramp_time)

    def test_rf_on(self):
        cutoff = -80.0
        driver = SimSMC100(_ADDR, cutoff=cutoff)

        driver.rf_off()
        self.assertEqual(driver.get_power(), cutoff)
        self.assertFalse(driver.is_rf_on())

        driver.set_power(0.0)
        self.assertEqual(driver.get_power(), 0.0)
        self.assertFalse(driver.is_rf_on())

        driver.rf_on()
        self.assertEqual(driver.get_power(), 0.0)
        self.assertTrue(driver.is_rf_on())

    def test_rf_off(self):
        cutoff = -80.0
        driver = SimSMC100(_ADDR, cutoff=cutoff)

        driver.rf_off()
        self.assertEqual(driver.get_power(), cutoff)
        self.assertFalse(driver.is_rf_on())

        driver.rf_on()
        self.assertEqual(driver.get_power(), cutoff)
        self.assertTrue(driver.is_rf_on())

        driver.set_power(0.0)
        self.assertEqual(driver.get_power(), 0.0)
        self.assertTrue(driver.is_rf_on())

        driver.rf_off()
        self.assertEqual(driver.get_power(), cutoff)
        self.assertFalse(driver.is_rf_on())

    def test_set_frequency(self):
        driver = SimSMC100(_ADDR)

        # Switch RF off
        driver.rf_off()

        for freq in [8e3, 9e3 - 1, 3.2e9 + 1, 3.3e9]:
            with self.assertRaises(ValueError, msg='Out of range frequency did not raise'):
                driver.set_freq(freq)

        for freq in [9e3, 10e3, 3.1e9, 3.2e9]:
            driver.set_freq(freq)
            self.assertEqual(freq, driver.get_freq())

    def test_set_frequency_step_limit(self):
        driver = SimSMC100(_ADDR, freq_step_limit=5e3)
        base_freq = 50e3

        def reset_freq():
            # Switch RF off, set base frequency, enable RF
            driver.rf_off()
            driver.set_freq(base_freq)
            driver.rf_on()

        for diff in [-5e3, -2e3, 0, 3e3, 5e3]:
            reset_freq()
            driver.set_freq(base_freq + diff)

        for diff in [-10e3, -5000.1, 5000.001, 11e3]:
            reset_freq()
            with self.assertRaises(RuntimeError, msg='Frequency step exceeding limit did not raise'):
                driver.set_freq(base_freq + diff)

            # Works with RF off
            driver.rf_off()
            driver.set_freq(base_freq + diff)

    def test_offset(self):
        for offset in [-100.0, -50.0, 0.1, 99.0]:
            driver = SimSMC100(_ADDR, offset=offset, cutoff=SimSMC100.DEFAULT_CUTOFF + offset)
            self.assertEqual(driver.get_offset(), offset)

    def test_set_offset(self):
        driver = SimSMC100(_ADDR)
        driver.rf_off()
        for offset in [-100.1, -150.0, 101.0]:
            with self.assertRaises(ValueError, msg='Out of range power offset did not raise'):
                driver._set_offset(offset)

        valid_offsets = [-100.0, -50.0, 0.1, 99.0, 100.0]
        for offset in valid_offsets:
            driver = SimSMC100(_ADDR)
            driver.rf_off()
            driver._set_offset(offset)
            self.assertEqual(offset, driver.get_offset())

        for offset in valid_offsets:
            driver = SimSMC100(_ADDR, offset=offset,
                               cutoff=SimSMC100.DEFAULT_CUTOFF + offset, limit_power=SimSMC100.DEFAULT_LIMIT)
            driver.rf_off()
            driver._set_offset(offset)
            driver.rf_on()
            driver._set_offset(offset)  # Should not raise because the offset did not change

        # Switch RF on
        driver.rf_on()
        for offset in valid_offsets[:-1]:
            with self.assertRaises(RuntimeError, msg='Changing offset while rf is on did not raise'):
                driver._set_offset(offset)

    def test_ref_osc_src(self):
        for src in SimSMC100.REF_OSC_SRC:
            driver = SimSMC100(_ADDR, ref_osc_src=src)
            self.assertEqual(driver.get_ref_osc(), src)

    def test_set_ref_osc_src(self):
        driver = SimSMC100(_ADDR)

        driver.rf_off()
        for src in ['intt', 'ect', 'foo', 'bar', 'INT', 'EXT']:
            with self.assertRaises(ValueError):
                driver._set_ref_osc(src)
        for src in SimSMC100.REF_OSC_SRC:
            driver._set_ref_osc(src)
            self.assertEqual(driver.get_ref_osc(), src)

        for src in SimSMC100.REF_OSC_SRC:
            driver.rf_off()
            driver._set_ref_osc(src)
            driver.rf_on()
            for src2 in SimSMC100.REF_OSC_SRC:
                if src != src2:
                    with self.assertRaises(RuntimeError):
                        driver._set_ref_osc(src2)
                    self.assertEqual(driver.get_ref_osc(), src)
                else:
                    driver._set_ref_osc(src2)
                    self.assertEqual(driver.get_ref_osc(), src2)
