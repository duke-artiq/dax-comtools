import unittest

from dax_comtools.devices.agilent_psu.driver import AgilentPowerSupplyDriver


class test_valid_AgilentPowerSupplyDriver(unittest.TestCase):
    """Test cases for vaild usage of LEGACY AgilentPowerSupplyDriver class"""

    # Simulation test case for Legacy Agilent PSU driver
    def test_simulation(self):
        driver = AgilentPowerSupplyDriver(
            "ASRL/dev/null",
            baud_rate=max(AgilentPowerSupplyDriver.VALID_BAUD_RATES),
            bypass_remote_mode=True,
            simulation=True,
        )
        self.assertEqual(driver.get_device_model(), "sim_psu")


class test_invalid_AgilentPowerSupplyDriver(unittest.TestCase):
    """Test cases for invaild usage of LEGACY AgilentPowerSupplyDriver class"""

    # Simulation test case for invalid baud rate
    def test_invalid_baud_rate(self):
        with self.assertRaises(ValueError) as context:
            AgilentPowerSupplyDriver(
                "ASRL/dev/null",
                baud_rate=9599,
                bypass_remote_mode=True,
                simulation=True,
            )
        self.assertTrue(
            f"Invalid baud rate, valid options are {AgilentPowerSupplyDriver.VALID_BAUD_RATES}"
            in str(context.exception)
        )

    # Simulation test case for invalid model number
    def test_invalid_model(self):
        with self.assertRaises(ValueError) as context:
            AgilentPowerSupplyDriver(
                "ASRL/dev/null",
                model="e36300",
                bypass_remote_mode=True,
                simulation=True,
            )
        self.assertTrue(
            f"Invalid model number, valid options are {AgilentPowerSupplyDriver.VALID_MODELS}."
            "For support of newer models please use the updated KeysightPowerSupplyDriver."
            in str(context.exception)
        )


if __name__ == "__main__":
    unittest.main()
