import unittest
from time import sleep

from dax_comtools.devices.button_presser.driver import ButtonPresserDriverV2, ButtonPresserDriver


class ButtonPresserTestCase(unittest.TestCase):

    def setUp(self) -> None:
        self.driver = ButtonPresserDriver(device="COM1", simulation=True, recurring_update=True)

    def test_set(self):
        self.driver.on()
        self.driver.off()
        self.assertEqual(self.driver._sim_output_stack, ["S", "E"])


class ButtonPresser2RecurringTestCase(unittest.TestCase):
    interval = 1

    def setUp(self) -> None:
        self.driver = ButtonPresserDriverV2(device="COM1", simulation=True,
                                            recurring_update=True, write_interval=self.interval)

    def test_set(self):
        self.assertEqual(self.driver.get_state(), "F")
        self.driver.on()
        self.assertEqual(self.driver.get_state(), "N")
        self.assertEqual(self.driver.get_state_msg(), "On")
        self.driver.off()
        self.assertEqual(self.driver.get_state(), "F")
        self.assertEqual(self.driver.get_state_msg(), "Off")

    def test_recurring_set(self):
        # Clear sim output stack
        self.driver.off()
        self.driver._sim_output_stack = []

        self.driver.on()
        # In recurring mode, when "on", the driver should send out an "N" message every `interval` duration
        # Delay long enough that a single output should have been sent and verify
        sleep(1.5*self.interval)
        self.driver.off()
        self.assertEqual(self.driver._sim_output_stack[0], "N")

    def test_ping(self):
        self.assertEqual(self.driver.ping(), True)


class ButtonPresser2NonRecurringTestCase(unittest.TestCase):

    def setUp(self) -> None:
        self.driver = ButtonPresserDriverV2(device="COM1", simulation=True, recurring_update=False)

    def test_set(self):
        self.assertEqual(self.driver.get_state(), "F")
        self.driver.on()
        self.assertEqual(self.driver.get_state(), "N")
        self.driver.off()
        self.assertEqual(self.driver.get_state(), "F")

    def test_on_off(self):
        # Clear sim output stack
        self.driver.off()
        self.driver._sim_output_stack = []

        self.driver.on()
        # Delay long enough to send an output
        self.driver.off()
        self.assertEqual(self.driver._sim_output_stack, ["N", "F"])
