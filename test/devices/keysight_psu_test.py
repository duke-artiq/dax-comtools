import unittest

from dax_comtools.devices.keysight_psu.driver import KeysightPowerSupplyDriver


class test_valid_KeysightPowerSupplyDriver(unittest.TestCase):
    """Test cases for vaild usage of KeysightPowerSupplyDriver class"""

    # Simulation test serial connection (ex. RS-232)
    def test_asrl_simulation(self):
        driver = KeysightPowerSupplyDriver(
            "ASRL/dev/null",
            bypass_remote_mode=True,
            simulation=True,
            baud_rate=max(KeysightPowerSupplyDriver.VALID_BAUD_RATES),
        )
        self.assertEqual(driver.get_device_model(), "sim_psu")

    # Simulation test serial GPIB connection
    def test_gpib_simulation(self):
        driver = KeysightPowerSupplyDriver(
            "GPIB::INTFC",
            bypass_remote_mode=True,
            simulation=True,
        )
        self.assertEqual(driver.get_device_model(), "sim_psu")

    # Simulation test TCPIP ethernet connection
    def test_tcpip_simulation(self):
        driver = KeysightPowerSupplyDriver(
            "TCPIP::100.100.1.100::1000::SOCKET",
            bypass_remote_mode=True,
            simulation=True,
        )
        self.assertEqual(driver.get_device_model(), "sim_psu")

    # Simulation test USB connection
    def test_usb_simulation(self):
        driver = KeysightPowerSupplyDriver(
            "USB::0x1234::125::A22-5::INSTR",
            bypass_remote_mode=True,
            simulation=True,
        )
        self.assertEqual(driver.get_device_model(), "sim_psu")


class test_invalid_KeysightPowerSupplyDriver(unittest.TestCase):
    """Test cases for invaild usage of KeysightPowerSupplyDriver class"""

    # Simulation test case for invalid baud rate
    def test_invalid_baud_rate(self):
        with self.assertRaises(ValueError) as context:
            KeysightPowerSupplyDriver(
                "ASRL/dev/null",
                baud_rate=9601,
                bypass_remote_mode=True,
                simulation=True,
            )
        self.assertTrue(
            f"Invalid baud rate, valid options are {KeysightPowerSupplyDriver.VALID_BAUD_RATES}"
            in str(context.exception)
        )

    # Simulation test case for invalid model number
    def test_invalid_model(self):
        with self.assertRaises(ValueError) as context:
            KeysightPowerSupplyDriver(
                "ASRL/dev/null",
                model="e36300",
                bypass_remote_mode=True,
                simulation=True,
            )
        self.assertTrue(
            f"Invalid model number, valid options are {KeysightPowerSupplyDriver.VALID_MODELS}"
            in str(context.exception)
        )


if __name__ == "__main__":
    unittest.main()
