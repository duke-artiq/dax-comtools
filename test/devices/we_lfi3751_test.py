import unittest

from dax_comtools.devices.we_lfi3751.driver import LFI3751


class LFI3751TestCase(unittest.TestCase):

    def test_fcs(self):
        self.assertEqual(LFI3751._fcs_packet('!101101+000.000'), '!101101+000.00024'.encode('ascii'))
