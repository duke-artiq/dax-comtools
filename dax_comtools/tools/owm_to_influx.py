#!/usr/bin/env python3

import argparse
import urllib.request
import json
import influxdb

from dax_comtools.util.args import influx_args

# The Open Weather Map URL format using city ID
OWM_URL_FORMAT: str = 'http://api.openweathermap.org/data/2.5/weather?id={city_id:s}&units={units:s}&appid={api_key:s}'


def main() -> None:
    """Retrieve weather data from OWM and write to InfluxDB."""
    # Parse arguments
    args = get_argparser().parse_args()

    # Open the URL and obtain the data
    url: str = OWM_URL_FORMAT.format(city_id=args.city_id, units=args.units, api_key=args.api_key)
    with urllib.request.urlopen(url) as response:
        # Parse the JSON response
        data: dict = json.loads(response.read())

    if data['cod'] != 200:
        # Request failed
        raise ValueError(f'HTTP request failed: {data.get("message", "No error message available")}')

    # Create a point
    point: dict = {
        'measurement': 'weather',
        'tags': {
            'source': 'owm',
            'location': str(data['name']),
            'id': str(data['id']),
        },
        'fields': {str(k): float(v) for k, v in data['main'].items()},
    }

    # Open InfluxDB connection
    db = influxdb.InfluxDBClient(host=args.server_db, port=args.port_db,
                                 username=args.user_db, password=args.password_db,
                                 database=args.db)

    try:
        # Write the data point
        db.write_points([point])
    finally:
        # Close the connection
        db.close()


def get_argparser() -> argparse.ArgumentParser:
    """Return argument parser with necessary arguments for OWM and InfluxDB."""
    # Argument parser
    parser = argparse.ArgumentParser(description='DAX Open Weather Map to InfluxDB tool')

    # Open Weather Map arguments
    owm = parser.add_argument_group('Open Weather Map arguments')
    owm.add_argument('city_id', help='City ID', type=str)
    owm.add_argument('api_key', help='API key', type=str)
    owm.add_argument('--units', help='Units format', type=str,
                     choices=['metric', 'kelvin', 'imperial'], default='metric')

    # InfluxDB arguments
    influx_args(parser)

    # Return parser
    return parser


if __name__ == '__main__':
    main()
