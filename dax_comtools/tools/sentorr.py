#!/usr/bin/env python3

"""
Agilent senTorr ion gauge
"""

import argparse

from dax_comtools.devices.agilent_sentorr.driver import AgilentsenTorrIonGauge
from dax_comtools.util.influx_db import InfluxDBUtil
from dax_comtools.util.args import influx_args, get_influx_args, monitoring_tags_args, get_monitoring_tags_args, \
    logger_args, process_logger_args


def get_argparser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(description="Agilent senTorr Ion Gauge reader for InfluxDB")
    parser.add_argument("device", type=str)
    parser.add_argument("--field-name", type=str, default="pressure",
                        help="Field name for influx DB (default: %(default)s)")
    parser.add_argument("--dry-run", "-n", action="store_true", help="read data but do not write to influx DB")

    logger_args(parser)
    influx_args(parser)
    monitoring_tags_args(parser, device_type="agilent_sentorr", required=True)

    return parser


def main():
    args = get_argparser().parse_args()
    process_logger_args(args)

    if not args.field_name:
        raise ValueError('Field name can not be empty')

    with AgilentsenTorrIonGauge(device=args.device) as sentorr:
        # Get pressure
        pressure = sentorr.get_pressure()

        if not args.dry_run:
            # write data to influx DB
            with InfluxDBUtil(**get_influx_args(args)) as influx:
                influx.write_point_monitoring(**get_monitoring_tags_args(args), fields={args.field_name: pressure})


if __name__ == '__main__':
    main()
