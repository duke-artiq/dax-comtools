#!/usr/bin/env python3

import argparse
import logging

from dax_comtools.devices.flow_sensor.flow_sensor import FlowSensor, FLOW_SENSOR_DEFAULT_DIVISOR, \
    FLOW_SENSOR_DEFAULT_BOUNCETIME, FLOW_SENSOR_DEFAULT_SAMPLE_MS
from dax_comtools.util.influx_db import InfluxDBUtil
from dax_comtools.util.args import influx_args, get_influx_args, monitoring_tags_args, get_monitoring_tags_args, \
    logger_args, process_logger_args

logger = logging.getLogger(__name__)


def get_argparser() -> argparse.ArgumentParser:
    # Argument parser
    parser = argparse.ArgumentParser(description='Flow sensor for InfluxDB')

    # Arguments
    flow_sensor = parser.add_argument_group('Flow Sensor arguments')
    flow_sensor.add_argument('pin', type=int,
                             help='RPi GPIO Pin Number')
    flow_sensor.add_argument('--bouncetime', default=FLOW_SENSOR_DEFAULT_BOUNCETIME, type=int,
                             help='Bounce Time to re-measure (default: %(default)s)')
    flow_sensor.add_argument('--sample-ms', default=FLOW_SENSOR_DEFAULT_SAMPLE_MS, type=int,
                             help='Amount of time sample flow in ms (default: %(default)s)')
    flow_sensor.add_argument('--freq-divisor', default=FLOW_SENSOR_DEFAULT_DIVISOR, type=float,
                             help='Number to divide frequency by to convert to flow (default: %(default)s)')
    parser.add_argument('--dry-run', '-n', action='store_true', help='read data but do not write to influx DB')

    # InfluxDB, monitoring, and logger arguments
    influx_args(parser)
    monitoring_tags_args(parser, device_type='flow_sensor', required=True)
    logger_args(parser)

    # Return parser
    return parser


def main() -> None:
    """Retrieve data from a flow sensor and write results to InfluxDB."""
    # Parse arguments
    args = get_argparser().parse_args()
    process_logger_args(args)  # This initializes logging system log level according to -v/-q args

    # Read data from sensor
    sensor = FlowSensor(args.pin,
                        bouncetime=args.bouncetime,
                        sample_ms=args.sample_ms,
                        freq_divisor=args.freq_divisor)
    flow_rate = sensor.get_flow()
    assert isinstance(flow_rate, float), 'Returned flow rate is not of type float'
    logger.info('Flow rate: %d L/m', flow_rate)

    if not args.dry_run:
        # Write data to influx DB
        with InfluxDBUtil(**get_influx_args(args)) as influx:
            influx.write_point_monitoring(**get_monitoring_tags_args(args), flow_rate=flow_rate)


if __name__ == '__main__':
    main()
