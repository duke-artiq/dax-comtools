#!/usr/bin/env python3

"""
Granville Phillips 500 Series CCG Frontend
"""

import argparse

from dax_comtools.devices.gp_ccg.driver import GranvilleCCG
from dax_comtools.util.influx_db import InfluxDBUtil
from dax_comtools.util.args import influx_args, get_influx_args, monitoring_tags_args, get_monitoring_tags_args, \
    logger_args, process_logger_args


def get_argparser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(description="Granville Phillips CCG reader for InfluxDB")
    parser.add_argument("device", type=str,
                        help="serial port to connect to")
    parser.add_argument("--timeout", type=float, default=GranvilleCCG.DEFAULT_TIMEOUT,
                        help="Read timeout in seconds (default: %(default)s)")
    parser.add_argument("--flush", action="store_true",
                        help="Clear output buffers and flush input buffers before any operations")
    parser.add_argument("--version", action="store_true",
                        help="Print the version string of the device in addition to the other operations")
    parser.add_argument("--field-name", type=str, default="pressure",
                        help="Field name for influx DB (default: %(default)s)")
    parser.add_argument('--dry-run', '-n', action='store_true', help='Read data but do not write to influx DB')

    logger_args(parser)
    influx_args(parser)
    monitoring_tags_args(parser, device_type='granville-phillips-ccg', required=True)

    return parser


def main():
    args = get_argparser().parse_args()
    process_logger_args(args)

    if not args.field_name:
        raise ValueError('Field name can not be empty')

    with GranvilleCCG(device=args.device, timeout=args.timeout) as CCG:
        if args.flush:
            # Flush
            CCG.flush()

        if args.version:
            print(f'Device version string: {CCG.get_version()}')

        # Get values
        pressure = CCG.get_pressure()

        if not args.dry_run:
            # Write data to influx DB
            with InfluxDBUtil(**get_influx_args(args)) as influx:
                influx.write_point_monitoring(**get_monitoring_tags_args(args), fields={args.field_name: pressure})


if __name__ == '__main__':
    main()
