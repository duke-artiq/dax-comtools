#!/usr/bin/env python3

"""
Find Kasli USB adapter locations.

Useful for tools that take a USB location as input and when multiple Kasli's are attached using USB.
On Linux, all USB locations can be found using `lsusb -t`.
For example: `artiq_flash -I "adapter usb location [<bus>-<port>[.<port>]...]" ...`.
See https://openocd.org/doc-release/html/Debug-Adapter-Configuration.html.
"""

import serial.tools.list_ports


def main():
    ports = serial.tools.list_ports.comports()
    kasli = [p for p in ports if p.vid == 0x403 and p.pid == 0x6011 and "Quad RS232-HS" in p.description]
    usb = sorted({k.location.split(":")[0] for k in kasli})

    if usb:
        print("Kasli USB locations:")
        for u in usb:
            print(u)
    else:
        print("No Kasli USB locations found")


if __name__ == '__main__':
    main()
