#!/usr/bin/env python3

"""
Wavelength Electronics LFI3751 temperature reader.
"""

import argparse

from dax_comtools.devices.we_lfi3751.driver import LFI3751
from dax_comtools.util.influx_db import InfluxDBUtil
from dax_comtools.util.args import influx_args, get_influx_args, monitoring_tags_args, get_monitoring_tags_args, \
    logger_args, process_logger_args


def get_argparser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(description="Wavelength Electronics LFI3751 temperature reader for InfluxDB")
    parser.add_argument("device", type=str,
                        help="Serial port for connected device (i.e. 'COM5' or '/dev/ttyS0')")
    parser.add_argument("--unit", type=int, default=LFI3751.DEFAULT_UNIT_NR,
                        help="Unit number (default: %(default)s)")
    parser.add_argument("--timeout", type=float, default=LFI3751.DEFAULT_TIMEOUT,
                        help="Read timeout in seconds (default: %(default)s)")
    parser.add_argument("--flush", action="store_true",
                        help="Clear output buffers and flush input buffers before any operations")
    parser.add_argument("--local-mode", action="store_true",
                        help="Return device state to local mode")
    parser.add_argument("--field-name", type=str, default="temperature",
                        help="Field name for influx DB (default: %(default)s)")
    parser.add_argument('--dry-run', '-n', action='store_true', help='Read data but do not write to influx DB')

    logger_args(parser)
    influx_args(parser)
    monitoring_tags_args(parser, device_type='we_lfi3751', required=True)

    return parser


def main():
    args = get_argparser().parse_args()
    process_logger_args(args)

    if not args.field_name:
        raise ValueError('Field name can not be empty')

    with LFI3751(device=args.device, unit_nr=args.unit, timeout=args.timeout) as lfi3751:
        if args.flush:
            # Flush
            lfi3751.flush()

        # Get values
        temperature = lfi3751.get_temperature()

        if args.local_mode:
            # Return state to local mode
            lfi3751.local_mode()

        if not args.dry_run:
            # Write data to influx DB
            with InfluxDBUtil(**get_influx_args(args)) as influx:
                influx.write_point_monitoring(**get_monitoring_tags_args(args), fields={args.field_name: temperature})


if __name__ == '__main__':
    main()
