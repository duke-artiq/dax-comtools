#!/usr/bin/env python3

"""
Red Pitaya logger
"""

import argparse

from dax_comtools.devices.red_pitaya.driver import RedPitayaV1, fft, RPTAcquisition
from dax_comtools.util.influx_db import InfluxDBUtil
from dax_comtools.util.args import influx_args, get_influx_args, monitoring_tags_args, get_monitoring_tags_args, \
    logger_args, process_logger_args
import numpy as np


def get_argparser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(description="Red Pitaya reader for InfluxDb")
    parser.add_argument("-i", "--ip", type=str,
                        help="IP address of the Red Pitaya", required=True)
    parser.add_argument("-c", "--channel", type=int, default=1, help="Channel to read from (default: %(default)s)")
    parser.add_argument("--lv-gain", action='store_true', help='Read using low-voltage gain')
    parser.add_argument('--dry-run', '-n', action='store_true', help='Read data but do not write to influx DB')

    logger_args(parser)
    influx_args(parser)
    monitoring_tags_args(parser, device_type='red_pitaya', required=True)

    return parser


def main():
    args = get_argparser().parse_args()
    process_logger_args(args)

    rpt = RedPitayaV1(addr=args.ip)
    # Get data and do FFT
    gain = RPTAcquisition.Gains.LV if args.lv_gain else RPTAcquisition.Gains.HV
    data = rpt.acquisition.simple_acquisition(channel=args.channel, gain=gain)
    # assert isinstance(data, np.ndarray)
    freqs, ps = fft(data, rpt.acquisition.sample_rate)

    peak_frequency = freqs[np.argmax(ps)]
    # Get values
    max_voltage = np.max(data)
    min_voltage = np.min(data)
    average_voltage = np.average(data)
    rms_voltage = np.sqrt(np.mean(np.square(data)))

    assert isinstance(max_voltage, float)
    assert isinstance(min_voltage, float)
    assert isinstance(average_voltage, float)
    assert isinstance(rms_voltage, float)
    assert isinstance(peak_frequency, float)

    values = {
        'max_voltage': max_voltage,
        'min_voltage': min_voltage,
        'average_voltage': average_voltage,
        'rms_voltage': rms_voltage,
        'frequency': peak_frequency
    }

    if not args.dry_run:
        # Write data to influx DB
        with InfluxDBUtil(**get_influx_args(args)) as influx:
            influx.write_point_monitoring(**get_monitoring_tags_args(args), **values)


if __name__ == '__main__':
    main()
