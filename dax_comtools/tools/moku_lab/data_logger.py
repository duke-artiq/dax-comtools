#!/usr/bin/env python3

"""
Moku:Lab data logger that logs immediately to InfluxDB.

Note that before using the Moku:Lab data logger, the instrument files have to be updated and installed::

    moku update fetch
    moku ... update install
"""

import os
import argparse
import logging
import typing
import time
import itertools

import pymoku
import pymoku.instruments

import dax_comtools.util.influx_db
import dax_comtools.util.args
from dax_comtools.tools.moku_lab.patch_instr_path import INSTRUMENT_PATH_KEY, INSTRUMENT_PATH, patch_instrument_path

logger = logging.getLogger(__name__)
"""The module logger."""


class MokuLabDataLogger:
    """Moku:Lab data logger to InfluxDB."""

    _MAX_DURATION: int = 2 ** 30  # Found empirically
    """Maximum stream duration in seconds."""

    def __init__(self, *,
                 serial: typing.Optional[str] = None,
                 name: typing.Optional[str] = None,
                 ip: typing.Optional[str] = None,
                 force: bool = False,
                 ch1: bool, ch2: bool, sample_rate: int, duration: int,
                 buffer_size: int = 15, timeout: typing.Optional[int] = None,
                 latency_compensation: float = 0.0,
                 progress: bool = False,
                 influx_kwargs: typing.Dict[str, typing.Any],
                 tag_kwargs: typing.Dict[str, typing.Any]):
        """Initialize a new Moku:Lab data logging object.

        Provide `serial`, `name`, or `ip`.

        Connections are immediately opened.
        Call :func:`close` to close all connections.

        :param serial: Serial number to connect to
        :param name: Name of the device to connect to
        :param ip: IP address of the device to connect to
        :param force: Force the connection
        :param ch1: Enable ch1
        :param ch2: Enable ch2
        :param sample_rate: Data sample rate in Hz
        :param duration: Stream duration in seconds (`0` for infinite stream)
        :param buffer_size: The buffer size in seconds
        :param timeout: Connection timeout when streaming data in seconds
        :param latency_compensation: Compensation for latency between starting the capture and fixing the timestamp
        :param progress: Print progress messages
        :param influx_kwargs: Dict with keyword arguments for InfluxDB
        :param tag_kwargs: Dict with keyword arguments for monitoring tags
        """
        assert serial is None or isinstance(serial, str)
        assert name is None or isinstance(name, str)
        assert ip is None or isinstance(ip, str)
        assert isinstance(force, bool)
        assert isinstance(ch1, bool)
        assert isinstance(ch2, bool)
        assert isinstance(sample_rate, int)
        assert isinstance(duration, int)
        assert isinstance(buffer_size, int)
        assert timeout is None or isinstance(timeout, int)
        assert isinstance(latency_compensation, float)
        assert isinstance(progress, bool)
        assert isinstance(influx_kwargs, dict)
        assert isinstance(tag_kwargs, dict)

        if sum(a is not None for a in [serial, name, ip]) != 1:
            # Constructor called incorrectly
            raise TypeError('Must provide a serial, name, or ip')
        if not ch1 and not ch2:
            raise ValueError('No channels were enabled')
        if sample_rate < 10:
            raise ValueError('Sample rate must be >=10')
        if sample_rate > 20e3:
            raise ValueError('Sample rate must be <=20e3')
        if sample_rate > 1000:
            logger.warning('High sampling rates can cause strain on your computer and the database')
        if duration < 0:
            raise ValueError('Duration must be >=0')
        if duration > self._MAX_DURATION:
            raise ValueError('Duration exceeds maximum stream duration')
        if buffer_size < 1:
            raise ValueError('Buffer size must be >=1')

        # Create the Moku
        if serial is not None:
            logger.info(f'Connecting to serial "{serial}"')
            self._moku = pymoku.Moku.get_by_serial(serial, force=force)
        elif name is not None:
            logger.info(f'Connecting to name "{name}"')
            self._moku = pymoku.Moku.get_by_name(name, force=force)
        else:
            logger.info(f'Connecting to ip [{ip}]')
            self._moku = pymoku.Moku(ip, force=force)
        logger.debug(f'Serial {self._moku.get_serial()}, version {self._moku.get_version()}, '
                     f'hw version {self._moku.get_hw_version()}, firmware build {self._moku.get_firmware_build()}')

        # Store attributes
        self._ch1: bool = ch1
        self._ch2: bool = ch2
        self._sample_rate: int = sample_rate
        self._inf_stream: bool = duration == 0
        self._duration: int = self._MAX_DURATION if self._inf_stream else duration
        self._buffer_size: int = buffer_size * sample_rate  # Convert buffer size to number of samples
        self._timeout: typing.Optional[int] = timeout
        self._latency_compensation: float = latency_compensation
        self._progress: bool = progress
        self._tag_kwargs: typing.Dict[str, typing.Any] = tag_kwargs
        logger.debug(f'Buffer size set to {self._buffer_size} samples')

        # Internal timestamp and sample counter
        self._timestamp: float = 0.0
        self._sample_count: int = 0

        # Create InfluxDB util
        self._influx: dax_comtools.util.influx_db.InfluxDBUtil = dax_comtools.util.influx_db.InfluxDBUtil(
            buffer_size=self._buffer_size,
            retries=1,
            timeout=max(buffer_size // 2, 1),
            **influx_kwargs
        )

    def stream(self) -> None:
        """Start streaming."""

        try:
            # Deploy the data logger
            logger.debug('Deploying and configuring data logger')
            i: pymoku.instruments.Datalogger = self._moku.deploy_or_connect(pymoku.instruments.Datalogger)
            # Set the sampling rate
            i.set_samplerate(self._sample_rate)
            # Stop a previous session, if any
            i.stop_stream_data()

            while True:
                try:
                    # Start a new stream session
                    i.start_stream_data(duration=self._duration, ch1=self._ch1, ch2=self._ch2)
                    # Record the timestamp and apply latency compensation
                    self._timestamp = time.time() + self._latency_compensation
                    # Reset the sample counter
                    self._sample_count = 0

                    while True:
                        # Obtain data
                        data = i.get_stream_data(n=self._buffer_size, timeout=self._timeout)

                        if not any(data):
                            # When no samples are received, the streaming session has ended
                            break

                        # Process data and update timestamp
                        self._process_data(data)

                finally:
                    # Stop data streaming session to free resources
                    i.stop_stream_data()

                if not self._inf_stream:
                    break  # Break if we are not in infinite stream mode

        except pymoku.StreamException:
            logger.exception('Stream exception occurred')
        except KeyboardInterrupt:
            logger.info('Program terminated by user')

    def _process_data(self, data: typing.Tuple[typing.List[float], typing.List[float]]) -> None:
        # Unpack data
        ch1_data, ch2_data = data
        if self._progress:
            logger.info('Processing data (ch1 %d sample(s), ch2 %d sample(s)) (timestamp %f)',
                        len(ch1_data), len(ch2_data), self._timestamp + (self._sample_count / self._sample_rate))

        for ch1, ch2 in itertools.zip_longest(ch1_data, ch2_data, fillvalue=None):
            # Calculate timestamp (re-calculate each time to prevent accumulating drift due to rounding errors)
            timestamp: int = round((self._timestamp + (self._sample_count / self._sample_rate)) * 1e9)
            # Write
            self._influx.write_point_monitoring(ch1=ch1, ch2=ch2, timestamp=timestamp, **self._tag_kwargs)
            # Increment the sample counter
            self._sample_count += 1

    def close(self) -> None:
        # Close the connection to release network resources
        logger.info('Closing connections')
        self._moku.close()
        self._influx.close()


def _get_args() -> argparse.Namespace:
    # Create an argument parser
    parser = argparse.ArgumentParser(description="Moku:Lab data logger")

    group = parser.add_argument_group('Device connection')
    device = group.add_mutually_exclusive_group(required=True)
    device.add_argument('--serial', type=str, default=None, help='Serial number to connect to')
    device.add_argument('--name', type=str, default=None, help='Name of the device to connect to')
    device.add_argument('--ip', type=str, default=None, help='IP address of the device to connect to')
    group.add_argument('--force', action='store_true', help='Force connection to the device')

    group = parser.add_argument_group('Stream configuration')
    group.add_argument('--ch1', '-1', action='store_true', help='Enable input channel 1')
    group.add_argument('--ch2', '-2', action='store_true', help='Enable input channel 2')
    group.add_argument('--sample-rate', type=int, default=10, help='Data sample rate in Hz (default: %(default)s)')
    group.add_argument('--duration', type=int, default=0,
                       help='Streaming duration in seconds, 0 for infinite stream (default: %(default)s)')

    group = parser.add_argument_group('Advanced settings')
    group.add_argument('--progress', action='store_true', help='Print periodic progress messages (INFO level)')
    group.add_argument('--buffer_size', type=int, default=15, help='The buffer size in seconds (default: %(default)s)')
    group.add_argument('--timeout', type=int, default=None, help='Connection timeout when streaming data in seconds')
    group.add_argument('--latency-compensation', type=float, default=0.0,
                       help='Compensation for latency between starting the capture and fixing the timestamp')
    group.add_argument('--no-patch-instr-path', action='store_true',
                       help='Do not check or patch the pymoku instrument path before logging')

    # Add standard arguments
    dax_comtools.util.args.influx_args(parser)
    dax_comtools.util.args.monitoring_tags_args(parser, device_type='moku', required=True)
    dax_comtools.util.args.logger_args(parser)

    return parser.parse_args()


def main() -> None:
    # Get the arguments
    args = _get_args()
    # Configure logger
    dax_comtools.util.args.process_logger_args(args, default_level=logging.INFO)

    if INSTRUMENT_PATH is None and os.getenv('NIX_STORE') is not None:
        # The Nix store is read-only, as a result we require a custom instrument path
        logger.error(f'Set the pymoku instrument path environment variable {INSTRUMENT_PATH_KEY} when using Nix')
        exit(1)

    if INSTRUMENT_PATH is not None and not args.no_patch_instr_path:
        # Patch instrument path
        patch_instrument_path()

    # Create the data logger and stream
    data_logger = MokuLabDataLogger(
        serial=args.serial,
        name=args.name,
        ip=args.ip,
        force=args.force,
        ch1=args.ch1,
        ch2=args.ch2,
        sample_rate=args.sample_rate,
        duration=args.duration,
        buffer_size=args.buffer_size,
        timeout=args.timeout,
        latency_compensation=args.latency_compensation,
        progress=args.progress,
        influx_kwargs=dax_comtools.util.args.get_influx_args(args),
        tag_kwargs=dax_comtools.util.args.get_monitoring_tags_args(args)
    )
    try:
        data_logger.stream()
    finally:
        data_logger.close()


if __name__ == '__main__':
    main()
