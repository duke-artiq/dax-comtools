#!/usr/bin/env python3

"""
Moku:Lab patch tool for `pymoku` instrument path.
"""

import os
import os.path
import argparse
import logging
import typing

import dax_comtools.util.args

__all__ = ['INSTRUMENT_PATH_KEY', 'INSTRUMENT_PATH', 'patch_instrument_path']

logger = logging.getLogger(__name__)
"""The module logger."""

INSTRUMENT_PATH_KEY: str = 'PYMOKU_INSTR_PATH'
"""The key of the pymoku instrument path."""
INSTRUMENT_PATH: typing.Optional[str] = os.getenv(INSTRUMENT_PATH_KEY)
"""The pymoku instrument path."""


def patch_instrument_path() -> None:
    if INSTRUMENT_PATH is not None:
        # Expand instrument path
        instrument_path = os.path.expanduser(INSTRUMENT_PATH)
        logger.debug(f'Expanded instrument path: {instrument_path}')

        # Find the data folder
        import pymoku
        data_folder = os.path.join(os.path.dirname(pymoku.__file__), 'data')
        logger.debug(f'pymoku data folder: {data_folder}')

        try:
            for i in range(1000):
                # Source
                file_name = f'{i:03d}'
                src = os.path.join(data_folder, file_name)

                if os.path.exists(src):
                    # Destination
                    dst = os.path.join(instrument_path, file_name)

                    if os.path.exists(dst):
                        if os.path.islink(dst):
                            # Remove existing link
                            os.remove(dst)
                        else:
                            logger.error(f'Destination exists but is not a link: {dst}')
                            exit(1)

                    # Make a symlink
                    logger.debug(f'Creating symlink with destination: {dst}')
                    os.symlink(src, dst)
                else:
                    break
        except OSError:
            logger.exception('On Windows, symlinks can only be created by privileged users')
            exit(1)

    else:
        # No instrument path available
        logger.error(f'The pymoku instrument path environment variable {INSTRUMENT_PATH_KEY} is not set')
        exit(1)


def main() -> None:
    # Create an argument parser
    parser = argparse.ArgumentParser(description="Moku:Lab instrument path patcher")
    # Add standard arguments
    dax_comtools.util.args.logger_args(parser)
    # Parse arguments
    args = parser.parse_args()

    # Configure logger
    dax_comtools.util.args.process_logger_args(args, default_level=logging.INFO)
    # Patch instrument path
    patch_instrument_path()


if __name__ == '__main__':
    main()
