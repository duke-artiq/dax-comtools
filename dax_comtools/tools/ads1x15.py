#!/usr/bin/env python3

import argparse
import logging
import typing

from dax_comtools.devices.ads1x15.ads1x15 import ADS1x15, ADS1X15_DEFAULT_ADDRESS, Mode
from dax_comtools.devices.ads1x15.ads1015 import ADS1015
from dax_comtools.devices.ads1x15.ads1115 import ADS1115
from dax_comtools.devices.ads1x15.analog_in import AnalogIn
from dax_comtools.util.influx_db import InfluxDBUtil
from dax_comtools.util.args import influx_args, get_influx_args, monitoring_tags_args, get_monitoring_tags_args, \
    logger_args, process_logger_args, str_to_hex

logger = logging.getLogger(__name__)

MODELS: typing.Mapping[str, typing.Type[ADS1x15]] = {
    'ads1015': ADS1015,
    'ads1115': ADS1115,
}

GAIN_MAP: typing.Mapping[str, float] = {
    '2/3': 2 / 3,
    '1': 1,
    '2': 2,
    '4': 4,
    '8': 8,
    '16': 16,
}
"""
# The ADS1015 and ADS1115 both have the same gain options.
#
#       GAIN    RANGE (V)
#       ----    ---------
#        2/3    +/- 6.144
#          1    +/- 4.096
#          2    +/- 2.048
#          4    +/- 1.024
#          8    +/- 0.512
#         16    +/- 0.256
"""

_DEFAULT_GAIN = '1'
_DEFAULT_DEVICE_TYPE = '<ADC model>'


def get_argparser() -> argparse.ArgumentParser:
    # Argument parser
    parser = argparse.ArgumentParser(description='ADS1x15 reader for InfluxDB')

    # Arguments
    ads1x15 = parser.add_argument_group('ADS1x15 arguments')
    ads1x15.add_argument('model', choices=sorted(MODELS), help='ADC model')
    ads1x15.add_argument('channel', type=int, help='channel number (positive pin for differential input)')
    ads1x15.add_argument('i2c', type=str, help='path to the I2C bus (e.g. "/dev/i2c-1")')
    ads1x15.add_argument('--channel-n', default=None, type=int,
                         help='channel number for negative pin of differential input')
    ads1x15.add_argument('--gain', choices=list(GAIN_MAP), default=_DEFAULT_GAIN,
                         help='ADC gain (default: %(default)s with a range of +/- 4.096 V)')
    ads1x15.add_argument('--address', default=ADS1X15_DEFAULT_ADDRESS, type=str_to_hex,
                         help='hex I2C address (default: %(default)#x)')
    parser.add_argument('--dry-run', '-n', action='store_true', help='read data but do not write to influx DB')

    # InfluxDB, monitoring, and logger arguments
    influx_args(parser)
    monitoring_tags_args(parser, device_type=_DEFAULT_DEVICE_TYPE, required=True)
    logger_args(parser)

    # Return parser
    return parser


def main() -> None:
    """Retrieve data from an ADS1x15 and write results to InfluxDB."""
    # Parse arguments
    args = get_argparser().parse_args()
    process_logger_args(args)  # This initializes logging system log level according to -v/-q args

    # Create ADC object
    logger.debug('I2C bus %s, address 0x%x', args.i2c, args.address)
    adc = MODELS[args.model](args.i2c, address=args.address, mode=Mode.SINGLE)
    # Set gain
    logger.debug('setting gain to %s', args.gain)
    adc.gain = GAIN_MAP[args.gain]
    # Create channel
    logger.debug(f'creating channel {args.channel}' if args.channel_n is None
                 else f'creating differential channel ({args.channel},{args.channel_n})')
    channel = AnalogIn(adc, args.channel, negative_pin=args.channel_n)
    # Read voltage
    voltage = channel.voltage
    assert isinstance(voltage, float), 'Returned voltage is not of type float'
    logger.info('voltage: %f V', voltage)

    if not args.dry_run:
        # Get monitoring tags
        monitoring_tags = get_monitoring_tags_args(args)
        if monitoring_tags['device_type'] == _DEFAULT_DEVICE_TYPE:
            # Override device type
            monitoring_tags['device_type'] = args.model

        # Write data to influx DB
        with InfluxDBUtil(**get_influx_args(args)) as influx:
            influx.write_point_monitoring(**monitoring_tags, voltage=voltage)


if __name__ == '__main__':
    main()
