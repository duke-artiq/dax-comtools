#!/usr/bin/env python3

"""
State message logger for InfluxDB.
Allows a computer to report a message to the server
"""

import argparse

from dax_comtools.util.influx_db import InfluxDBUtil
from dax_comtools.util.args import influx_args, get_influx_args, monitoring_tags_args, get_monitoring_tags_args, \
    logger_args, process_logger_args


def get_argparser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(description="State Message Writer for InfluxDb")
    parser.add_argument("-m", "--message", type=str, default='Ping',
                        help="State message to send to InfluxDB (default: %(default)s)")
    parser.add_argument('--dry-run', '-n', action='store_true', help='Read data but do not write to influx DB')

    logger_args(parser)
    influx_args(parser)
    monitoring_tags_args(parser, required=True)

    return parser


def main():
    args = get_argparser().parse_args()
    process_logger_args(args)

    # Get values
    values = {
        'message': args.message,
    }

    if not args.dry_run:
        # Write data to influx DB
        with InfluxDBUtil(**get_influx_args(args)) as influx:
            influx.write_point_monitoring(**get_monitoring_tags_args(args), **values)


if __name__ == '__main__':
    main()
