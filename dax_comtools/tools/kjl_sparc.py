#!/usr/bin/env python3

"""
KJL SPARC vacuum gauge reader.
"""

import argparse

from dax_comtools.devices.kjl_sparc.driver import SparcDriver
from dax_comtools.util.influx_db import InfluxDBUtil
from dax_comtools.util.args import influx_args, get_influx_args, monitoring_tags_args, get_monitoring_tags_args, \
    logger_args, process_logger_args


def get_argparser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(description="KJL SPARC reader for InfluxDB")
    parser.add_argument("device", type=str,
                        help="serial port to connect to")
    parser.add_argument("--manual", action="store_true",
                        help="Read in manual mode (i.e. request data from device with a query)")
    parser.add_argument("--set-mode", action="store_true",
                        help="Set the device to the configured mode")
    parser.add_argument("--timeout", type=float, default=None,
                        help=f"Read timeout in seconds (default: {SparcDriver.DEFAULT_TIMEOUT}/"
                             f"{SparcDriver.DEFAULT_AUTO_TIMEOUT} for manual/auto mode)")
    parser.add_argument("--flush", action="store_true",
                        help="Clear serial output buffer and flush serial input buffer before any operations")
    parser.add_argument("--version", action="store_true",
                        help="Print the version string of the device in addition to the other operations")
    parser.add_argument("--field-name", type=str, default="pressure",
                        help="Field name for influx DB (default: %(default)s)")
    parser.add_argument('--dry-run', '-n', action='store_true', help='Read data but do not write to influx DB')

    logger_args(parser)
    influx_args(parser)
    monitoring_tags_args(parser, device_type='kjl_sparc', required=True)

    return parser


def main():
    args = get_argparser().parse_args()
    process_logger_args(args)

    if not args.field_name:
        raise ValueError('Field name can not be empty')

    if args.timeout is None:
        # Set default timeout
        timeout = SparcDriver.DEFAULT_TIMEOUT if args.manual else SparcDriver.DEFAULT_AUTO_TIMEOUT
    else:
        timeout = args.timeout

    with SparcDriver(device=args.device, timeout=timeout) as sparc:
        if args.set_mode:
            # Set mode first
            if args.manual:
                sparc.set_manual_mode()
            else:
                sparc.set_auto_mode()

        if args.flush:
            # Flush buffers
            sparc.flush()

        if args.version:
            print(f'Device version string: {sparc.get_version()}')

        # Obtain pressure
        pressure = sparc.get_pressure() if args.manual else sparc.read_pressure()

        if not args.dry_run:
            # Write data to influx DB
            with InfluxDBUtil(**get_influx_args(args)) as influx:
                influx.write_point_monitoring(**get_monitoring_tags_args(args), fields={args.field_name: pressure})


if __name__ == '__main__':
    main()
