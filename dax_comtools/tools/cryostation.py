#!/usr/bin/env python3

"""
Montana instruments CryoStation logger.
"""

import argparse

from dax_comtools.devices.mi_cryostation.driver import CryoComm
from dax_comtools.util.influx_db import InfluxDBUtil
from dax_comtools.util.args import influx_args, get_influx_args, monitoring_tags_args, get_monitoring_tags_args, \
    logger_args, process_logger_args


def get_argparser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(description="Montana Instruments CryoStation reader for InfluxDb")
    parser.add_argument("-i", "--ip", type=str, default='localhost',
                        help="IP address of the CryoStation laptop (default: %(default)s)")
    parser.add_argument("-p", "--port", type=int, default=CryoComm.DEFAULT_PORT,
                        help="Port to connect to (default: %(default)s)")
    parser.add_argument('--dry-run', '-n', action='store_true', help='Read data but do not write to influx DB')

    logger_args(parser)
    influx_args(parser)
    monitoring_tags_args(parser, device_type='cryostation', required=True)

    return parser


def main():
    args = get_argparser().parse_args()
    process_logger_args(args)

    with CryoComm(ip=args.ip, port=args.port) as cryo:
        # Get values
        values = {
            'idle_state': cryo.get_idle_state(),
            'alarm_state': cryo.get_alarm_state(),
            'nitrogen_state': cryo.get_nitrogen_state(),
            'platform_temp': cryo.get_platform_temperature(),
            'sample_temp': cryo.get_sample_temperature(),
            'stage_1_temp': cryo.get_stage_1_temperature(),
            'stage_2_temp': cryo.get_stage_2_temperature(),
            'chamber_pressure': cryo.get_chamber_pressure(),
        }

        if not args.dry_run:
            # Write data to influx DB
            with InfluxDBUtil(**get_influx_args(args)) as influx:
                influx.write_point_monitoring(**get_monitoring_tags_args(args), **values)


if __name__ == '__main__':
    main()
