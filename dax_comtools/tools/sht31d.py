#!/usr/bin/env python3

import argparse
import logging

from dax_comtools.devices.sht31d.adafruit_sht31d import SHT31D, SHT31_DEFAULT_ADDRESS
from dax_comtools.util.influx_db import InfluxDBUtil
from dax_comtools.util.args import influx_args, get_influx_args, monitoring_tags_args, get_monitoring_tags_args, \
    logger_args, process_logger_args, str_to_hex

logger = logging.getLogger(__name__)


def get_argparser() -> argparse.ArgumentParser:
    # Argument parser
    parser = argparse.ArgumentParser(description='SHT31-D temperature/humidity sensor reader for InfluxDB')

    # Arguments
    sht31d = parser.add_argument_group('SHT31-D arguments')
    sht31d.add_argument('i2c', type=str, help='path to the I2C bus (e.g. "/dev/i2c-1")')
    sht31d.add_argument('--address', default=SHT31_DEFAULT_ADDRESS, type=str_to_hex,
                        help='hex I2C address of the sensor (default: %(default)#x)')
    parser.add_argument('--dry-run', '-n', action='store_true', help='read data but do not write to influx DB')

    # InfluxDB, monitoring, and logger arguments
    influx_args(parser)
    monitoring_tags_args(parser, device_type='sht31d', required=True)
    logger_args(parser)

    # Return parser
    return parser


def main() -> None:
    """Retrieve data from an SHT31-D temperature/humidity sensor and write results to InfluxDB."""
    # Parse arguments
    args = get_argparser().parse_args()
    process_logger_args(args)  # This initializes logging system log level according to -v/-q args

    # Read data from sensor
    logger.debug('I2C bus %s', args.i2c)
    sensor = SHT31D(args.i2c, address=args.address)
    temperature, humidity = sensor.temperature_and_relative_humidity
    assert isinstance(temperature, float), 'Returned temperature is not of type float'
    assert isinstance(humidity, float), 'Returned humidity is not of type float'
    logger.info('Temperature: %.2f C', temperature)
    logger.info('Relative humidity: %.2f %%', humidity)

    if not args.dry_run:
        # Write data to influx DB
        with InfluxDBUtil(**get_influx_args(args)) as influx:
            influx.write_point_monitoring(**get_monitoring_tags_args(args), temperature=temperature, humidity=humidity)


if __name__ == '__main__':
    main()
