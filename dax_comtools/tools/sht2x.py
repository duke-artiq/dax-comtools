#!/usr/bin/env python3

import argparse
import logging

from dax_comtools.devices.sht2x.sht21 import SHT21
from dax_comtools.util.influx_db import InfluxDBUtil
from dax_comtools.util.args import influx_args, get_influx_args, monitoring_tags_args, get_monitoring_tags_args, \
    logger_args, process_logger_args

logger = logging.getLogger(__name__)


def get_argparser() -> argparse.ArgumentParser:
    # Argument parser
    parser = argparse.ArgumentParser(description='SHT2x temperature/humidity sensor reader for InfluxDB')

    # Arguments
    sht2x = parser.add_argument_group('SHT2x arguments')
    sht2x.add_argument('i2c', type=str, help='path to the I2C bus (e.g. "/dev/i2c-1")')
    sht2x.add_argument('--no-soft-reset', action='store_false',
                       help='do not reboot the sensor system and reset to default configuration before measuring')
    parser.add_argument('--dry-run', '-n', action='store_true', help='read data but do not write to influx DB')

    # InfluxDB, monitoring, and logger arguments
    influx_args(parser)
    monitoring_tags_args(parser, device_type='sht2x', required=True)
    logger_args(parser)

    # Return parser
    return parser


def main() -> None:
    """Retrieve data from an SHT2x temperature/humidity sensor and write results to InfluxDB."""
    # Parse arguments
    args = get_argparser().parse_args()
    process_logger_args(args)  # This initializes logging system log level according to -v/-q args
    soft_reset = not args.no_soft_reset

    # Read data from sensor
    logger.debug('I2C bus %s', args.i2c)
    logger.debug('Soft reset = %s', str(soft_reset))
    sensor = SHT21(args.i2c, soft_reset=soft_reset)
    temperature: float = sensor.read_temperature()
    humidity: float = sensor.read_humidity()
    assert isinstance(temperature, float), 'Returned temperature is not of type float'
    assert isinstance(humidity, float), 'Returned humidity is not of type float'
    logger.info('Temperature: %.2f C', temperature)
    logger.info('Relative humidity: %.2f %%', humidity)

    if not args.dry_run:
        # Write data to influx DB
        with InfluxDBUtil(**get_influx_args(args)) as influx:
            influx.write_point_monitoring(**get_monitoring_tags_args(args), temperature=temperature, humidity=humidity)


if __name__ == '__main__':
    main()
