#!/usr/bin/env python3

import argparse
import logging

from dax_comtools.devices.tca9548a.tca9548a import TCA9548A
from dax_comtools.util.args import logger_args, process_logger_args, str_to_hex

logger = logging.getLogger(__name__)


def cmd_status(mux: TCA9548A, _: argparse.Namespace) -> None:
    logger.debug('Reading status')
    status = mux.get_status()
    print('| CH | STATUS |')
    print('|----|--------|')
    for i, s in enumerate(status):
        print(f'| {i:<2} | {s:>6} |')


def cmd_reset(mux: TCA9548A, _: argparse.Namespace) -> None:
    logger.info('Reset channels')
    mux.set_channels()


def cmd_set(mux: TCA9548A, args: argparse.Namespace) -> None:
    logger.info(f'Set channels {args.channels}')
    mux.set_channels(*args.channels)


def cmd_enable(mux: TCA9548A, args: argparse.Namespace) -> None:
    logger.info(f'Enable channels {args.channels}')
    mux.enable_channels(*args.channels)


def cmd_disable(mux: TCA9548A, args: argparse.Namespace) -> None:
    logger.info(f'Disable channels {args.channels}')
    mux.disable_channels(*args.channels)


def get_argparser() -> argparse.ArgumentParser:
    # Argument parser
    parser = argparse.ArgumentParser(description='TCA9548A I2C MUX utility')
    subparsers = parser.add_subparsers(dest='command', required=True)

    # Generic arguments
    parser.add_argument('i2c', type=str, help='path to the I2C bus (e.g. "/dev/i2c-1")')
    parser.add_argument('--address', default=TCA9548A.DEFAULT_ADDRESS, type=str_to_hex,
                        help='hex I2C address of the I2C MUX (default: %(default)#x)')
    logger_args(parser)

    # Status arguments
    status = subparsers.add_parser('status', help='read the status of the device')
    status.set_defaults(fn=cmd_status)

    # Reset arguments
    reset = subparsers.add_parser('reset', help='disable all channels')
    reset.set_defaults(fn=cmd_reset)

    # Set/enable/disable arguments
    for name, fn in [('set', cmd_set), ('enable', cmd_enable), ('disable', cmd_disable)]:
        sub = subparsers.add_parser(name, help=f'{name} channels of the I2C MUX')
        sub.add_argument('channels', type=int, nargs='+', choices=list(range(TCA9548A.NUM_CHANNELS)))
        sub.set_defaults(fn=fn)

    # Return parser
    return parser


def main() -> None:
    # Parse arguments
    args = get_argparser().parse_args()
    process_logger_args(args)  # This initializes logging system log level according to -v/-q args

    # Create driver
    logger.debug('I2C bus %s', args.i2c)
    mux = TCA9548A(args.i2c, address=args.address)

    # Call subcommand function
    args.fn(mux, args)


if __name__ == '__main__':
    main()
