#!/usr/bin/env python3

"""
Tool to generate a Grafana dashboard from the sensor JSON file.
Outputs a JSON dashboard model that can be imported in Grafana.
"""

import argparse
import json
from copy import deepcopy
import itertools
import typing

# template for the dashboard
_BASE_DASHBOARD: typing.Dict[str, typing.Any] = {
    "__inputs": [{
        "name": "DS_INFLUXDB_DAX",
        "label": "InfluxDB DAX",
        "description": "",
        "type": "datasource",
        "pluginId": "influxdb",
        "pluginName": "InfluxDB"
    }],
    "annotations": {
        "list": [{
            "builtIn": 1,
            "datasource": "-- Grafana --",
            "enable": True,
            "hide": True,
            "iconColor": "rgba(0, 211, 255, 1)",
            "name": "Annotations & Alerts",
            "type": "dashboard"
        }]
    },
    "description": "Dashboard for SensorPush data from InfluxDB",
    "editable": True,
    "graphToolTip": 1,
    "links": [],
    "panels": [],
    "refresh": "1m",
    "schemaVersion": 19,
    "style": "dark",
    "templating": {
        "list": [{
            "allFormat": "glob",
            "current": {
                "text": "InfluxDB DAX",
                "value": "InfluxDB DAX"
            },
            "datasource": "InfluxDB DAX",
            "hide": 0,
            "includeAll": False,
            "label": "",
            "multi": False,
            "name": "datasource",
            "options": [],
            "query": "influxdb",
            "refresh": 1,
            "regex": "",
            "skipUrlSync": False,
            "type": "datasource"
        }]
    },
    "time": {
        "from": "now-1h",
        "to": "now"
    },
    "timepicker": {
        "refresh_intervals": [
            "5s",
            "10s",
            "30s",
            "1m",
            "5m",
            "15m",
            "30m",
            "1h",
            "2h",
            "1d"
        ],
        "time_options": [
            "5m",
            "15m",
            "1h",
            "6h",
            "12h",
            "24h",
            "2d",
            "7d",
            "30d"
        ]
    },
    "timezone": "browser",
    "title": "SensorPush Data",
    "version": 1
}

_CUR_TITLE_PANEL: typing.Dict[str, typing.Any] = {
    "collapsed": False,
    "gridPos": {
        "h": 1,
        "w": 24,
        "x": 0,
        "y": 0
    },
    "id": None,
    "panels": [],
    "repeat": None,
    "title": "Current Values",
    "type": "row"
}

_PLOT_TITLE_PANEL: typing.Dict[str, typing.Any] = {
    "collapsed": False,
    "gridPos": {
        "h": 1,
        "w": 24,
        "x": 0,
        "y": 0
    },
    "id": None,
    "panels": [],
    "repeat": None,
    "title": "Plots",
    "type": "row"
}

_CUR_PANEL: typing.Dict[str, typing.Any] = {
    "datasource": "${DS_INFLUXDB_DAX}",
    "fieldConfig": {
        "defaults": {
            "custom": {},
            "decimals": 2,
            "mappings": [],
            "max": 50,
            "min": 0,
            "thresholds": {
                "mode": "absolute",
                "steps": [
                    {
                        "color": "green",
                        "value": None
                    },
                    {
                        "color": "red",
                        "value": 30
                    }
                ]
            }
        },
        "overrides": [
            {
                "matcher": {
                    "id": "byName",
                    "options": "Temperature"
                },
                "properties": [
                    {
                        "id": "unit",
                        "value": "celsius"
                    }
                ]
            },
            {
                "matcher": {
                    "id": "byName",
                    "options": "Humidity"
                },
                "properties": [
                    {
                        "id": "unit",
                        "value": "humidity"
                    },
                    {
                        "id": "max",
                        "value": 100
                    },
                    {
                        "id": "thresholds",
                        "value": {
                            "mode": "absolute",
                            "steps": [
                                {
                                    "color": "green",
                                    "value": None
                                },
                                {
                                    "color": "red",
                                    "value": 80
                                }
                            ]
                        }
                    }
                ]
            }
        ]
    },
    "gridPos": {
        "h": 5,
        "w": 6,
        "x": 0,
        "y": 0
    },
    "id": None,
    "options": {
        "reduceOptions": {
            "calcs": [
                "lastNotNull"
            ],
            "fields": "",
            "values": False
        },
        "showThresholdLabels": False,
        "showThresholdMarkers": True
    },
    "targets": [
        {
            "alias": "Temperature",
            "groupBy": [],
            "measurement": "monitoring",
            "orderByTime": "ASC",
            "policy": "default",
            "refId": "A",
            "resultFormat": "time_series",
            "select": [
                [
                    {
                        "params": [
                            "temperature"
                        ],
                        "type": "field"
                    },
                    {
                        "params": [],
                        "type": "last"
                    }
                ]
            ],
            "tags": [
                {
                    "key": "building",
                    "operator": "=",
                    "value": None
                },
                {
                    "condition": "AND",
                    "key": "room",
                    "operator": "=",
                    "value": None
                },
                {
                    "condition": "AND",
                    "key": "source",
                    "operator": "=",
                    "value": None
                },
                {
                    "condition": "AND",
                    "key": "device_type",
                    "operator": "=",
                    "value": "sensorpush"
                }
            ]
        },
        {
            "alias": "Humidity",
            "groupBy": [],
            "measurement": "monitoring",
            "orderByTime": "ASC",
            "policy": "default",
            "refId": "B",
            "resultFormat": "time_series",
            "select": [
                [
                    {
                        "params": [
                            "humidity"
                        ],
                        "type": "field"
                    },
                    {
                        "params": [],
                        "type": "last"
                    }
                ]
            ],
            "tags": [
                {
                    "key": "building",
                    "operator": "=",
                    "value": None
                },
                {
                    "condition": "AND",
                    "key": "room",
                    "operator": "=",
                    "value": None
                },
                {
                    "condition": "AND",
                    "key": "source",
                    "operator": "=",
                    "value": None
                },
                {
                    "condition": "AND",
                    "key": "device_type",
                    "operator": "=",
                    "value": "sensorpush"
                }
            ]
        }
    ],
    "timeFrom": None,
    "timeShift": None,
    "title": None,
    "type": "gauge"
}

_PLOT_PANEL: typing.Dict[str, typing.Any] = {
    "aliasColors": {},
    "bars": False,
    "dashLength": 10,
    "dashes": False,
    "datasource": "${DS_INFLUXDB_DAX}",
    "fieldConfig": {
        "defaults": {
            "custom": {}
        },
        "overrides": []
    },
    "fill": 1,
    "fillGradient": 0,
    "gridPos": {
        "h": 8,
        "w": 8,
        "x": 0,
        "y": 0
    },
    "hiddenSeries": False,
    "id": 2,
    "legend": {
        "alignAsTable": False,
        "avg": False,
        "current": False,
        "max": False,
        "min": False,
        "rightSide": False,
        "show": True,
        "total": False,
        "values": False
    },
    "lines": True,
    "linewidth": 1,
    "nullPointMode": "null",
    "percentage": False,
    "pointradius": 2,
    "points": False,
    "renderer": "flot",
    "seriesOverrides": [
        {
            "alias": "Humidity",
            "yaxis": 2
        }
    ],
    "spaceLength": 10,
    "stack": False,
    "steppedLine": False,
    "targets": [
        {
            "alias": "Temperature",
            "groupBy": [],
            "measurement": "monitoring",
            "orderByTime": "ASC",
            "policy": "default",
            "refId": "A",
            "resultFormat": "time_series",
            "select": [
                [
                    {
                        "params": [
                            "temperature"
                        ],
                        "type": "field"
                    }
                ]
            ],
            "tags": [
                {
                    "key": "building",
                    "operator": "=",
                    "value": None
                },
                {
                    "condition": "AND",
                    "key": "room",
                    "operator": "=",
                    "value": None
                },
                {
                    "condition": "AND",
                    "key": "source",
                    "operator": "=",
                    "value": None
                },
                {
                    "condition": "AND",
                    "key": "device_type",
                    "operator": "=",
                    "value": "sensorpush"
                }
            ]
        },
        {
            "alias": "Humidity",
            "groupBy": [],
            "measurement": "monitoring",
            "orderByTime": "ASC",
            "policy": "default",
            "refId": "B",
            "resultFormat": "time_series",
            "select": [
                [
                    {
                        "params": [
                            "humidity"
                        ],
                        "type": "field"
                    }
                ]
            ],
            "tags": [
                {
                    "key": "building",
                    "operator": "=",
                    "value": None
                },
                {
                    "condition": "AND",
                    "key": "room",
                    "operator": "=",
                    "value": None
                },
                {
                    "condition": "AND",
                    "key": "source",
                    "operator": "=",
                    "value": None
                },
                {
                    "condition": "AND",
                    "key": "device_type",
                    "operator": "=",
                    "value": "sensorpush"
                }
            ]
        }
    ],
    "thresholds": [],
    "timeFrom": None,
    "timeRegions": [],
    "timeShift": None,
    "title": None,
    "tooltip": {
        "shared": True,
        "sort": 0,
        "value_type": "individual"
    },
    "type": "graph",
    "xaxis": {
        "buckets": None,
        "mode": "time",
        "name": None,
        "show": True,
        "values": []
    },
    "yaxes": [
        {
            "format": "celsius",
            "label": "Temperature",
            "logBase": 1,
            "max": None,
            "min": None,
            "show": True
        },
        {
            "format": "humidity",
            "label": "Humidity",
            "logBase": 1,
            "max": None,
            "min": None,
            "show": True
        }
    ],
    "yaxis": {
        "align": False,
        "alignLevel": None
    }
}


def get_argparser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(description="Tool to generate a Grafana dashboard from the sensor JSON file. "
                                                 "Outputs a JSON dashboard model that can be imported in Grafana.")
    parser.add_argument("sensor_file", type=str,
                        help="Path to JSON file used with dct_sensorpush_to_influx")
    parser.add_argument("--output-file", type=str, default="dashboard.json",
                        help="Output file (default: %(default)s)")
    return parser


def main() -> None:
    args = get_argparser().parse_args()
    sensors = json.load(open(args.sensor_file, "r"))
    output = deepcopy(_BASE_DASHBOARD)
    # keep track of x and y position as we add panels
    # x is in units of columns (grafana dashboard is divided into 24 columns)
    # y is in units of 30 pixels
    cur_x = 0
    cur_y = 0
    cur_id = itertools.count(1)
    panel = deepcopy(_CUR_TITLE_PANEL)
    panel["id"] = next(cur_id)
    output["panels"].append(panel)
    cur_y += _CUR_TITLE_PANEL["gridPos"]["h"]
    # add panels for current (latest) reading
    for sensor_name in sorted(sensors.keys()):
        tags = sensors[sensor_name]
        panel = deepcopy(_CUR_PANEL)
        panel["gridPos"]["x"] = cur_x
        panel["gridPos"]["y"] = cur_y
        panel["title"] = sensor_name
        panel["id"] = next(cur_id)
        for target in panel["targets"]:
            for target_tag in target["tags"]:
                if target_tag["key"] == "device_type":
                    continue
                target_tag["value"] = tags[target_tag["key"]]
            if tags.get("system"):
                target["tags"].append({
                    "condition": "AND",
                    "key": "system",
                    "operator": "=",
                    "value": tags.get("system")
                })
        cur_y += ((cur_x + panel["gridPos"]["w"]) // 24) * panel["gridPos"]["h"]
        # note: this assumes that the panel width is evenly divisible by 24
        # if it isn't, then stuff will end up being misaligned
        cur_x = (cur_x + panel["gridPos"]["w"]) % 24
        output["panels"].append(panel)

    panel = deepcopy(_PLOT_TITLE_PANEL)
    panel["id"] = next(cur_id)
    panel["gridPos"]["y"] = cur_y
    output["panels"].append(panel)
    cur_y += _PLOT_TITLE_PANEL["gridPos"]["h"]
    # add plot panels
    cur_x = 0
    for sensor_name in sorted(sensors.keys()):
        tags = sensors[sensor_name]
        panel = deepcopy(_PLOT_PANEL)
        panel["gridPos"]["x"] = cur_x
        panel["gridPos"]["y"] = cur_y
        panel["title"] = sensor_name
        panel["id"] = next(cur_id)
        for target in panel["targets"]:
            for target_tag in target["tags"]:
                if target_tag["key"] == "device_type":
                    continue
                target_tag["value"] = tags[target_tag["key"]]
            if tags.get("system"):
                target["tags"].append({
                    "condition": "AND",
                    "key": "system",
                    "operator": "=",
                    "value": tags.get("system")
                })
        cur_y += ((cur_x + panel["gridPos"]["w"]) // 24) * panel["gridPos"]["h"]
        # note: this assumes that the panel width is evenly divisible by 24
        # if it isn't, then stuff will end up being misaligned
        cur_x = (cur_x + panel["gridPos"]["w"]) % 24
        output["panels"].append(panel)

    # write to file
    with open(args.output_file, "w") as outfile:
        json.dump(output, outfile)


if __name__ == '__main__':
    main()
