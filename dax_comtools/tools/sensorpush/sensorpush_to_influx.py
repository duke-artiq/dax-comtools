#!/usr/bin/env python3

import argparse
import logging
import requests
import typing
import json

from sipyco.common_args import verbosity_args, init_logger_from_args

from dax_comtools.util.args import influx_args
from dax_comtools.util.influx_db import InfluxDBUtil

logger = logging.getLogger(__name__)
"""Module logger."""


class SensorPushLogger:
    """Log SensorPush data to InfluxDB."""

    def __init__(self, email: str, password: str, sensor_file: str, **kwargs: typing.Any):
        # open sensor file, allow exception to be thrown if it doesn't exist
        with open(sensor_file, "r") as file:
            self._sensors = json.load(file)
        self._access_token = self.get_access_token(email, password)
        self._sensor_ids = self.get_sensor_ids()
        self._influx = InfluxDBUtil(**kwargs)

    @staticmethod
    def get_access_token(email: str, password: str) -> str:
        """
        Use provided email and password to retrieve oauth access token from SensorPush. Accesses the `/oauth/authorize`
        and `/oauth/accesstoken` API endpoints.
        :param email: SensorPush email
        :param password: SensorPush password
        :return: access token
        """
        # first get authorization code
        headers = {
            "accept": "application/json",
            "Content-Type": "application/json"
        }
        data = f'{{"email": "{email}", "password": "{password}"}}'
        response = requests.post("https://api.sensorpush.com/api/v1/oauth/authorize", headers=headers, data=data)
        if response.status_code != 200:
            response.raise_for_status()
        auth_code = json.loads(response.content.decode("utf-8")).get("authorization")

        # now use auth code to get oauth access token
        data = f'{{"authorization": "{auth_code}"}}'
        response = requests.post("https://api.sensorpush.com/api/v1/oauth/accesstoken", headers=headers, data=data)
        if response.status_code != 200:
            response.raise_for_status()
        access_token = json.loads(response.content.decode("utf-8")).get("accesstoken")

        return access_token

    def list_gateways(self) -> dict:
        """
        Retrieves gateway list from the `/devices/gateways` API endpoint.
        :return: dictionary from JSON response
        """
        headers = {
            "accept": "application/json",
            "authorization": self._access_token
        }
        response = requests.post("https://api.sensorpush.com/api/v1/devices/gateways", headers=headers, data="{}")
        if response.status_code != 200:
            response.raise_for_status()

        return json.loads(response.content.decode("utf-8"))

    def list_sensors(self) -> dict:
        """
        Retrieves sensor list from the `/devices/sensors` API endpoint.
        :return: dictionary from JSON response
        """
        headers = {
            "accept": "application/json",
            "authorization": self._access_token
        }
        response = requests.post("https://api.sensorpush.com/api/v1/devices/sensors", headers=headers, data="{}")
        if response.status_code != 200:
            response.raise_for_status()

        return json.loads(response.content.decode("utf-8"))

    def get_samples(self, sensors: typing.Optional[typing.List[str]] = None, start_time: typing.Optional[str] = None,
                    stop_time: typing.Optional[str] = None, limit: int = 10) -> dict:
        """
        Retrieves samples from the `/samples` API endpoint.
        :param sensors: list of sensor IDs to obtain samples from. If None (default), will retrieve data from all
        active sensors.
        :param start_time: beginning of period over which to retrieve samples. If None (default), the most recent
        samples will be retrieved.
        :param stop_time: end of period over which to retrieve samples. If None (default), the most recent
        samples will be retrieved.
        :param limit: maximum number of samples to retrieve. Defaults to 10.
        :return: dictionary from JSON response
        """
        headers = {
            "accept": "application/json",
            "authorization": self._access_token
        }
        # need to do some extra formatting for the sensor list to send
        sensor_field = f'''"sensors": ["{'", "'.join(sensors) if sensors is not None else ""}"], '''
        data = f'{{{sensor_field if sensors else ""}"startTime": "{start_time if start_time else ""}", "stopTime": ' \
               f'"{stop_time if stop_time else ""}", "limit": {limit:d}}}'
        response = requests.post("https://api.sensorpush.com/api/v1/samples", headers=headers, data=data)
        if response.status_code != 200:
            response.raise_for_status()

        return json.loads(response.content.decode("utf-8"))

    def get_sensor_ids(self) -> dict:
        """
        Retrieve sensor list and match provided names (via JSON file) with sensor IDs.
        :return: dictionary of sensor_id:sensor_name pairs
        """
        # list sensors
        sensor_list = self.list_sensors()
        # check that all names are unique
        name_set = {sensor_list[k]["name"] for k in sensor_list.keys()}
        # not sure if it's possible for multiple sensors to have the same name, but better check just in case
        if len(name_set) != len(sensor_list):
            raise ValueError("Duplicate names present in SensorPush sensor list.")
        # create the dictionary to return
        sensor_ids = {}
        for k, v in sensor_list.items():
            # check if sensor name is in user's json file
            name = v["name"]
            if name in self._sensors:
                # add entry to sensor_ids dict
                sensor_ids[k] = name
            else:
                logger.warning("Sensor '%s' not found in provided JSON file", name)

        # check whether all sensors in json file were found
        not_found = [name for name in self._sensors.keys() if name not in name_set]
        if not_found:
            logger.warning("The following sensors provided in the JSON file were not found in SensorPush:\t%s",
                           str(not_found))
        return sensor_ids

    def sample_all_sensors(self, start_time: typing.Optional[str] = None, stop_time: typing.Optional[str] = None,
                           limit: int = 10) -> dict:
        """
        Get samples for all sensors in the provided JSON file (simple wrapper for `get_samples()`).
        :param start_time: beginning of period over which to retrieve samples. If None (default), the most recent
        samples will be retrieved.
        :param stop_time: end of period over which to retrieve samples. If None (default), the most recent
        samples will be retrieved.
        :param limit: maximum number of samples to retrieve. Defaults to 10.
        :return: dictionary from JSON response
        """
        return self.get_samples(sensors=list(self._sensor_ids.keys()), start_time=start_time, stop_time=stop_time,
                                limit=limit)

    def log_all_sensors(self, start_time: typing.Optional[str] = None, stop_time: typing.Optional[str] = None,
                        limit: int = 10) -> None:
        """
        Get samples for all sensors in the provided JSON file and log the data to InfluxDB. Point is written with the
        tags in the provided JSON file, and the following fields:
        - sensor_name: the name of the sensor in SensorPush
        - sensor_id: the SensorPush ID of the sensor
        - temperature: the temperature reading
        - humidity: the humidity reading
        :param start_time: beginning of period over which to retrieve samples. If None (default), the most recent
        samples will be retrieved.
        :param stop_time: end of period over which to retrieve samples. If None (default), the most recent
        samples will be retrieved.
        :param limit: maximum number of samples to retrieve. Defaults to 10.
        """
        data = self.sample_all_sensors(start_time=start_time, stop_time=stop_time, limit=limit)
        # hard code device type to 'sensorpush'
        device_type = "sensorpush"
        for sensor_id in data["sensors"]:
            name = self._sensor_ids[sensor_id]
            tags = self._sensors[name]
            source = tags["source"]
            building = tags["building"]
            room = tags["room"]
            system = tags.get("system")
            for point in data["sensors"][sensor_id]:
                # convert to celsius because science
                temp = (float(point["temperature"]) - 32) * 5 / 9
                self._influx.write_point_monitoring(source, device_type, building, room, system=system,
                                                    timestamp=point["observed"], sensor_name=name, sensor_id=sensor_id,
                                                    temperature=temp, humidity=float(point["humidity"]))

    def close(self) -> None:
        """Close all connections."""
        self._influx.close()


def get_argparser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(description="SensorPush data logger")
    verbosity_args(parser)
    influx_args(parser)

    sp = parser.add_argument_group("SensorPush arguments")
    sp.add_argument("email", type=str, help="Email address for SensorPush login")
    sp.add_argument("password", type=str, help="Password for SensorPush")
    sp.add_argument("sensor_file", type=str,
                    help="JSON file containing SensorPush sensor names and their associated tags for InfluxDB. Format "
                         "should be e.g.: {'<sensor_name_1>': {'<tag_name>':'<tagvalue>', ...}, '<sensor_name_2>': "
                         "{'<tag_name>':'<tag_value>', ...}} See dax_comtools.util.influx_db for a list of required and"
                         " optional tags.")
    sp.add_argument("--start-time", type=str, help="Beginning of period over which to retrieve samples. If not provided"
                                                   ", the most recent samples will be retrieved.")
    sp.add_argument("--stop-time", type=str, help="End of period over which to retrieve samples. If not provided"
                                                  ", the most recent samples will be retrieved.")
    sp.add_argument("--limit", type=int, default=10, help="Number of samples to retrieve (default: %(default)s)")

    other = parser.add_argument_group("Other options")
    other.add_argument("--buffer-size", type=int, default=10000,
                       help='The buffer size for writes to InfluxDB (default: %(default)s)')

    return parser


def main() -> None:
    args = get_argparser().parse_args()
    init_logger_from_args(args)

    sp = SensorPushLogger(
        email=args.email, password=args.password, sensor_file=args.sensor_file,
        db=args.db, server_db=args.server_db, port_db=args.port_db,
        user_db=args.user_db, password_db=args.password_db,
        buffer_size=args.buffer_size
    )
    try:
        sp.log_all_sensors(start_time=args.start_time, stop_time=args.stop_time, limit=args.limit)
    finally:
        sp.close()


if __name__ == '__main__':
    main()
