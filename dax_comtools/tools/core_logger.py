#!/usr/bin/env python3

"""
UART logger for ARTIQ core devices.
Works for Kasli. The KC705 does not have a UART serial output.
"""

import argparse
import logging
import typing
import datetime
import time
import os
import os.path
import serial
import serial.tools.list_ports

import dax_comtools.util.args

logger = logging.getLogger(__name__)
"""The module logger."""


class CoreLogger:
    """UART logger for an ARTIQ core device."""

    DEFAULT_BAUD_RATE: typing.ClassVar[int] = 115200
    """Default baud rate."""
    DEFAULT_OUTPUT_DIR: typing.ClassVar[str] = './'
    """Default output directory."""
    DEFAULT_RECONNECT_DELAY: typing.ClassVar[float] = 5.0
    """Default delay before reconnecting."""

    def __init__(self, *,
                 port: str,
                 baud_rate: int = DEFAULT_BAUD_RATE,
                 output_dir: str = DEFAULT_OUTPUT_DIR,
                 output_prefix: str = ''):
        """Initialize the core logger class.

        :param port: The port to connect to
        :param baud_rate: Baud rate
        :param output_dir: Output directory for the log files
        :param output_prefix: Prefix for the log filename
        """
        assert isinstance(port, str)
        assert isinstance(baud_rate, int)
        assert isinstance(output_dir, str)
        assert isinstance(output_prefix, str)

        # Validate baud rate
        if baud_rate not in serial.SerialBase.BAUDRATES:
            raise ValueError('Invalid baud rate')

        # Expand path and validate existence
        self._output_dir = os.path.expanduser(output_dir)
        if not os.path.isdir(self._output_dir):
            raise NotADirectoryError(f'"{self._output_dir}" is not a valid directory')

        # Store output prefix
        self._output_prefix = output_prefix

        # Create serial port
        logger.info(f'Opening serial connection to "{port}"')
        self._serial = serial.Serial(
            port=port, baudrate=baud_rate,
            bytesize=serial.EIGHTBITS, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE
        )
        logger.debug(f'Serial: {self._serial}')

    def start(self) -> None:
        """Start logging."""

        # Generate file name
        file_name = os.path.join(self._output_dir,
                                 datetime.datetime.now().strftime(f'{self._output_prefix}%Y-%m-%d_%H:%M:%S.log'))
        logger.info(f'Log file: {file_name}')

        with open(file_name, mode='w', buffering=1) as file, self._serial:
            while True:
                # Get, print, and write the line
                line = self._serial.readline().decode(encoding='utf8', errors='ignore')
                line = f'[{datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}] {line}'
                print(line, end='', flush=True)
                file.write(line)

    @classmethod
    def run(cls, *, reconnect_delay: float, **kwargs: typing.Any) -> None:
        """Run this core logger class in an infinite loop that reconnects if the connection is lost.

        :param reconnect_delay: Delay before reconnecting in seconds
        :param kwargs: Keyword arguments for the constructor of :class:`CoreLogger`
        """
        assert isinstance(reconnect_delay, float)
        assert reconnect_delay > 0.0

        try:
            while True:
                try:
                    # Create the core logger
                    core_logger = cls(**kwargs)
                except serial.SerialException as e:
                    logger.warning(f'Connection failed: {e}')
                else:
                    try:
                        # Start logging
                        core_logger.start()
                    except serial.SerialException as e:
                        logger.warning(f'Connection lost: {e}')

                # Insert delay
                logger.info(f'Waiting to reconnect ({reconnect_delay} s)...')
                time.sleep(reconnect_delay)

        except ValueError:
            logger.exception('Value error, logger terminated')
        except OSError:
            logger.exception('IO error, logger terminated')
        except KeyboardInterrupt:
            logger.info('Core logger stopped by user')


def _get_args() -> argparse.Namespace:
    # Create an argument parser
    parser = argparse.ArgumentParser(
        description="UART logger for ARTIQ core devices (Kasli)",
        epilog="The USB connection on the Kasli presents itself as a quad-RS232 device.\n"
               "The serial port is on channel 1/2 for v1.0/v1.1 hardware (e.g. /dev/ttyUSB2 for v1.1 hardware).\n"
               "Source: https://www.m-labs.hk/artiq/manual/"
    )

    def baud_rate(arg: typing.Any) -> int:
        try:
            b = int(arg)
        except ValueError:
            raise argparse.ArgumentTypeError(f"invalid int value: '{arg}'")
        if b not in serial.SerialBase.BAUDRATES:
            raise argparse.ArgumentTypeError(f'not a valid baud rate: {b}')
        return b

    def positive_float(arg: typing.Any) -> float:
        try:
            f = float(arg)
        except ValueError:
            raise argparse.ArgumentTypeError(f"invalid float value: '{arg}'")
        if not f > 0.0:
            raise argparse.ArgumentTypeError(f'float value must be positive: {f}')
        return f

    group = parser.add_argument_group('Core device')
    group.add_argument('port', type=str, help='device to connect to')
    group.add_argument('--usb', action='store_true', help='pass the port as a USB location instead of a port')
    group.add_argument('--baud-rate', type=baud_rate, default=CoreLogger.DEFAULT_BAUD_RATE,
                       help='baud rate (default: %(default)s)')

    group = parser.add_argument_group('Output')
    group.add_argument('--output-dir', type=str, default=CoreLogger.DEFAULT_OUTPUT_DIR,
                       help='output directory (default: "%(default)s")')
    group.add_argument('--output-prefix', type=str, default='', help='prefix for log output filename')

    group = parser.add_argument_group('Connection')
    group.add_argument('--reconnect-delay', type=positive_float, default=CoreLogger.DEFAULT_RECONNECT_DELAY,
                       help='delay before reconnecting in seconds (default: %(default)s)')

    # Add standard arguments
    dax_comtools.util.args.logger_args(parser)

    return parser.parse_args()


def _usb_to_port(usb: str, *, interface: str = '1.2') -> str:
    """Convert a USB location to a serial port.

    :param usb: USB location (e.g. 1-13.1)
    :param interface: The interface to use for this USB location (defaults to interface for Kasli>=1.1)
    """
    # Get all ports
    ports = serial.tools.list_ports.comports()
    # Filter ports by given USB location
    ports = (p for p in ports if p.location and p.location.startswith(usb))
    # Filter ports by USB interface (for Kasli>=1.1)
    ports = [p for p in ports if p.location.endswith(f':{interface}')]

    if not ports:
        raise RuntimeError(f'No port could be found based on USB location {usb}')
    elif len(ports) > 1:
        raise RuntimeError(f'Found multiple ports based on USB location {usb}')
    else:
        return ports[0].device


def main() -> None:
    # Get the arguments
    args = _get_args()
    # Configure logger
    dax_comtools.util.args.process_logger_args(args)

    # Port to log
    port = _usb_to_port(args.port) if args.usb else args.port

    # Run the core logger
    CoreLogger.run(port=port, baud_rate=args.baud_rate,
                   output_dir=args.output_dir, output_prefix=args.output_prefix,
                   reconnect_delay=args.reconnect_delay)


if __name__ == '__main__':
    main()
