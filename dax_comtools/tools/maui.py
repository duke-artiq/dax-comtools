#!/usr/bin/env python3

"""
Teledyne MAUI Wavesurfer 3104Z - Simple Query Logger
"""

import argparse
import typing

from dax_comtools.devices.maui.driver import MAUI
from dax_comtools.util.influx_db import InfluxDBUtil
from dax_comtools.util.args import influx_args, get_influx_args, monitoring_tags_args, get_monitoring_tags_args, \
    logger_args, process_logger_args

_FIELDS: typing.Mapping[str, typing.Callable[..., float]] = {
    'freq': MAUI.get_freq,
    'amp': MAUI.get_amp,
}


def get_argparser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(description="Teledyne Wavesurfer 3104Z reader for InfluxDB")
    parser.add_argument("device", type=str, help="PyVISA resource string of device to connect to")
    parser.add_argument("--timeout", type=int, default=MAUI.DEFAULT_TIMEOUT,
                        help="connection timeout in milliseconds (default: %(default)s)")
    parser.add_argument("--dry-run", "-n", action="store_true", help="read data but do not write to influx DB")

    logger_args(parser)
    influx_args(parser)
    monitoring_tags_args(parser, device_type="maui", required=True)

    return parser


def main():
    parser = get_argparser()
    parser.add_argument("channel", type=int, choices=sorted(MAUI.CHANNELS), help="the target channel on the device")
    parser.add_argument("fields", type=str, nargs='+', choices=sorted(_FIELDS), help="fields to log")

    args = parser.parse_args()
    process_logger_args(args)

    with MAUI(args.device, timeout=args.timeout) as maui:
        values = {f: _FIELDS[f](maui, args.channel) for f in set(args.fields)}
        assert values
        assert all(isinstance(v, float) for v in values.values())

        if not args.dry_run:
            # write data to influx DB
            with InfluxDBUtil(**get_influx_args(args)) as influx:
                influx.write_point_monitoring(**get_monitoring_tags_args(args), **values)


def cust_stat():
    parser = get_argparser()
    parser.add_argument("slot", type=int, choices=sorted(MAUI.SLOTS), help="the target slot on the device")
    parser.add_argument("stats", type=str, nargs='+', choices=sorted(MAUI.STATS), help="custom statistics to log")

    args = parser.parse_args()
    process_logger_args(args)

    with MAUI(args.device, timeout=args.timeout) as maui:
        values = {k: v for k, v in maui.get_cust_stat(args.slot).items() if k in set(args.stats)}
        assert values
        assert all(isinstance(v, float) for v in values.values())

        if not args.dry_run:
            # write data to influx DB
            with InfluxDBUtil(**get_influx_args(args)) as influx:
                influx.write_point_monitoring(**get_monitoring_tags_args(args), **values)


if __name__ == '__main__':
    main()
