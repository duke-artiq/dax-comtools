#!/usr/bin/env python3

import argparse
import logging
import typing
import time

from dax_comtools.devices.am23xx.adafruit_am2320 import AM2320, AM2320_DEFAULT_ADDR
from dax_comtools.util.influx_db import InfluxDBUtil
from dax_comtools.util.args import influx_args, get_influx_args, monitoring_tags_args, get_monitoring_tags_args, \
    logger_args, process_logger_args, str_to_hex

logger = logging.getLogger(__name__)


def get_argparser() -> argparse.ArgumentParser:
    # Argument parser
    parser = argparse.ArgumentParser(description='AM23xx temperature/humidity sensor reader for InfluxDB')

    # Arguments
    am23xx = parser.add_argument_group('AM23xx arguments')
    am23xx.add_argument('i2c', type=str,
                        help='path to the I2C bus (e.g. "/dev/i2c-1")')
    am23xx.add_argument('--address', default=AM2320_DEFAULT_ADDR, type=str_to_hex,
                        help='hex I2C address of the sensor (default: %(default)#x)')
    parser.add_argument('--dry-run', '-n', action='store_true', help='read data but do not write to influx DB')
    am23xx.add_argument('--attempts', default=1, type=int,
                        help='number of read attempts for each value (default: %(default)s)')
    am23xx.add_argument('--setup-delay', default=0.0, type=float,
                        help='setup delay before read in seconds (default: %(default)s)')

    # InfluxDB, monitoring, and logger arguments
    influx_args(parser)
    monitoring_tags_args(parser, device_type='am23xx', required=True)
    logger_args(parser)

    # Return parser
    return parser


def main() -> None:
    """Retrieve data from an AD23xx temperature/humidity sensor and write results to InfluxDB."""
    # Parse arguments
    args = get_argparser().parse_args()
    process_logger_args(args)  # This initializes logging system log level according to -v/-q args

    # Check arguments
    if args.attempts < 1:
        raise ValueError('Number of attempts can not be less than one')
    if args.setup_delay < 0.0:
        raise ValueError('Setup delay time can not be less than zero')

    # Initialize driver
    logger.debug('I2C bus %s', args.i2c)
    logger.debug('I2C address %#x', args.address)
    sensor = AM2320(args.i2c, address=args.address)

    # Initialize values
    temperature: typing.Optional[float] = None
    humidity: typing.Optional[float] = None

    # Read temperature
    for i in range(args.attempts):
        logger.debug(f'Temperature read attempt {i} (after delay {args.setup_delay}s)')
        time.sleep(args.setup_delay)
        try:
            temperature = sensor.temperature
        except OSError:
            logger.debug(f'Temperature read failed (attempt {i})')
        else:
            break
    else:
        logger.error(f'Temperature read failed after {args.attempts} attempt(s)')
        exit(1)

    # Read humidity
    for i in range(args.attempts):
        logger.debug(f'Humidity read attempt {i} (after delay {args.setup_delay}s)')
        time.sleep(args.setup_delay)
        try:
            humidity = sensor.relative_humidity
        except OSError:
            logger.debug(f'Humidity read failed (attempt {i})')
        else:
            break
    else:
        logger.error(f'Humidity read failed after {args.attempts} attempt(s)')
        exit(1)

    # Verify values
    assert isinstance(temperature, float), 'Returned temperature is not of type float'
    assert isinstance(humidity, float), 'Returned humidity is not of type float'
    logger.info('Temperature: %.2f C', temperature)
    logger.info('Relative humidity: %.2f %%', humidity)

    if not args.dry_run:
        # Write data to influx DB
        with InfluxDBUtil(**get_influx_args(args)) as influx:
            influx.write_point_monitoring(**get_monitoring_tags_args(args), temperature=temperature, humidity=humidity)


if __name__ == '__main__':
    main()
