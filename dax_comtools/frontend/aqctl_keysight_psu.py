import argparse
import logging
import typing

from sipyco.common_args import (
    simple_network_args,
    verbosity_args,
    init_logger_from_args,
    bind_address_from_args,
)
from sipyco.pc_rpc import simple_server_loop
from dax_comtools.devices.keysight_psu.driver import KeysightPowerSupplyDriver

logger = logging.getLogger(__name__)


def get_argparser() -> argparse.ArgumentParser:
    """Return argument parser with necessary arguments for PSU driver."""
    parser = argparse.ArgumentParser(description="Agilent DC Power Supply controller")
    simple_network_args(parser, 3285)
    verbosity_args(parser)  # This adds the -q and -v handling
    subparsers = parser.add_subparsers(
        title="interface type subcommands",
        help="sub-command help",
    )  # Subcommand parsers)
    parser.add_argument(
        "device",
        type=str,
        help="Device to use, e.g. 'ASRL/dev/ttyUSBX' for rs232"
        "or TCPIP[board]::host address::port::SOCKET for TCPIP etc.",
    )
    parser.add_argument(
        "--model",
        type=str,
        default="",
        help="Agilent PSU model number, e.g. 'E3631A' or 'E3633A'",
    )
    parser.add_argument(
        "--bypass-remote-mode",
        action="store_true",
        help="Bypass setting device in remote mode",
    )
    parser.add_argument(
        "--connection-optional",
        action="store_true",
        help="Suppresses exceptions related to disconnected or unreachable device. "
        "Logs warnings instead.",
    )
    parser.add_argument(
        "--disable-on-close",
        action="store_true",
        help="Disable all outputs when the controller is closed",
    )
    parser.add_argument(
        "-s", "--simulation", action="store_true", help="Run in simulation mode"
    )
    parser.add_argument(
        "--timeout",
        type=int,
        default=5000,
        help="Set timeout parameter passed to device connection (ms)",
    )
    # Subcommand functinality for RS-232 interface connection
    rs232_parser = subparsers.add_parser(
        "rs232",
        help="Subcommand for serial RS-232 connection",
    )
    rs232_parser.add_argument(
        "--baudrate",
        default=max(KeysightPowerSupplyDriver.VALID_BAUD_RATES),
        type=int,
        choices=sorted(KeysightPowerSupplyDriver.VALID_BAUD_RATES),
        help="Serial connection baud rate (default: %(default)s)",
    )
    # Subcommand functinality for TCPIP interface connection
    tcpip_parser = subparsers.add_parser(  # noqa: F841
        "tcpip",
        help="Subcommand for TCPIP connection",
    )
    # Subcommand functinality for GPIB interface connection
    gpib_parser = subparsers.add_parser(  # noqa: F841
        "gpib",
        help="Subcommand for GPIB connection",
    )
    # Subcommand functinality for USB interface connection
    usb_parser = subparsers.add_parser(  # noqa: F841
        "usb",
        help="Subcommand for USB connection",
    )

    return parser


def collect_subcommand_kwargs(args: argparse.Namespace) -> typing.Dict[str, str]:
    """ Collects kwargs from subcommand options if specified. """
    subcommand_kwargs: typing.Dict[str, str] = {}
    subcommand_kwargs["timeout"] = args.timeout
    # RS-232 subcommand
    if "rs232" in args:
        subcommand_kwargs["baud_rate"] = args.baud_rate

    return subcommand_kwargs


def main():
    """Start an RPC server for the controller."""
    args = get_argparser().parse_args()
    init_logger_from_args(
        args
    )  # This initializes logging system log level according to -v/-q args

    # Collect kwargs from specified subcommands
    subcommand_kwargs = collect_subcommand_kwargs(args)

    try:
        driver = KeysightPowerSupplyDriver(
            device=args.device,
            model=args.model,
            bypass_remote_mode=args.bypass_remote_mode,
            connection_optional=args.connection_optional,
            simulation=args.simulation,
            **subcommand_kwargs,
        )
    except ValueError as e:
        raise ValueError(
            "Could not initialize driver, please provide valid command-line arguments"
        ) from e

    logger.info("Starting controller on port %i", args.port)
    try:
        simple_server_loop(
            {"KeysightPowerSupply": driver}, bind_address_from_args(args), args.port
        )
    except OSError as e:
        raise OSError(f"Network port {args.port} is probably already in use") from e
    except KeyboardInterrupt:
        pass
    finally:
        if args.disable_on_close:
            if driver.is_online():
                logger.info("Disabling all outputs (on close)")
                # noinspection PyBroadException
                try:
                    # Disable all outputs, ignore any exceptions
                    driver.enable_outputs(False)
                except:  # noqa: E722
                    logger.exception(
                        "Exception occurred while disabling outputs on close"
                    )
            else:
                logger.warning(
                    "Could not disable all outputs (on close), device is offline"
                )

        # Close the PSU driver
        driver.close()
        logger.info("Shut down controller")


if __name__ == "__main__":
    main()
