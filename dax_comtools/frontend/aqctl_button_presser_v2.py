#!/usr/bin/env python3

from dax_comtools.devices.button_presser.driver import ButtonPresserDriverV2
import argparse
import logging

from sipyco.common_args import simple_network_args, verbosity_args, init_logger_from_args, bind_address_from_args
from sipyco.pc_rpc import simple_server_loop
import sys
sys.path.append('..')

logger = logging.getLogger(__name__)


def get_argparser() -> argparse.ArgumentParser:
    """Return argument parser with necessary arguments for driver."""
    parser = argparse.ArgumentParser(description="Button presser controller")
    simple_network_args(parser, 3275)
    verbosity_args(parser)  # This adds the -q and -v handling
    parser.add_argument("device", type=str,
                        help="Serial device to use, e.g. 'COMxx' on Windows or '/dev/ttyUSBx' on Linux")
    parser.add_argument("--baudrate", type=int, default=ButtonPresserDriverV2.DEFAULT_BAUD_RATE,
                        help="Connection baud rate (default: %(default)s)")
    parser.add_argument("--recurring_update", type=bool, default=False,
                        help="Connection baud rate (default: %(default)s)")
    parser.add_argument("--interval", type=int, default=1.0,
                        help="Connection baud rate (default: %(default)s)")
    parser.add_argument("-s", "--simulation", action="store_true",
                        help="Run in simulation mode")
    return parser


def main():
    """Start an RPC server for the controller."""
    args = get_argparser().parse_args()
    init_logger_from_args(args)  # This initializes logging system log level according to -v/-q args

    try:
        driver = ButtonPresserDriverV2(device=args.device, baud_rate=args.baudrate, simulation=args.simulation,
                                       recurring_update=args.recurring_update, write_interval=args.interval)
    except ValueError as e:
        raise ValueError("Could not initialize driver, please provide valid command-line arguments") from e

    logger.info("Starting controller on port %i", args.port)
    try:
        simple_server_loop({"ButtonPresser": driver}, bind_address_from_args(args), args.port)
    except OSError as e:
        raise OSError(f"Network port {args.port} is probably already in use") from e
    except KeyboardInterrupt:
        pass
    finally:
        driver.close()
        logger.info("Shut down controller")


if __name__ == "__main__":
    main()
