#!/usr/bin/env python3

import argparse
import logging

from sipyco.common_args import simple_network_args, verbosity_args, init_logger_from_args, bind_address_from_args
from sipyco.pc_rpc import simple_server_loop
from dax_comtools.devices.influx_db.driver import InfluxDBDriver
from dax_comtools.util.args import influx_args

logger = logging.getLogger(__name__)


def get_argparser() -> argparse.ArgumentParser:
    """Return argument parser with necessary arguments for driver."""
    parser = argparse.ArgumentParser(description="InfluxDB controller")
    simple_network_args(parser, 3277)
    verbosity_args(parser)  # This adds the -q and -v handling
    influx_args(parser)  # add InfluxDB connection arguments
    parser.add_argument("-s", "--simulation", action="store_true",
                        help="Run in simulation mode")
    return parser


def main():
    """Start an RPC server for the controller."""
    args = get_argparser().parse_args()
    init_logger_from_args(args)  # This initializes logging system log level according to -v/-q args

    try:
        driver = InfluxDBDriver(args.db, args.server_db, args.port_db,
                                args.user_db, args.password_db, simulation=args.simulation)
    except ValueError as e:
        raise ValueError("Could not initialize driver, please provide valid command-line arguments") from e

    logger.info("Starting controller on port %i", args.port)
    try:
        simple_server_loop({"InfluxDB": driver}, bind_address_from_args(args), args.port)
    except OSError as e:
        raise OSError(f"Network port {args.port} is probably already in use") from e
    except KeyboardInterrupt:
        pass
    finally:
        driver.close()
        logger.info("Shut down controller")


if __name__ == "__main__":
    main()
