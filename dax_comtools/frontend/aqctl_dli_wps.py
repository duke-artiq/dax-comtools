#!/usr/bin/env python3

import argparse
import logging

from sipyco.common_args import simple_network_args, verbosity_args, init_logger_from_args, bind_address_from_args
from sipyco.pc_rpc import simple_server_loop
from dax_comtools.devices.dli_wps.driver import WPS

logger = logging.getLogger(__name__)


def get_argparser() -> argparse.ArgumentParser:
    """Return argument parser with necessary arguments for driver."""
    parser = argparse.ArgumentParser(description="Web Power Switch controller")
    simple_network_args(parser, 3289)
    verbosity_args(parser)  # This adds the -q and -v handling
    parser.add_argument("address", type=str,
                        help="IP address of the device")
    parser.add_argument("user", type=str,
                        help="Username for device authentication")
    parser.add_argument("password", type=str,
                        help="Password for device authentication")
    parser.add_argument("--num-outlets", type=int, default=WPS.DEFAULT_NUM_OUTLETS,
                        help="number of available outlets on the device (default: %(default)s)")
    parser.add_argument("--retries", type=int, default=WPS.DEFAULT_RETRIES,
                        help="retry connections in case of exception (default: %(default)s)")
    parser.add_argument("--disable-on-close", action="store_true",
                        help="disable all outputs when the controller is closed")
    parser.add_argument("-s", "--simulation", action="store_true",
                        help="run in simulation mode")
    return parser


def main():
    """Start an RPC server for the controller."""
    args = get_argparser().parse_args()
    init_logger_from_args(args)  # This initializes logging system log level according to -v/-q args

    # Create driver
    driver = WPS(args.address, args.user, args.password, num_outlets=args.num_outlets, retries=args.retries,
                 simulation=args.simulation)

    logger.info("Starting controller on port %i", args.port)
    try:
        simple_server_loop({"WPS": driver}, bind_address_from_args(args), args.port)
    except OSError as e:
        raise OSError(f"Network port {args.port} is probably already in use") from e
    except KeyboardInterrupt:
        pass
    finally:
        if args.disable_on_close:
            logger.info("Disabling all outputs (on close)")
            # noinspection PyBroadException
            try:
                driver.switch_all_off()
            except:  # noqa: E722
                logger.exception("Exception occurred while disabling outputs on close")

        logger.info("Shut down controller")


if __name__ == "__main__":
    main()
