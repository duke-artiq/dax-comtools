#!/usr/bin/env python3

import argparse
import logging

from sipyco.common_args import simple_network_args, verbosity_args, init_logger_from_args, bind_address_from_args
from sipyco.pc_rpc import simple_server_loop
from dax_comtools.devices.agilent_psu.driver import AgilentPowerSupplyDriver

logger = logging.getLogger(__name__)


def get_argparser() -> argparse.ArgumentParser:
    """Return argument parser with necessary arguments for driver."""
    parser = argparse.ArgumentParser(description="Agilent E3631A DC Power Supply controller")
    simple_network_args(parser, 3285)
    verbosity_args(parser)  # This adds the -q and -v handling
    parser.add_argument("device", type=str,
                        help="Serial device to use, e.g. 'COMxx' on Windows or '/dev/ttyUSBx' on Linux")
    parser.add_argument("--model", type=str, default='',
                        help="Agilent PSU model number, e.g. 'E3631A' or 'E3633A'")
    default_baud_rate = max(AgilentPowerSupplyDriver.VALID_BAUD_RATES)
    parser.add_argument("--baudrate", type=int, default=default_baud_rate,
                        choices=sorted(AgilentPowerSupplyDriver.VALID_BAUD_RATES),
                        help="Connection baud rate (default: %(default)s)")
    parser.add_argument("--bypass-remote-mode", action="store_true", help="Bypass setting device in remote mode")
    parser.add_argument("--connection-optional", action="store_true",
                        help="Suppresses exceptions related to disconnected or unreachable device. "
                             "Logs warnings instead.")
    parser.add_argument("--disable-on-close", action="store_true",
                        help="Disable all outputs when the controller is closed")
    parser.add_argument("-s", "--simulation", action="store_true",
                        help="Run in simulation mode")
    return parser


def main():
    """Start an RPC server for the controller."""
    args = get_argparser().parse_args()
    init_logger_from_args(args)  # This initializes logging system log level according to -v/-q args

    try:
        driver = AgilentPowerSupplyDriver(device=args.device, model=args.model, baud_rate=args.baudrate,
                                          bypass_remote_mode=args.bypass_remote_mode,
                                          connection_optional=args.connection_optional, simulation=args.simulation)
    except ValueError as e:
        raise ValueError("Could not initialize driver, please provide valid command-line arguments") from e

    logger.info("Starting controller on port %i", args.port)
    try:
        simple_server_loop({"AgilentPowerSupply": driver}, bind_address_from_args(args), args.port)
    except OSError as e:
        raise OSError(f"Network port {args.port} is probably already in use") from e
    except KeyboardInterrupt:
        pass
    finally:
        if args.disable_on_close:
            if driver.is_online():
                logger.info("Disabling all outputs (on close)")
                # noinspection PyBroadException
                try:
                    # Disable all outputs, ignore any exceptions
                    driver.enable_outputs(False)
                except:  # noqa: E722
                    logger.exception("Exception occurred while disabling outputs on close")
            else:
                logger.warning("Could not disable all outputs (on close), device is offline")

        # Close the driver
        driver.close()
        logger.info("Shut down controller")


if __name__ == "__main__":
    main()
