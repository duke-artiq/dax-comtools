#!/usr/bin/env python3

import argparse
import logging

from sipyco.common_args import simple_network_args, verbosity_args, init_logger_from_args, bind_address_from_args
from sipyco.pc_rpc import simple_server_loop
from dax_comtools.devices.smart_strip import SmartPowerStrip
from dax_comtools.devices.smart_strip.sim import SmartPowerStripSim

logger = logging.getLogger(__name__)


def get_argparser() -> argparse.ArgumentParser:
    """Return argument parser with necessary arguments for driver."""
    parser = argparse.ArgumentParser(description="TP-Link Kasa Smart Power Strip HS300 controller")
    simple_network_args(parser, 3288)
    verbosity_args(parser)  # This adds the -q and -v handling
    parser.add_argument("address", type=str,
                        help="IP address of the device")
    parser.add_argument("--device-id", type=str, default=None,
                        help="Device ID")
    parser.add_argument("--timeout", type=float, default=2.0,
                        help="Connection timeout in seconds (default: %(default)s)")
    parser.add_argument("--protocol", type=str, default="tcp", choices={"tcp", "udp"},
                        help="Communication protocol (default: %(default)s)")
    parser.add_argument("-s", "--simulation", action="store_true",
                        help="run in simulation mode")
    return parser


def main():
    """Start an RPC server for the controller."""
    args = get_argparser().parse_args()
    init_logger_from_args(args)  # This initializes logging system log level according to -v/-q args

    # Create driver
    driver_cls = SmartPowerStripSim if args.simulation else SmartPowerStrip
    driver = driver_cls(ip=args.address, device_id=args.device_id, timeout=args.timeout, protocol=args.protocol)

    logger.info("Starting controller on port %i", args.port)
    try:
        simple_server_loop({"SmartStrip": driver}, bind_address_from_args(args), args.port)
    except OSError as e:
        raise OSError(f"Network port {args.port} is probably already in use") from e
    except KeyboardInterrupt:
        pass
    finally:
        logger.info("Shut down controller")


if __name__ == "__main__":
    main()
