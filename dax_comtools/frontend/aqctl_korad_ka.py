#!/usr/bin/env python3

from dax_comtools.devices.korad_ka.driver import KoradKADriver
import argparse
import logging

from sipyco.common_args import simple_network_args, verbosity_args, init_logger_from_args, bind_address_from_args
from sipyco.pc_rpc import simple_server_loop
import sys
sys.path.append('..')

logger = logging.getLogger(__name__)


def get_argparser() -> argparse.ArgumentParser:
    """Return argument parser with necessary arguments for driver."""
    parser = argparse.ArgumentParser(description="BKorad KA controller")
    simple_network_args(parser, 3275)
    verbosity_args(parser)  # This adds the -q and -v handling
    parser.add_argument("device", type=str,
                        help="Serial device to use, e.g. 'COMxx' on Windows or '/dev/ttyUSBx' on Linux")
    parser.add_argument("--write_timeout", type=float, default=1.0,
                        help="Timeout for write operations (default: %(default)s)")
    parser.add_argument("--valid_channels", type=int, nargs='+', default=[1],
                        help="List of valid channels (default: %(default)s)")
    parser.add_argument("--read_delay", type=float, default=0.1,
                        help="Delay between write and read operations (default: %(default)s)")
    parser.add_argument("-s", "--simulation", action="store_true",
                        help="Run in simulation mode")
    return parser


def main():
    """Start an RPC server for the controller."""
    args = get_argparser().parse_args()
    init_logger_from_args(args)  # This initializes logging system log level according to -v/-q args

    try:
        driver = KoradKADriver(device=args.device, write_timeout=args.write_timeout, valid_channels=args.valid_channels,
                               read_delay=args.read_delay, simulation=args.simulation)
    except ValueError as e:
        raise ValueError("Could not initialize driver, please provide valid command-line arguments") from e

    logger.info("Starting controller on port %i", args.port)
    try:
        simple_server_loop({"Korad KA": driver}, bind_address_from_args(args), args.port)
    except OSError as e:
        raise OSError(f"Network port {args.port} is probably already in use") from e
    except KeyboardInterrupt:
        pass
    finally:
        driver.close()
        logger.info("Shut down controller")


if __name__ == "__main__":
    main()
