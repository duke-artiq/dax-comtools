import logging
import socket
import time
import typing

__all__ = ['SMC100', 'SimSMC100']

_logger = logging.getLogger(__name__)


class SMC100:
    """Device driver for Rohde&Schwarz SMC100A signal generator."""

    __MIN_POWER: typing.ClassVar[float] = -120.0
    """Minimum power output in dbm."""
    __MAX_POWER: typing.ClassVar[float] = 17.0
    """Maximum power output in dbm."""
    __MIN_OFFSET: typing.ClassVar[float] = -100.0
    """Minimum power offset in dbm."""
    __MAX_OFFSET: typing.ClassVar[float] = 100.0
    """Maximum power offset in dbm."""
    __NDIGITS_POWER: typing.ClassVar[int] = 2
    """Number of digits for power."""
    __MIN_POWER_STEP: typing.ClassVar[float] = 10 ** -__NDIGITS_POWER
    """Minimum power step in dbm (i.e. power resolution)."""
    __MIN_FREQ: typing.ClassVar[float] = 9e3
    """Minimum frequency in Hz."""
    __MAX_FREQ: typing.ClassVar[float] = 3.2e9
    """Maximum frequency in Hz."""
    __NDIGITS_FREQ: typing.ClassVar[int] = 3
    """Number of digits for frequency."""
    __MIN_FREQ_STEP: typing.ClassVar[float] = 10 ** -__NDIGITS_FREQ
    """Minimum frequency step in Hz (i.e. frequency resolution)."""
    REF_OSC_SRC: typing.ClassVar[typing.FrozenSet[str]] = frozenset(["int", "ext"])
    """Reference oscillator source options."""

    _SOCKET_END: typing.ClassVar[str] = '\n'
    """Socket end character."""
    _SOCKET_CHUNK_SIZE: typing.ClassVar[int] = 1024
    """Socket chuck size."""
    _SOCKET_MAX_ATTEMPT: typing.ClassVar[int] = 10
    """Socket maximum number of attempts."""
    _DWELL: typing.ClassVar[int] = 10
    """Dwell in ms."""
    _MAX_ABS_SLOPE: typing.ClassVar[float] = 2.0
    """Maximum slope for power output in dbm per second."""
    _CONFIRM_RETRIES: typing.ClassVar[int] = 3
    """Number of retries to confirm a finished sweep."""

    DEFAULT_PORT: typing.ClassVar[int] = 5025
    """Default port for SMC100A."""
    DEFAULT_SLOPE: typing.ClassVar[float] = 1.0
    """Default slope in dbm per second."""
    DEFAULT_CUTOFF: typing.ClassVar[float] = -80.0
    """Default cutoff power level in dbm."""
    DEFAULT_OFFSET: typing.ClassVar[float] = 0.0
    """Default offset in dbm."""
    DEFAULT_LIMIT: typing.ClassVar[float] = __MAX_POWER
    """Default power limit in dbm."""
    DEFAULT_FREQ_STEP_LIMIT: typing.ClassVar[float] = __MAX_FREQ
    """Default frequency step limit in Hz."""
    DEFAULT_REF_OSC_SRC: typing.ClassVar[str] = "int"
    """Default reference oscillator source."""

    _addr: str
    _port: int
    _slope: float
    _step: float
    _cutoff: float
    _offset: float
    _limit: float
    _freq_step_limit: float
    _ref_osc_src: str
    _simulation: bool

    def __init__(self, addr: str, port: int = DEFAULT_PORT, *,
                 slope: float = DEFAULT_SLOPE, cutoff: float = DEFAULT_CUTOFF, offset: float = DEFAULT_OFFSET,
                 limit_level: typing.Optional[float] = None, limit_power: typing.Optional[float] = None,
                 freq_step_limit: float = DEFAULT_FREQ_STEP_LIMIT, ref_osc_src: str = DEFAULT_REF_OSC_SRC,
                 simulation: bool = False):
        """Initialize a new SMC100A driver.

        :param addr: IP address of the device
        :param port: Port to connect to
        :param slope: Ramp slope in dbm per second
        :param cutoff: Cutoff power level in dbm
        :param offset: Power offset in dbm
        :param limit_level: Power level limit in dbm (includes offset)
        :param limit_power: Power limit in dbm (without offset)
        :param freq_step_limit: Frequency step limit in Hz
        :param ref_osc_src: Reference oscillator source
        :param simulation: Simulation flag
        """

        assert isinstance(self._SOCKET_END, str)
        assert isinstance(self._SOCKET_CHUNK_SIZE, int) and self._SOCKET_CHUNK_SIZE > 0
        assert isinstance(self._SOCKET_MAX_ATTEMPT, int) and self._SOCKET_MAX_ATTEMPT > 0
        assert isinstance(self._DWELL, int) and self._DWELL > 0
        assert isinstance(self._MAX_ABS_SLOPE, float) and self._MAX_ABS_SLOPE > 0.0
        assert isinstance(self._CONFIRM_RETRIES, int) and self._CONFIRM_RETRIES > 0

        assert isinstance(self.DEFAULT_PORT, int)
        assert isinstance(self.DEFAULT_SLOPE, float)
        assert isinstance(self.DEFAULT_CUTOFF, float)
        assert isinstance(self.DEFAULT_LIMIT, float)
        assert isinstance(self.DEFAULT_OFFSET, float)
        assert isinstance(self.DEFAULT_FREQ_STEP_LIMIT, float)

        assert isinstance(addr, str)
        assert isinstance(port, int)
        assert isinstance(slope, float)
        assert isinstance(cutoff, float)
        assert isinstance(offset, float)
        assert isinstance(limit_level, float) or limit_level is None
        assert isinstance(limit_power, float) or limit_power is None
        assert isinstance(freq_step_limit, float)
        assert isinstance(simulation, bool)

        # Decide which limit parameter to use
        if sum(p is not None for p in [limit_level, limit_power]) > 1:
            raise TypeError("Limit level and limit power parameters can not be used together")
        elif limit_level is not None:
            limit: float = limit_level
        elif limit_power is not None:
            limit = limit_power + offset  # Convert power to level
        else:
            limit = self.DEFAULT_LIMIT + offset  # Convert power to level

        # Store attributes
        self._addr = addr
        self._port = port
        self._slope = abs(slope)
        self._step = round(self._slope / (1000 / self._DWELL), self.__NDIGITS_POWER)
        self._cutoff = round(cutoff, self.__NDIGITS_POWER)
        self._offset = round(offset, self.__NDIGITS_POWER)
        self._limit = round(limit, self.__NDIGITS_POWER)
        self._freq_step_limit = round(abs(freq_step_limit), self.__NDIGITS_FREQ)
        self._ref_osc_src = ref_osc_src
        self._simulation = simulation

        # Report configuration
        _logger.debug(f"slope: {self._slope} dbm/s")
        _logger.debug(f"step: {self._step} dbm")
        _logger.debug(f"cutoff: {self._cutoff} dbm")
        _logger.debug(f"offset: {self._offset} dbm")
        _logger.debug(f"limit: {self._limit} dbm")
        _logger.debug(f"frequency step limit: {self._freq_step_limit} Hz")
        _logger.debug(f"reference oscillator source: {self._ref_osc_src}")

        # Check values
        if self._slope > self._MAX_ABS_SLOPE:
            raise ValueError(f"Ramp slope higher than the hard-coded limit of {self._MAX_ABS_SLOPE} dbm/s")
        if self._slope == 0.0:
            raise ValueError("Ramp slope can not be zero")
        if self._step < self.__MIN_POWER_STEP:
            raise ValueError("Ramp slope is lower than the power resolution")
        if not self.is_valid_power(self._cutoff):
            raise ValueError("Cutoff power level out of range")
        if not self.is_valid_power(self._limit):
            raise ValueError("Power limit out of range")
        if self._limit < self._cutoff:
            raise ValueError("Power limit can not be lower than the cutoff power level")
        if self._freq_step_limit < self.__MIN_FREQ_STEP:
            raise ValueError("Frequency step limit is lower than the frequency resolution")

        # Open connection
        self.open()
        # Set offset
        self._set_offset(self._offset)  # Also validates offset value
        # Set reference oscillator source
        self._set_ref_osc(self._ref_osc_src)  # Also validates source value

    # noinspection PyMethodMayBeStatic
    def ping(self) -> bool:
        """Always returns true (indication that the driver is running)."""
        return True

    """Device communication methods"""

    def open(self) -> None:
        """Open socket connection to device."""
        if not self._simulation:
            _logger.debug(f"Opening network connection to [{self._addr}]:{self._port}")
            self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self._socket.connect((self._addr, self._port))
        else:
            _logger.debug("Opening network connection : simulation mode")

    def close(self) -> None:
        """Close socket connection to device."""
        if not self._simulation:
            _logger.debug(f"Closing network connection to [{self._addr}]:{self._port}")
            self._socket.close()
        else:
            _logger.debug("Closing network connection : simulation mode")

    def _write(self, message: str) -> None:
        """Write message to the device."""
        assert isinstance(message, str)

        if self._simulation:
            _logger.debug(f'Writing message "{message}" : simulation mode')
        else:
            self._socket.send(f"{message}{self._SOCKET_END}".encode())

    def _read(self) -> str:
        """Read message from the device."""

        if self._simulation:
            raise RuntimeError("Read message not available in simulation mode")

        result = ''
        for _ in range(self._SOCKET_MAX_ATTEMPT):
            incoming_bytes = self._socket.recv(self._SOCKET_CHUNK_SIZE)
            if incoming_bytes[-1] == ord(self._SOCKET_END):
                incoming_string = incoming_bytes[0:-1].decode('ascii')
                result += incoming_string
                return result
            else:
                incoming_string = incoming_bytes.decode('ascii')
                result += incoming_string

        # Maximum number of attempts was reached
        raise RuntimeError(f"Did not reach end of message. Received message:\n{result}")

    def _query(self, message: str) -> str:
        """Write message to instrument and read the response."""
        assert isinstance(message, str)

        self._write(message)
        return self._read()

    """Helper functions"""

    def is_valid_power(self, power: float) -> bool:
        """Check if a given power level is in the valid range based on the current offset.

        :param power: The power level to validate in dbm
        :return: :const:`True` if the power level is valid
        """
        assert isinstance(power, float)
        return self.__MIN_POWER + self._offset <= power <= self.__MAX_POWER + self._offset

    def is_valid_offset(self, offset: float) -> bool:
        """Check if a given power offset is in the valid.

        :param offset: The power offset to validate in dbm
        :return: :const:`True` if the power offset is valid
        """
        assert isinstance(offset, float)
        return self.__MIN_OFFSET <= offset <= self.__MAX_OFFSET

    def is_valid_freq(self, freq: float) -> bool:
        """Check if a given frequency is in the valid.

        :param freq: The frequency to validate in Hz
        :return: :const:`True` if the frequency is valid
        """
        assert isinstance(freq, float)
        return self.__MIN_FREQ <= freq <= self.__MAX_FREQ

    """Device control methods"""

    def get_ref_osc(self) -> str:
        """Query the current reference oscillator source."""
        s: str = self._query("rosc:source?").strip().lower()

        # Validate value
        if s not in self.REF_OSC_SRC:
            raise ValueError(f"Device returned unexpected reference oscillator source: {s}")

        # Return value
        return s

    def _set_ref_osc(self, source: str) -> None:
        """Set the reference oscillator source.

        :param source: Either ``'int'`` or ``'ext'``
        """
        # Validate value
        if source not in self.REF_OSC_SRC:
            raise ValueError(f"Expected reference oscillator source '{source}' not in {self.REF_OSC_SRC}")

        # Message
        _logger.debug(f"Set reference oscillator source: {source}")

        # Only set source if the value changed
        if source != self.get_ref_osc():
            if self.is_rf_on():
                raise RuntimeError("Cannot change reference oscillator source while rf is on")

            # Write source
            self._write(f"rosc:source {source:s}")
            _logger.info(f"Reference oscillator source set to {source}")

        else:
            _logger.debug("Reference oscillator source not changed")

    def get_power(self) -> float:
        """Query the current set power of the device."""
        p: float = float(self._query("power?"))

        # Validate value
        if not self.is_valid_power(p):
            raise ValueError(f"Device returned unexpected power level: {p}")

        # Return value
        return p

    def set_power(self, level_dbm: float) -> None:
        """Set the output power level.

        Triggers a linear ramp in power from the current level to the new set level.

        :param level_dbm: target power in dbm
        """
        assert isinstance(level_dbm, float)

        # Validate input values
        if not self.is_valid_power(level_dbm):
            raise ValueError(f"Power level of {level_dbm} dbm is out of range")
        if level_dbm < self._cutoff:
            raise ValueError(f"Power level of {level_dbm} dbm is lower than the cutoff power level")
        if level_dbm > self._limit:
            raise ValueError(f"Power level of {level_dbm} dbm is higher than the power limit")

        # Round target level
        level_dbm = round(level_dbm, self.__NDIGITS_POWER)
        _logger.debug(f"Set power to level: {level_dbm} dbm")

        # Get the current level
        current_level: float = self.get_power()
        _logger.debug(f"Current power level: {current_level} dbm")
        if current_level > self._limit:
            raise RuntimeError("Current power level is higher than the power limit (undefined behavior)")

        # Decide the start level
        start_level: float = round(max(current_level, self._cutoff), self.__NDIGITS_POWER)
        _logger.debug(f"Start power level: {start_level} dbm")

        # Verify that the distance is at least enough for an actual ramp
        # NOTE: The state of the device becomes inconsistent when ramping with start=stop
        if abs(start_level - level_dbm) < self._step:
            _logger.debug("Not ramping, power difference is smaller than the step size")
            return

        # Ramp
        messages: typing.List[str] = [
            f"sweep:power:dwell {self._DWELL:d} ms",
            "sweep:power:mode auto",
            f"sweep:power:step {self._step:.{self.__NDIGITS_POWER}f}dB",
            "sweep:power:shape sawtooth",
            f"power:start {start_level:.{self.__NDIGITS_POWER}f}",
            f"power:stop {level_dbm:.{self.__NDIGITS_POWER}f}",
            f"power:limit {self._limit - self._offset:.{self.__NDIGITS_POWER}f}",  # Convert level to power
            "power:mode sweep",
            "*WAI",
            "sweep:power:execute",
            "*WAI",
        ]
        for msg in messages:
            self._write(msg)

        # Calculate the ramp time
        ramp_time: float = abs(level_dbm - start_level) / self._slope
        # Wait for the ramp to finish
        _logger.info(f"Ramping rf to {level_dbm} dbm for {ramp_time:.1f} second(s)...")
        self._wait_for_ramp(ramp_time)

    def _wait_for_ramp(self, ramp_time: float) -> None:
        assert isinstance(ramp_time, float)

        # Wait for the ramp to complete
        time.sleep(ramp_time + 0.1)

        # Confirm the ramp execution finished
        for _ in range(self._CONFIRM_RETRIES):
            if not int(self._query("sweep:power:running?")):
                # Ramping is done
                _logger.debug("Ramp completed!")
                break
            else:
                # Sleep before trying again
                _logger.debug("Ramp running longer than expected, waiting a bit more")
                time.sleep(max(ramp_time * 0.1, 0.5))
        else:
            raise RuntimeError("Ramp running longer than expected")

    def rf_off(self) -> None:
        """Turn off the rf output."""
        if self.is_rf_on():
            # Reduce power to cutoff value
            self.set_power(self._cutoff)

        # Switch off rf
        _logger.debug("Switching rf off")
        self._write("output 0")
        self._write("*WAI")

    def rf_on(self) -> None:
        """Turn on the rf output."""
        if not self.is_rf_on():
            # Guarantee power level is save without ramping for faster operation
            self._write(f"power {self._cutoff:.{self.__NDIGITS_POWER}f}")

        # Switch on rf
        _logger.debug("Switching rf on")
        self._write("output 1")
        self._write("*WAI")

    def is_rf_on(self) -> bool:
        """Query if the rf output is on or off."""
        s = int(self._query("output?"))

        # Validate value
        if s not in {0, 1}:
            raise ValueError(f"Device returned unexpected output state: {s}")

        # Return value as a bool
        return bool(s)

    def get_freq(self) -> float:
        """Get current output frequency of device."""
        f: float = float(self._query("freq?"))

        # Validate value
        if not self.is_valid_freq(f):
            raise ValueError(f"Device returned unexpected frequency: {f}")

        # Return value
        return f

    def set_freq(self, freq: float) -> None:
        """Set current output frequency of device.

        :param freq: frequency in Hz
        """
        assert isinstance(freq, float)

        if not self.is_valid_freq(freq):
            raise ValueError(f"Frequency {freq} out of the valid range")

        # Round frequency
        freq = round(freq, self.__NDIGITS_FREQ)
        _logger.debug(f"Set frequency: {freq} Hz")

        # Get the current frequency
        current_freq: float = self.get_freq()
        _logger.debug(f"Current frequency: {current_freq} Hz")

        # Check if the frequency difference does not exceed the step limit
        if self.is_rf_on() and abs(current_freq - freq) > self._freq_step_limit:
            raise RuntimeError(f"Frequency step to {freq} Hz is larger than the frequency step limit")

        # Set frequency
        self._write(f"freq {freq:.{self.__NDIGITS_FREQ}f}")

    def get_offset(self) -> float:
        """Get the power offset.

        :return: offset in dbm
        """
        o: float = float(self._query("power:offset?"))

        # Validate value
        if not self.is_valid_offset(o):
            raise ValueError(f"Device returned unexpected offset: {o}")

        # Return value
        return o

    def _set_offset(self, offset: float) -> None:
        """Set the power offset.

        :param offset: offset in dbm
        """
        assert isinstance(offset, float)

        # Validate input values
        if not self.is_valid_offset(offset):
            raise ValueError(f"Power offset of {offset} dbm is out of range")

        # Round offset
        offset = round(offset, self.__NDIGITS_POWER)
        _logger.debug(f"Set power offset: {offset} dbm")

        # Get the current offset
        current_offset: float = self.get_offset()
        _logger.debug(f"Current power offset: {current_offset} dbm")

        # Only set offset if the value changed
        if current_offset != offset:
            if self.is_rf_on():
                raise RuntimeError("Offset can not be set while rf is on")

            # Set power offset
            self._write(f"power:offset {offset:.{self.__NDIGITS_POWER}f} dB")
            _logger.info(f"Power offset set to {offset} dbm")

        else:
            _logger.debug("Power offset was not changed")


class SimSMC100(SMC100):
    """Simulated device driver for :class:`SMC100`."""

    _output: bool
    _freq: float
    _power: float

    def __init__(self, *args: typing.Any, **kwargs: typing.Any):
        # Initial state
        self._output = False
        self._freq = 9e3

        # Guarantee simulation is enabled
        kwargs['simulation'] = True
        # Call super
        super(SimSMC100, self).__init__(*args, **kwargs)

        # Derived state
        self._power = self._cutoff

    def get_ref_osc(self) -> str:
        return self._ref_osc_src

    def _set_ref_osc(self, source: str) -> None:
        super(SimSMC100, self)._set_ref_osc(source)
        self._ref_osc_src = source

    def get_power(self) -> float:
        return self._power

    def set_power(self, level_dbm: float) -> None:
        super(SimSMC100, self).set_power(level_dbm)
        self._power = level_dbm

    def _wait_for_ramp(self, ramp_time: float) -> None:
        pass

    def rf_off(self) -> None:
        super(SimSMC100, self).rf_off()
        self._output = False

    def rf_on(self) -> None:
        super(SimSMC100, self).rf_on()
        self._output = True

    def is_rf_on(self) -> bool:
        return self._output

    def get_freq(self) -> float:
        return self._freq

    def set_freq(self, freq: float) -> None:
        super(SimSMC100, self).set_freq(freq)
        self._freq = freq

    def get_offset(self) -> float:
        return self._offset

    def _set_offset(self, offset: float) -> None:
        super(SimSMC100, self)._set_offset(offset)
        self._offset = offset
