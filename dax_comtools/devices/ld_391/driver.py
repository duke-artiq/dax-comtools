# mypy: no_warn_unused_ignores

import logging
import pyvisa
from pyvisa.constants import Parity, StopBits

__all__ = ['LD391Driver']

_logger = logging.getLogger(__name__)


class LD391Driver:
    """Driver for LD391 controller to turn on the ionization laser."""

    BAUD_RATE: int = 38400
    """Fixed baud rate for this device (USB serial connection)."""

    def __init__(self, device: str, *, simulation: bool = False):
        """
        :param device: serial port device is connected to
        :param simulation: flag to run in simulation mode (dump all received messages to stdout)
        """
        assert isinstance(device, str)
        assert isinstance(simulation, bool)

        self._simulation: bool = simulation

        # create simulation variables
        self._temperature = 0
        self._current = 0
        self._ld_status = 0
        self._tec_status = 0

        if self._simulation:
            return

        # Set up connection
        self._conn = pyvisa.ResourceManager().open_resource(
            device, parity=Parity.none, stop_bits=StopBits.one, data_bits=8, baud_rate=self.BAUD_RATE)

    # noinspection PyMethodMayBeStatic
    def ping(self):
        """Always returns true (indication that the driver is running)."""
        return True

    def _write(self, message: str):
        """Wrapper code for write functionality to take into account simulations."""
        if self._simulation:
            _logger.info(message)
        else:
            self._conn.write(message)  # type: ignore[attr-defined]

    def _query(self, message: str, simulation_value):
        """Wrapper code for query functionality to take into account simulations."""
        if self._simulation:
            _logger.info(message)
            return simulation_value
        else:
            return self._conn.query(message)  # type: ignore[attr-defined]

    def flush(self):
        """Flush pyvisa buffer."""
        if self._simulation:
            return

        self._conn.flush(pyvisa.constants.VI_READ_BUF_DISCARD)

        while True:
            # noinspection PyBroadException
            try:
                print(self._conn.read())
            except:  # noqa: E722
                break

    def ld_check_status(self):
        """Return status of laser diode."""
        return int(self._query('LAS:OUTPUT?', self._ld_status))

    def ld_on(self):
        """Turn on laser diode."""
        self._ld_status = 1
        self._write('LAS:OUTPUT 1')

    def ld_off(self):
        """Turn off laser diode."""
        self._ld_status = 0
        self._write('LAS:OUTPUT 0')

    def ld_set_current(self, set_value):
        """Set laser diode current.

        :param set_value: new current value
        """
        self._current = set_value
        self._write('LAS:SET:I ' + str(set_value))

    def ld_get_current(self):
        """Get laser diode current."""
        return float(self._query('LAS:I?', self._current))

    def tec_check_status(self):
        """Return status of TEC."""
        return int(self._query('TEC:OUTPUT?', self._tec_status))

    def tec_on(self):
        """Turn on temperature control."""
        self._tec_status = 1
        self._write('TEC:OUTPUT 1')

    def tec_off(self):
        """Turn off temperature control."""
        self._tec_status = 0
        self._write('TEC:OUTPUT 0')

    def tec_set_temperature(self, set_value):
        """Set TEC temperature.

        :param set_value: new temperature value
        """
        self._temperature = set_value
        self._write('TEC:T ' + str(set_value))

    def tec_get_temperature(self):
        """Get TEC temperature."""
        return float(self._query('TEC:T?', self._temperature))

    def close(self):
        """Close serial connection."""
        if not self._simulation:
            self._conn.close()
