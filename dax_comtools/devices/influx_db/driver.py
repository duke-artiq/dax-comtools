from influxdb import InfluxDBClient
from influxdb.exceptions import InfluxDBClientError, InfluxDBServerError
from concurrent.futures import ThreadPoolExecutor
import logging

logger = logging.getLogger(__name__)


def _write(client, data):
    try:
        client.write_points(data)
    except (InfluxDBClientError, InfluxDBServerError):
        logger.exception("Influx error")
    except Exception as e:
        # any uncaught/unlogged exception in the worker thread is silent, hence the catch-all
        # also for some reason I was unable to catch connection errors via specific exceptions,
        # so those are caught here too
        logger.exception(str(e))


class InfluxDBDriver:
    """Simple InfluxDB API wrapper to allow non-blocking writes."""

    def __init__(self, db: str, server_db: str, port_db: int, user_db: str, password_db: str, simulation: bool):
        """
        :param db: name of database to connect to
        :param server_db: IP address of InfluxDB server
        :param port_db: InfluxDB server API port
        :param user_db: InfluxDB user name
        :param password_db: InfluxDB password
        :param simulation: flag to run in simulation mode (dump all received messages to stdout)
        """

        self.simulation = simulation
        if simulation:
            return

        self._client = InfluxDBClient(host=server_db, port=port_db, database=db,
                                      username=user_db, password=password_db, timeout=15)
        # check connection, allow exception to be raised if it fails
        self._client.ping()

        # basically just using a ThreadPoolExecutor as a way to do non-blocking operations
        self._executor = ThreadPoolExecutor(max_workers=1)

    # noinspection PyMethodMayBeStatic
    def ping(self):
        """Always returns true (indication that the driver is running)."""
        return True

    def write_points(self, data):
        """
        Write points to InfluxDB.

        Data format should be the same as for the regular InfluxDB Python API, e.g.
        ::
            [
                {
                    "measurement": "measurement1",
                    "tags": {
                        "tag1.1": "value",
                        "tag1.2": "value"
                    },
                    "fields": {
                        "field1.1": "value",
                        "field1.2": "value"
                    }
                }, {
                    "measurement": "measurement2",
                    "tags": {
                        "tag2.1": "value",
                        "tag2.2": "value"
                    },
                    "fields": {
                        "field2.1": "value",
                        "field2.2": "value"
                    }
                }
            ]
        :param data: data to write
        """
        if self.simulation:
            logger.info(data)
        else:
            self._executor.submit(_write, self._client, data)

    def close(self):
        """Close connection to InfluxDB."""
        if not self.simulation:
            self._client.close()
