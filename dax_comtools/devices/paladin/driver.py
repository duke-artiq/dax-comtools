import serial
import logging
import typing

from dax_comtools.devices.paladin.constants import EOS, TERMINATION, COMMAND, STATUS_DICT, ERROR_DICT

__all__ = ['PaladinDriver']

_logger = logging.getLogger(__name__)


class PaladinDriver:
    """ Driver code for Paladin laser """

    _MAX_TRIAL: int = 50

    def __init__(self, port_name: str):
        """
        :param port_name: serial port name, e.g. 'COM##'
        """
        assert isinstance(port_name, str)

        # For some reason, pyvisa doesn't work as well as pyserial for Paladin serial communication
        # establish serial connection
        try:
            self.ser = serial.Serial(port_name)
        except serial.SerialException:
            raise serial.SerialException(f"Failed to connect {port_name}")

        _logger.debug('Paladin laser connected via %s', port_name)
        self._port_name = port_name

        self.ser.baud_rate = 9600
        self.ser.timeout = 0.1

        self._value = 0.0
        self._error_code = 0

        # laser status
        self.laser_status = self.query_parameter('laser')[0]
        self.shutter_status = self.query_parameter('shutter')[0]
        self.alignment_mode = self.query_parameter('alignment_mode')[0]

        # flush serial communication
        self.flush()

        # turn on service mode
        self.write_parameter('service_mode', True)

    # basic device communications
    def open(self) -> None:
        """ open serial communication """
        self.ser.open()
        _logger.debug('Paladin laser communication port open. Port name : %s', self._port_name)

    def close(self) -> None:
        """ close serial communication """
        self.ser.close()
        _logger.debug('Paladin laser communication port closed. Port name : %s', self._port_name)

    def write(self, message: str) -> None:
        """
        write message to the device via serial communication
        :param message: message to be written
        """
        assert isinstance(message, str)
        message = message + TERMINATION
        self.ser.write(message.encode('ascii'))

    def flush(self) -> None:
        """ flush input buffer """
        self.ser.flush()
        if self.ser.in_waiting:
            while True:
                if self.ser.read().decode('ascii') == '':
                    break

    def query(self, message: str) -> typing.Tuple[typing.Union[int, float], int]:
        """
        customized query function for Paladin laser
        :param message: message to be written
        :returns: tuple of query value and error code
        """
        assert isinstance(message, str)

        # flush before communication
        if self.ser.in_waiting:
            self.flush()

        # write to the device
        self.write(message)

        # readout the return value
        read_val = ''
        trial = 0
        while True:
            trial += 1
            temp = self.ser.read().decode('ascii')
            if temp == EOS:
                break
            else:
                read_val += temp
            if trial > self._MAX_TRIAL:
                _logger.warning('No signal from device')
                return -1, -1

        [value, error] = read_val.split(":", 1)
        self._value = float(value)
        self._error_code = int(error)

        if self._error_code:
            _logger.debug('command: %s', message)
            _logger.debug('response: %s', read_val)
            if self._error_code in ERROR_DICT.keys():
                _logger.warning('Error: %s', ERROR_DICT[self._error_code])
            else:
                _logger.warning('Incorrect command: %d', self._error_code)

        return self._value, self._error_code

    def query_parameter(self, parameter: str) -> typing.Tuple[typing.Any, int]:
        """
        read the status/value from the device
        :param parameter: parameter name to be read
        :return: tuple of value and error code
        """
        assert isinstance(parameter, str)

        # reading float values from the laser (e.g. power, current, temperatures )
        if parameter in COMMAND['float_query']:
            if parameter == 'power' and self.alignment_mode:  # reading not reliable
                value = 0.0
                error = 0
            else:
                command = COMMAND['float_query'][parameter]
                (value, error) = self.query(command)
            return float(value), error
        # reading boolean values from the laser (e.g. on/off status, shutter status )
        elif parameter in COMMAND['boolean_query']:
            command = COMMAND['boolean_query'][parameter]
            (value, error) = self.query(command)
            return bool(value), error
        # reading laser status
        elif parameter == 'status':
            command = COMMAND['status_query'][parameter]
            (value, error) = self.query(command)
            return STATUS_DICT[int(value)], error
        # given query parameter is not found
        else:
            _logger.warning('%s is an unknown parameter', parameter)
            return -1, -1

    def write_parameter(self, parameter: str, value: typing.Union[bool, float]) -> int:
        """
        write the status/value to the device
        :param parameter: parameter name to be write
        :param value: value to write
        :return: tuple of value and error code
        """
        assert isinstance(parameter, str)
        assert isinstance(value, (bool, float))

        # write settings with float value
        if parameter in COMMAND['float_write'].keys():
            command = COMMAND['float_write'][parameter].format(value)
            value, error = self.query(command)
            return error
        # write settings with boolean value
        elif parameter in COMMAND['boolean_write'].keys():
            command = COMMAND['boolean_write'][parameter].format(int(value))
            value, error = self.query(command)
            # update driver attributes
            if parameter == 'laser':
                self.laser_status = value
            elif parameter == 'shutter':
                self.shutter_status = value
            elif parameter == 'alignment_mode':
                self.alignment_mode = value
            return error
        # given write parameter is not found
        else:
            _logger.warning('%s is an unknown parameter', parameter)
            return -1
