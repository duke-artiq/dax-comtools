""" constants and command list for Paladin laser """
from typing import Dict

__all__ = ['TERMINATION', 'EOS',
           'STATUS_DICT', 'ERROR_DICT', 'COMMAND']

TERMINATION: str = '\r'  # or ';'
EOS: str = ';'  # End of string

# translation table for laser status query
STATUS_DICT: Dict[int, str] = {
    0: 'Ramp up oscillator',
    1: 'Equilibration oscillator',
    2: 'Checking IR Oscillator',
    7: 'Ramp up Lightloop',
    8: 'Normal operation',
    10: 'Ramp down oscillator',
    11: 'Standby',
    12: 'Warm up',
    13: 'Alignment mode wait',
    14: 'Alignment mode ready'}

# error code dictionary
ERROR_DICT: Dict[int, str] = {
    0: 'Normal operation',
    73: 'Bad command',
    77: 'Bad Data Format',
    78: 'Range error',
    120: 'Allowed only in stand-by',
    123: 'Power is out of range',
    130: 'Allowed only in normal mode',
    131: 'Power not reliable in alignment mode',
    132: 'Alignment mode not available',
    134: 'Output power ok. Power headroom too low'}

# command list for the paladin laser serial communication
COMMAND: Dict[str, Dict[str, str]] = {
    'boolean_query': {
        'laser': '?L',
        'shutter': '?S',
        'alignment_mode': '?AM',
        'current_mode': '?CM',
        'service_mode': '?SM'
    },
    'float_query': {
        'current': '?CIP',
        'set_current': '?SCIP',
        'power': '?P',
        'power_rollover': '?PRO',
        'heatsink_laserhead': '?HT0',
        'heatsink_ld_ps': '?HT1',
        'ld_set_temp': '?DST1',
        'ld_act_temp': '?DT1',
        'vanadate_temp': '?T0',
        'resonator_temp': '?T1',
        'shg_temp': '?T5',
        'thg_temp': '?T6',
        'sbr_temp': '?T7'
    },
    'status_query': {
        'status': '?STA'
    },
    'boolean_write': {
        'laser': 'L={}',
        'shutter': 'S={}',
        'alignment_mode': 'AM={}',
        'current_mode': 'CM={}',
        'service_mode': 'SM={}'
    },
    'float_write': {
        'set_current': 'CIP={.2f}'
    }
}
