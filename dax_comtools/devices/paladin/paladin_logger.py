# mypy: no_warn_unused_ignores

import queue
import json
import threading
import logging
import typing

from dax_comtools.devices.paladin.driver import PaladinDriver
from dax_comtools.devices.paladin.constants import COMMAND, ERROR_DICT
from dax_comtools.devices.smart_strip import SmartPowerStrip

from dax_comtools.util.influx_db import InfluxDBUtil
from dax_comtools.util.recurring_task import RecurringTaskBase

_logger = logging.getLogger(__name__)

__all__ = ['PaladinLogger', 'PaladinSecuredLogger']


class PaladinLogger(RecurringTaskBase):
    """ Paladin logger, periodically record the laser parameters into Influx DB """

    ERROR_CODE_KEY = 'error_code'
    ERROR_MESSAGE_KEY = 'error_message'

    def __init__(self, config_file: str):
        """
        :param config_file: config json container. see `read_config` method for detail
        """
        # read config file
        self._config: typing.Dict[str, typing.Any] = {}
        self._config_file = config_file
        self.read_config(config_file)
        self._influx_tags: typing.Dict[str, typing.Any] = self._config['influx_tag']

        # setup devices
        self._paladin = PaladinDriver(self._config['port_name'])
        self._influx = InfluxDBUtil(**self._config['influx_db'])

        if 'warning_lamp' in self._config:
            self._lamp = SmartPowerStrip(self._config['warning_lamp']['ip_address'])
            self._lamp_channel = self._config['warning_lamp']['channel']
        if 'n2_valve' in self._config:
            self._valve = SmartPowerStrip(self._config['n2_valve']['ip_address'])
            self._valve_channel = self._config['n2_valve']['channel']

        # instantiate queue object and parameter dictionary, write has higher priority over read
        self._task_queue: queue.PriorityQueue[typing.Tuple[int, typing.Union[str, typing.Tuple[str, typing.Any]]]]
        self._task_queue = queue.PriorityQueue()
        self._laser_parameters: typing.Dict[str, typing.Union[float, bool, str]] = {}

        # set up communication thread
        self._communication_thread = threading.Thread(target=self._device_communications)
        self._communication_thread.start()

        # call super, start logging
        super(PaladinLogger, self).__init__(self._config['log_interval'], start=True)

    # noinspection PyMethodMayBeStatic
    def ping(self) -> bool:
        """ for controller communication check """
        return True

    def read_config(self, config_file: typing.Optional[str] = None) -> None:
        """
        read configurations from config json file
        :param config_file: config json file.
        {
          "port_name": "COMXX",   # xx goes port number
          "log_interval": xx.xx,  # log interval in sec
          "warning_lamp":{        # use this if warning lamp w/ smart strip is available.
            "ip_address": "xxx.xxx.xxx.xxx"  # ip address of the smart strip
            "channel" : x                    # channel number of the warning lamp
          },
          "n2_valve":{            # use this if pneumatic nitrogen valve w/ smart strip is available.
            "ip_address": "xxx.xxx.xxx.xxx"  # ip address of the smart strip
            "channel" : x                    # channel number of the n2 valve
          },
          "influx_db": {          # influx db server/account info
          },
          "influx_tag": {         # influx tag info
          },
          "password" : xxxx       # only for secured controller
        }
        """
        if config_file is None:
            config_file = self._config_file
        with open(config_file, 'r') as f:
            self._config = json.load(f)
            _logger.debug('config file %s loaded', config_file)

    def open(self) -> None:
        """ open serial communication port """
        self._paladin.open()
        self.start()

    def close(self, *, shutdown: bool = False) -> None:
        """ close serial communication port """
        self.stop()
        self._task_queue.join()
        self._paladin.close()
        if shutdown:
            # Close all other connections too
            self._influx.close()

    def task(self) -> None:
        """ recurring task """
        self.request_log()

    def _write(self, **kwargs: typing.Union[float, bool]) -> None:
        """
        write value to the device.
        writing (priority level 0) has higher priority over reading (priority level 1)
        NOTE : The action may not be done immediately. It will put the commands on action queue.
        :param kwargs: use keyword argument to write value to the device.
        e.g. current = 110.0 , laser = True
        See constants.COMMAND for the list of parameters can be written
        """
        for key, value in kwargs.items():
            _logger.debug('writing : %s, %s', key, str(value))
            self._task_queue.put((0, (key, value)))

    def _read(self, *args: str) -> None:
        """
        read value from the device and store it as internal attribute
        writing (priority level 0) has higher priority over reading (priority level 1)
        NOTE : The action may not be done immediately. It will put the commands on action queue.
        :param args: names of parameter be read
        e.g. laser, power, ...
        See constants.COMMAND for the list of parameters can be read
        """
        for arg in args:
            _logger.debug('reading %s', arg)
            self._task_queue.put((1, arg))

    def _read_all(self):
        """
        thoroughly scan the laser parameters and store them as internal attributes
        """
        self._read(*COMMAND['boolean_query'].keys())
        self._read(*COMMAND['float_query'].keys())
        self._read('status')

    def get_value(self, parameter_names: typing.List[str]) -> typing.Dict[str, typing.Union[float, bool, str]]:
        """
        return laser parameters for given parameter names.
        NOTE : The return value might not be up to date.
        :param parameter_names: list of parameter names to be acquired.
        :return: dictionary of parameter name and corresponding value
        """
        values = {}
        for parameter_name in parameter_names:
            try:
                values[parameter_name] = self._laser_parameters[parameter_name]
            except KeyError:
                _logger.warning('%s is unknown parameter name', parameter_name)

        return values

    def get_value_all(self) -> typing.Dict[str, typing.Union[float, bool, str]]:
        """
        return complete dictionary of laser parameters
        NOTE : The return value might not be up to date.
        :return: dictionary of parameter name and corresponding value
        """
        return self._laser_parameters

    def toggle_laser(self, onoff: bool) -> None:
        """
        toggle laser on or off
        it will also toggle the warning lamp if configured
        :param onoff: True if on, else False
        """
        self._write(laser=onoff)
        # warning lamp
        if hasattr(self, '_lamp'):
            text = 'on' if onoff else 'off'
            self._lamp.toggle_plug(text, plug_num=self._lamp_channel)

    def toggle_alignment_mode(self, onoff: bool) -> None:
        """
        toggle laser alignment mode. Only works when laser is is Normal operational mode.
        :param onoff: True if on, else False
        """
        self._write(alignment_mode=onoff)

    def toggle_shutter(self, onoff: bool) -> None:
        """
        toggle laser shutter. Only works when laser is is Normal operational mode or Alignment mode ready
        :param onoff: True if on, else False
        """
        self._write(shutter=onoff)
        # nitrogen purging valve
        if hasattr(self, '_valve'):
            text = 'on' if onoff else 'off'
            self._valve.toggle_plug(text, plug_num=self._valve_channel)

    def toggle_current_mode(self, onoff: bool) -> None:
        """
        toggle laser current mode.
        In current mode, user can change the laser current (0~110).
        In light loop mode, the laser will adjust the laser current to stabilize the output power to 3.75W
        :param onoff: True for current mode, False for light loop
        """
        self._write(current_mode=onoff)

    def set_laser_current(self, current: float) -> None:
        """
        set the laser current.
        Only functional when the laser is in current mode. See `toggle_current_mode` method
        :param current: laser current. 0~110
        """
        try:
            if self._laser_parameters['current_mode']:
                self._write(set_current=current)
            else:
                _logger.warning('light loop mode : current not configurable')
        except KeyError:
            _logger.warning('Check laser mode first!')

    def _write_log(self) -> None:
        """ write current laser parameter into influx DB """
        self._influx.write_point_monitoring(**self._influx_tags, **self._laser_parameters)  # type: ignore[arg-type]

        # remove error key once it is recorded.
        self._laser_parameters.pop(self.ERROR_CODE_KEY, None)
        self._laser_parameters.pop(self.ERROR_MESSAGE_KEY, None)

    def request_log(self) -> None:
        """ read all the laser parameter and record the influx log """
        self._read_all()
        self._task_queue.put((1, 'record_log'))  # priority level 1 - the same to reading

    def _device_communications(self) -> None:
        """
        Method for background communication thread.
        This method keeps monitoring queue items and read from/write to the device based on the queue item.
        Paladin serial communication is relatively slow, so the background thread is made not to block the
        code during serial communication time.
        """
        while True:
            task = self._task_queue.get()
            assert isinstance(task, tuple)

            # Unpack task
            _, data = task
            error_code = 0

            if isinstance(data, tuple):  # write
                # Unpack data
                assert len(data) == 2
                parameter, value = data
                error_code = self._paladin.write_parameter(parameter, value)

            elif isinstance(data, str):  # query
                parameter = data
                if parameter == 'record_log':
                    self._write_log()
                else:
                    value, error_code = self._paladin.query_parameter(parameter)
                    self._laser_parameters[parameter] = value

            else:
                _logger.error('Invalid task')

            if error_code:
                self._laser_parameters[self.ERROR_CODE_KEY] = error_code
                if error_code in ERROR_DICT:
                    self._laser_parameters[self.ERROR_MESSAGE_KEY] = ERROR_DICT[error_code]

            self._task_queue.task_done()


class PaladinSecuredLogger(PaladinLogger):
    """
    Paladin logger with password.
    writing and controlling the device is only allowed for qualified client with proper password.
    """

    def __init__(self, config_file: str):
        """
        :param config_file: config json container. see `read_config` method for detail
        """
        super(PaladinSecuredLogger, self).__init__(config_file)
        self._password = self._config['password']

    def _password_check(self, password: typing.Optional[str] = None) -> bool:
        """
        check if the keyword argument contains password and if it is correct password.
        :param password: the password
        :return: True if correct password is entered.
        """
        if password is not None:
            if password == self._password:
                # correct password
                return True
            else:
                _logger.warning('Wrong password')
                return False
        else:
            _logger.error('Please enter controller password as keyword argument')
            return False

    def toggle_laser(self, onoff: bool, **kwargs: str) -> None:
        """
        toggle laser on or off
        it will also toggle the warning lamp if configured
        :param onoff: True if on, else False
        :param kwargs: password needed as a keyword argument
        """
        if self._password_check(**kwargs):
            super(PaladinSecuredLogger, self).toggle_laser(onoff)

    def toggle_shutter(self, onoff: bool, **kwargs: str) -> None:
        """
        toggle laser shutter. Only works when laser is is Normal operational mode or Alignment mode ready
        :param onoff: True if on, else False
        :param kwargs: password needed as a keyword argument
        """
        if self._password_check(**kwargs):
            super(PaladinSecuredLogger, self).toggle_shutter(onoff)

    def toggle_alignment_mode(self, onoff: bool, **kwargs: str) -> None:
        """
        toggle laser alignment mode. Only works when laser is is Normal operational mode.
        :param onoff: True if on, else False
        :param kwargs: password needed as a keyword argument
        """
        if self._password_check(**kwargs):
            super(PaladinSecuredLogger, self).toggle_alignment_mode(onoff)

    def toggle_current_mode(self, onoff: bool, **kwargs: str) -> None:
        """
        toggle laser current mode.
        In current mode, user can change the laser current (0~110).
        In light loop mode, the laser will adjust the laser current to stabilize the output power to 3.75W
        :param onoff: True for current mode, False for light loop
        :param kwargs: password needed as a keyword argument
        """
        if self._password_check(**kwargs):
            super(PaladinSecuredLogger, self).toggle_current_mode(onoff)

    def set_laser_current(self, current: float, **kwargs: str) -> None:
        """
        set the laser current.
        Only functional when the laser is in current mode. See `toggle_current_mode` method
        :param current: laser current. 0~110
        :param kwargs: password needed as a keyword argument
        """
        if self._password_check(**kwargs):
            super(PaladinSecuredLogger, self).set_laser_current(current)
