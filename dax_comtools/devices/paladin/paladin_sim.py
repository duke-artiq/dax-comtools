import logging
import typing

from .constants import COMMAND, STATUS_DICT

__all__ = ['PaladinSim']

_logger = logging.getLogger(__name__)


class PaladinSim:
    def __init__(self, config_file: str):
        self._config_file = config_file

    # noinspection PyMethodMayBeStatic
    def ping(self) -> bool:
        """ for controller communication check """
        return True

    # noinspection PyMethodMayBeStatic
    def read_config(self, config_file: typing.Optional[str] = None) -> None:
        _logger.debug('config file %s loaded', config_file)

    # noinspection PyMethodMayBeStatic
    def open(self) -> None:
        _logger.debug('serial port opened')

    # noinspection PyMethodMayBeStatic
    def close(self, *, shutdown: bool = False) -> None:
        _logger.debug(f'serial port closed, shutdown={shutdown}')

    # noinspection PyMethodMayBeStatic
    def get_value(self, parameter_names: typing.List[str]) -> typing.Dict[str, typing.Union[float, bool, str]]:
        parameters: typing.Dict[str, typing.Union[float, bool, str]] = {}
        for parameter_name in parameter_names:
            if parameter_name in COMMAND['boolean_query']:
                parameters[parameter_name] = True
            elif parameter_name in COMMAND['float_query']:
                parameters[parameter_name] = 0.0
            elif parameter_name == 'status':
                parameters[parameter_name] = STATUS_DICT[8]  # Return `Normal operation`
        return parameters

    # noinspection PyMethodMayBeStatic
    def get_value_all(self) -> typing.Dict[str, typing.Union[float, bool, str]]:
        parameters: typing.Dict[str, typing.Union[float, bool, str]] = {}
        for parameter_name in COMMAND['boolean_query']:
            parameters[parameter_name] = True
        for parameter_name in COMMAND['float_query']:
            parameters[parameter_name] = 0.0
        parameters['status'] = STATUS_DICT[8]  # Return `Normal operation`
        return parameters

    # noinspection PyMethodMayBeStatic,PyUnusedLocal
    def toggle_laser(self, onoff: bool, **kwargs: str) -> None:
        _logger.debug('Turning %s the laser', 'on' if onoff else 'off')

    # noinspection PyMethodMayBeStatic,PyUnusedLocal
    def toggle_alignment_mode(self, onoff: bool, **kwargs: str) -> None:
        _logger.debug('Turning %s the alignment mode', 'on' if onoff else 'off')

    # noinspection PyMethodMayBeStatic,PyUnusedLocal
    def toggle_shutter(self, onoff: bool, **kwargs: str) -> None:
        _logger.debug('%s the shutter', 'opening' if onoff else 'closing')

    # noinspection PyMethodMayBeStatic,PyUnusedLocal
    def toggle_current_mode(self, onoff: bool, **kwargs: str) -> None:
        _logger.debug('laser mode set to %s', 'current mode' if onoff else 'light loop')

    # noinspection PyMethodMayBeStatic,PyUnusedLocal
    def set_laser_current(self, current: float, **kwargs: str) -> None:
        _logger.debug('the laser current set to %.2f', current)

    # noinspection PyMethodMayBeStatic
    def request_log(self) -> None:
        _logger.debug('log requested')

    def task(self) -> None:
        raise NotImplementedError

    def set_interval(self, interval: float) -> None:
        raise NotImplementedError

    def get_interval(self) -> float:
        raise NotImplementedError

    def start(self) -> None:
        raise NotImplementedError

    def stop(self) -> None:
        raise NotImplementedError

    def is_running(self) -> bool:
        raise NotImplementedError
