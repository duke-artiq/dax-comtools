from __future__ import annotations

import socket
import typing
import logging

_logger = logging.getLogger(__name__)
"""Module logger."""


def _to_bool(message: str) -> bool:
    """Convert a returned message to a boolean according to the CryoStation messaging protocol."""
    return message.startswith("T")


class CryoComm:
    """Class to provide Python communication with a Montana Instruments CryoStation.

    Based on code from Montana Instruments website: https://www.montanainstruments.com/Support/Software-Downloads/
    """

    DEFAULT_PORT: int = 7773
    """Default port to connect to."""

    _ip: str
    _port: int
    _socket: socket.socket

    def __init__(self, ip: str = 'localhost', port: int = DEFAULT_PORT, *, timeout: float = 5.0):
        """CryoComm - Constructor

        :param ip: Host to connect to
        :param port: Port to connect to
        :param timeout: Socket timeout
        """

        assert isinstance(ip, str)
        assert isinstance(port, int)
        assert isinstance(timeout, float)

        self._ip = ip
        self._port = port

        # Connect to the Cryostation
        _logger.info(f'Connecting to [{ip}]:{port}...')
        try:
            self._socket = socket.create_connection((ip, port), timeout=2 * timeout)
        except socket.timeout:
            _logger.error('Connection timed out, make sure the given address and port are correct and that the '
                          'CryoStation software is running with external control enabled')
            raise
        else:
            _logger.info('Connected')
        self._socket.settimeout(timeout)

    @property
    def ip(self) -> str:
        return self._ip

    @property
    def port(self) -> int:
        return self._port

    def __enter__(self) -> CryoComm:
        return self

    def __exit__(self, exc_type: typing.Any, exc_val: typing.Any, exc_tb: typing.Any) -> typing.Any:
        self.close()

    def write(self, message: str) -> None:
        """CryoComm - Write a message to the Cryostation"""
        assert isinstance(message, str)

        _logger.info(f'Write: "{message}"')

        if len(message) >= 100:
            raise ValueError('Message too long')

        total_sent: int = 0

        # Prepend the message length to the message
        message = f'{len(message):02d}{message:s}'

        while total_sent < len(message):
            # Send the message
            sent = self._socket.send(message[total_sent:].encode())

            if not sent:
                # If sent is zero, there is a communication issue
                raise RuntimeError("CryoComm:Cryostation connection lost during send")

            total_sent += sent

    def read(self) -> str:
        """CryoComm - read a message from the Cryostation"""

        _logger.info('Read...')

        chunks: typing.List[bytes] = []
        received: int = 0

        # Read the message length
        message_length = int(self._socket.recv(2).decode('UTF8'))

        while received < message_length:
            # Read the message
            chunk = self._socket.recv(message_length - received)

            if not chunk:
                # If an empty chunk is read, there is a communication issue
                raise RuntimeError("CryoComm:Cryostation connection lost during receive")

            chunks.append(chunk)
            received += len(chunk)

        message = ''.join([c.decode('UTF8') for c in chunks])
        _logger.info(f'Read: "{message}"')
        return message

    def query(self, message: str) -> str:
        """CryoComm - Send a message to the Cryostation and read the response"""
        self.write(message)
        return self.read()

    def close(self) -> None:
        """CryoComm - Destructor"""
        if self._socket:
            _logger.info('Closing connection')
            self._socket.shutdown(1)
            self._socket.close()

    """High-level functions"""

    def get_idle_state(self) -> bool:
        """Returns true if the system is idle. Returns false if the system is in any automatic or error mode."""
        return _to_bool(self.query("GIS"))

    def get_alarm_state(self) -> bool:
        """Returns true or false indicating the presence or absence of a system error."""
        return _to_bool(self.query("GAS"))

    def get_nitrogen_state(self) -> bool:
        """Returns true if nitrogen supply to the system is detected.
        Returns false if no nitrogen supply is detected."""
        return _to_bool(self.query("GNS"))

    def get_platform_temperature(self) -> float:
        """Returns the current platform temperature or -0.100 to indicate the platform temperature is not available.

        :return Temperature in Kelvin
        """
        return float(self.query("GPT"))

    def get_sample_temperature(self) -> float:
        """Returns the current sample temperature or -0.100 to indicate the sample temperature is not available.

        :return Temperature in Kelvin
        """
        return float(self.query("GST"))

    def get_stage_1_temperature(self) -> float:
        """Returns the current stage 1 temperature or -0.10 to indicate the stage 1 temperature is not available.

        :return Temperature in Kelvin
        """
        return float(self.query("GS1T"))

    def get_stage_2_temperature(self) -> float:
        """Returns the current stage 2 temperature or -0.10 to indicate the stage 2 temperature is not available.

        :return Temperature in Kelvin
        """
        return float(self.query("GS2T"))

    def get_chamber_pressure(self) -> float:
        """Returns the current chamber pressure or -0.1 to indicate the chamber pressure is not available.

        :return Pressure in mTorr
        """
        return float(self.query("GCP"))
