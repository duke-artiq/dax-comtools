import logging
import pyvisa
from pyvisa.constants import Parity, StopBits
import typing

# Import PSU classes from updated Keysight directory
from dax_comtools.devices.keysight_psu.psu.base import KeysightPowerSupplyBase
from dax_comtools.devices.keysight_psu.psu.base import (
    KeysightPowerSupplyConnectionError,
)
from dax_comtools.devices.keysight_psu.psu.autodetector import (
    KeysightPowerSupplyAutoDetector,
)
from dax_comtools.devices.keysight_psu.psu.offline import KeysightOfflinePowerSupply
from dax_comtools.devices.keysight_psu.psu.sim import KeysightPowerSupplySim
from dax_comtools.devices.keysight_psu.psu.e3631a import AgilentPowerSupplyE3631A
from dax_comtools.devices.keysight_psu.psu.e3633a import AgilentPowerSupplyE3633A
from dax_comtools.devices.keysight_psu.psu.e3634a import AgilentPowerSupplyE3634A


__all__ = ["AgilentPowerSupplyDriver"]

logger = logging.getLogger(__name__)
"""The module logger."""

_PSU_CLASSES: typing.Sequence[typing.Type[KeysightPowerSupplyBase]] = [
    AgilentPowerSupplyE3633A,
    AgilentPowerSupplyE3631A,
    AgilentPowerSupplyE3634A,
    KeysightOfflinePowerSupply,
    KeysightPowerSupplySim,
]
"""List of available PSU classes."""


class AgilentPowerSupplyDriver:
    """Driver for Agilent power supplies that use the rs232 serial connection interface."""

    _CONN_READ_TERMINATION = "\n"
    """Read termination character for serial communication to PSU"""
    _CONN_WRITE_TERMINATION = "\n"
    """Write termination character for serial communication to PSU"""

    VALID_BAUD_RATES: typing.Set[int] = {300, 600, 1200, 2400, 4800, 9600}
    """Valid baud rates."""

    VALID_MODELS: typing.Set[str] = {cls.get_device_model() for cls in _PSU_CLASSES}
    """Valid agilent model numbers."""

    def __init__(
        self,
        device: str,
        *,
        model: str = "",
        baud_rate: int = max(VALID_BAUD_RATES),
        bypass_remote_mode: bool = False,
        connection_optional: bool = False,
        simulation: bool = False,
    ):
        """Construct a new driver object.

        :param device: serial port device is connected to
        :param model: power supply model name, e.g. "E3631A"
        :param baud_rate: connection baud rate
        :param bypass_remote_mode: flag to bypass setting device in remote mode
        :param connection_optional: if True an exception will be thrown if the psu is not connected and reachable
        :param simulation: flag to run in simulation mode (dump all received messages to stdout)
        """
        assert isinstance(device, str)
        assert isinstance(model, str)
        assert isinstance(baud_rate, int)
        assert isinstance(bypass_remote_mode, bool)
        assert isinstance(connection_optional, bool)
        assert isinstance(simulation, bool)

        if baud_rate not in self.VALID_BAUD_RATES:
            raise ValueError(
                f"Invalid baud rate, valid options are {self.VALID_BAUD_RATES}"
            )

        # If a model was passed in, make sure that it is in the valid models list
        if model and model.lower() not in self.VALID_MODELS:
            raise ValueError(
                f"Invalid model number, valid options are {self.VALID_MODELS}."
                "For support of newer models please use the updated KeysightPowerSupplyDriver."
            )

        # Store attributes
        self._device: str = device
        self._model: str = model.lower()
        self._simulation: bool = simulation
        self._connection_optional: bool = connection_optional
        self._bypass_remote_mode: bool = bypass_remote_mode

        if not self._simulation:
            # Set up connection
            logger.debug(
                f'Setting up connection on serial device "{self._device}" with baud rate {baud_rate}'
            )
            self._conn: typing.Any = pyvisa.ResourceManager().open_resource(
                self._device, parity=Parity.none, stop_bits=StopBits.two
            )  # Typed as any
            self._conn.read_termination = self._CONN_READ_TERMINATION
            self._conn.write_termination = self._CONN_WRITE_TERMINATION
            self._conn.baud_rate = baud_rate
            # Set device mode and verify connection

        else:
            # Create simulation variables
            logger.info("Simulation enabled")
            self._conn = None

        self._psu = self._get_psu()
        # If a specific psu device is specified then create an instance of that device
        # otherwise get the psu id and attempt to create a psu instance based on the id

    def _get_psu(self) -> KeysightPowerSupplyBase:
        """Instantiates an AgilentPowerSupply class for the specified model. If no model is provided it attempts to
        identify the model using the autodetection feature.

        :returns: An instance of an implemented AgilentPowerSupplyBase class for the specified (or autodetected) model.
        """

        if not self._model:
            # If a model was not passed try to run autodetection
            autodetector = KeysightPowerSupplyAutoDetector(
                conn=self._conn,
                simulation=self._simulation,
                connection_optional=self._connection_optional,
                bypass_remote_mode=self._bypass_remote_mode,
            )
            self._model = autodetector.get_model_from_device().lower()

        # Try to instantiate the specified psu model
        try:
            if (
                self._simulation
                and self._model
                == KeysightPowerSupplyAutoDetector.get_device_model().lower()
            ):
                # If the system uses auto-detection and is in simulation mode, autodetector class will return
                #  own model name. Use generic simulation PSU in this case
                cls: typing.Type[KeysightPowerSupplyBase] = KeysightPowerSupplySim
            else:
                cls = {cls.get_device_model(): cls for cls in _PSU_CLASSES}[self._model]
        except KeyError:
            # If the provided model (or the autodetected model) is not valid then raise exception
            raise ValueError(
                f"Invalid model number. Received {self._model}. "
                f"Valid options are {self.VALID_MODELS}"
                "For support of newer models please use the updated KeysightPowerSupplyDriver."
            ) from None
        else:
            try:
                return cls(
                    conn=self._conn,
                    simulation=self._simulation,
                    connection_optional=self._connection_optional,
                    bypass_remote_mode=self._bypass_remote_mode,
                )
            except KeysightPowerSupplyConnectionError as error:
                # In the case that the check_connection check fails (when a psu model is explicitly specified along
                #   with connection_optional) instantiate the OfflinePowerSupply class.
                if self._connection_optional:
                    return KeysightOfflinePowerSupply(
                        conn=self._conn,
                        simulation=self._simulation,
                        connection_optional=self._connection_optional,
                        bypass_remote_mode=self._bypass_remote_mode,
                    )
                else:
                    raise error

    # noinspection PyMethodMayBeStatic
    def ping(self) -> bool:
        """Always returns true (indication that the driver is running)."""
        return True

    def get_device_id(self) -> str:
        """Gets the device ID from the Agilent PSU"""
        return self._psu.get_id_from_device()

    def get_device_model(self) -> str:
        """Gets the device model from the Agilent PSU"""
        return self._psu.get_model_from_device()

    def check_connection(self) -> None:
        """Check the connection, raises an exception in case of problems. Take into account if using the offline psu
        driver, in which case no problems are raised

        :raises AgilentPowerSupplyConnectionError: Raised in case there is a bad connection when something is expected
        """
        self._psu.check_connection()

    def enable_remote_mode(self) -> None:
        """Configure the power supply to be in REMOTE mode. Sending or receiving data over the
        RS-232 interface when not configured for remote operation can cause unpredictable results."""
        self._psu.enable_remote_mode()

    def enable_outputs(self, is_enabled: bool) -> None:
        """Sets the status of all outputs of the power supply.

        :param is_enabled: Enables/Disables power supply outputs
        """
        self._psu.toggle_all_outputs(status=is_enabled)

    def set_output_target(self, target_output: str) -> None:
        """Sets the output target of the powers supply.

        :param target_output: The name of the output on the power supply (name is model dependent).
        """
        self._psu.set_target_output(target_output)

    def apply(
        self, target_output: str, voltage: float, current: typing.Optional[float] = None
    ) -> None:
        """Sets the voltage and current on the specified target output of the power supply.

        :param target_output: The name of the output on the power supply (name is model dependent)
        :param voltage: The desired voltage
        :param current: The desired current
        """
        self._psu.apply(voltage, current, target_output)

    def get_output_settings(
        self, target_output: str = ""
    ) -> typing.Tuple[float, float]:
        """Returns the voltage and current values for the specified target.

        :param target_output: The target output
        :returns: A tuple of settings for the target output
        """
        return self._psu.get_output_settings(target_output)

    def clear(self) -> None:
        """Clears the event register and error array of the power supply."""
        self._psu.clear()

    def reset(self) -> None:
        """Resets the power supply to default settings"""
        self._psu.reset()

    def close(self) -> None:
        """Close serial connection."""
        logger.debug("Closing serial connection")
        if not self._simulation:
            self._conn.close()

    def is_online(self) -> bool:
        """True if the device is online."""
        return self._psu.is_online()
