"""
Simple querying driver for MAUI Scope (Teledyne Wavesurfer 3104Z)
"""

from __future__ import annotations

import logging
import typing

import pyvisa

__all__ = ['MAUI']

logger = logging.getLogger(__name__)


class MAUI:
    _CMD_FREQ: typing.ClassVar[str] = "PAVA? FREQ"
    _CMD_AMP: typing.ClassVar[str] = "PAVA? AMPL"

    DEFAULT_TIMEOUT: typing.ClassVar[int] = 5000
    """Default timeout in milliseconds."""
    CHANNELS: typing.ClassVar[typing.FrozenSet[int]] = frozenset(i + 1 for i in range(4))
    """The available channels on the device."""
    SLOTS: typing.ClassVar[typing.FrozenSet[int]] = frozenset(i + 1 for i in range(6))
    """The available measurement slots as defined on the scope."""
    STATS: typing.ClassVar[typing.FrozenSet[str]] = frozenset(('avg', 'low', 'high', 'sigma', 'sweeps', 'last'))
    """Measurement statistics from a slot."""

    def __init__(self, device: str, *, timeout: int = DEFAULT_TIMEOUT):
        """Create a new MAUI driver.

        :param device: PyVISA resource string
        :param timeout: Connection timeout in milliseconds
        """
        assert isinstance(device, str)
        assert isinstance(timeout, int)

        logger.debug(f'Connecting to device "{device}"')
        self._conn: typing.Any = pyvisa.ResourceManager().open_resource(device)
        self._conn.timeout = timeout  # time to timeout error (in milliseconds)

    def _query(self, message: str) -> str:
        assert isinstance(message, str)

        logger.debug(f'Query: {message}')
        response = self._conn.query(message)
        logger.debug(f'Response: {response}')
        return response

    def get_freq(self, channel: int) -> float:
        assert channel in self.CHANNELS
        response = self._query(f'C{channel:d}:{self._CMD_FREQ:s}')
        freq = float(response.split(',')[1])
        logger.info(f'Frequency reading of channel {channel}: {freq}')
        return freq

    def get_amp(self, channel: int) -> float:
        assert channel in self.CHANNELS
        response = self._query(f'C{channel:d}:{self._CMD_AMP:s}')
        amp = float(response.split(',')[1])
        logger.info(f'Amplitude reading of channel {channel}: {amp}')
        return amp

    def get_freq_amp(self, channel: int) -> typing.Tuple[float, float]:
        freq = self.get_freq(channel)
        amp = self.get_amp(channel)
        return freq, amp

    def get_cust_stat(self, slot: int) -> typing.Dict[str, float]:
        """Get the statistic requested from the custom measurement slot defined on the scope.

        The query returns ``UNDEF`` if that particular slot is not set on the scope.
        Available statistics: ``{AVG, LOW, HIGH, SIGMA, SWEEPS, LAST}`` (see :attr:`STATS`).
        """
        assert slot in self.SLOTS
        response = self._query(f'PAST? CUST,P{slot:d}')
        split = response.split(',')
        stats = {s.lower(): float(v.split(' ')[0]) for s, v in zip(split[4::2], split[5::2])}
        logger.info(f'Custom statistic reading of slot {slot}: {stats}')
        return stats

    def close(self) -> None:
        """Close connection."""
        logger.debug('Closing connection')
        self._conn.close()

    def __enter__(self) -> MAUI:
        return self

    def __exit__(self, exc_type: typing.Any, exc_val: typing.Any, exc_tb: typing.Any) -> typing.Any:
        self.close()
