"""Wavelength Electronics LFI3751.

https://www.teamwavelength.com/product/lfi3751-5a-digital-temperature-control-instrument/
"""

from __future__ import annotations

import typing
import logging
import serial

__all__ = ['LFI3751']

logger = logging.getLogger(__name__)


class LFI3751:
    """Driver for a Wavelength Electronics LFI3751 temperature controller with serial communication."""

    DEFAULT_UNIT_NR: typing.ClassVar[int] = 1
    DEFAULT_TIMEOUT: typing.ClassVar[typing.Optional[float]] = 5.0

    TYPE_READ: typing.ClassVar[int] = 1
    TYPE_WRITE: typing.ClassVar[int] = 2

    CODE_ACT_T: typing.ClassVar[int] = 1
    """Actual temperature measurement."""
    CODE_LOCAL: typing.ClassVar[int] = 53
    """Restore to local operation."""

    _BAUD_RATE: typing.ClassVar[int] = 19200
    _RESPONSE_LENGTH: typing.ClassVar[int] = 21

    def __init__(self, device: str, unit_nr: int = DEFAULT_UNIT_NR, *,
                 timeout: typing.Optional[float] = DEFAULT_TIMEOUT, **kwargs: typing.Any):
        """Initialize a LFI3751 driver.

        :param device: The serial device
        :param unit_nr: The unit number
        :param timeout: The read timeout in seconds
        :param kwargs: Keyword arguments passed to the serial device
        """
        assert isinstance(device, str)
        assert isinstance(unit_nr, int)
        assert isinstance(timeout, float) or timeout is None

        if not 0 < unit_nr < 100:
            raise ValueError('Unit number must be in the range [1,99]')

        # Store the unit number
        self._unit_nr: int = unit_nr
        # Obtain serial connection
        logger.debug(f'Connecting to device {device} (unit number {unit_nr})')
        self._serial = serial.Serial(device, baudrate=self._BAUD_RATE, bytesize=serial.EIGHTBITS,
                                     parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, timeout=timeout, **kwargs)

    def __enter__(self) -> LFI3751:
        return self

    def __exit__(self, exc_type: typing.Any, exc_val: typing.Any, exc_tb: typing.Any) -> typing.Any:
        self.close()

    def close(self) -> None:
        """Close connection to the device."""
        logger.debug('Closing connection')
        self._serial.close()

    @staticmethod
    def _fcs_packet(packet: str, *, end: str = '') -> bytes:
        """Add frame checksum to packet.

        :param packet: Packet as a string
        :param end: End of the packet, goes after the checksum
        :return: Packet with frame checksum as bytes
        """
        assert isinstance(packet, str)
        assert isinstance(end, str)

        # Calculate checksum
        fcs: int = 0x00
        for b in packet.encode('ascii'):
            fcs ^= b

        # Return packet with checksum
        fcs_packet = f'{packet:s}{fcs:02x}{end:s}'
        assert len(fcs_packet) == len(packet) + len(end) + 2, 'FCS length mismatch'
        return fcs_packet.encode('ascii')

    def _encode_command_packet(self, type_: int, code: int, data: float = 0.0) -> bytes:
        if type_ not in {self.TYPE_READ, self.TYPE_WRITE}:
            raise ValueError('Invalid command type')
        if not 0 <= code < 100:
            raise ValueError('Invalid command code')
        if not -1000 < data < 1000:
            raise ValueError('Command data out of range')

        command = f'!1{self._unit_nr:02d}{type_:d}{code:02d}{data:0=+8.3f}'
        assert len(command) == 15, 'Command length incorrect'
        return self._fcs_packet(command)

    def _decode_response_packet(self, packet: bytes) -> str:
        assert isinstance(packet, bytes)

        # Decode message
        assert len(packet) == self._RESPONSE_LENGTH, 'Response length incorrect'
        msg = packet.decode('ascii')

        if msg[0:2] != '@1':
            raise ValueError('Invalid response packet start character')
        if int(msg[2:4]) != self._unit_nr:
            raise ValueError('Response packet unit number mismatch')
        if msg[7:9] != '00':
            raise ValueError(f'Action failure with end code "{msg[7:9]}"')

        # Return data
        return msg[9:17]

    def _query(self, command_packet: bytes) -> str:
        assert isinstance(command_packet, bytes)

        # Write command packet
        logger.debug('Write command packet')
        self._serial.write(command_packet)

        # Read response packet
        logger.debug('Read response packet')
        response_packet = self._serial.read(self._RESPONSE_LENGTH)

        if not response_packet:
            raise serial.SerialException('Device did not respond (device probably not connected)')
        if len(response_packet) < self._RESPONSE_LENGTH:
            raise serial.SerialException('Device response too short (connection probably timed out)')
        if len(response_packet) > self._RESPONSE_LENGTH:
            raise serial.SerialException('Device response too long (unexpected error)')

        # Decode and return response
        response = self._decode_response_packet(response_packet)
        logger.debug(f'Decoded response data: {response}')
        return response

    def flush(self) -> None:
        """Clear output buffer (of the OS) and flush input buffer."""
        logger.debug('Clearing output buffer and flushing input buffer')
        self._serial.reset_output_buffer()
        self._serial.reset_input_buffer()

    def get_temperature(self) -> float:
        """Get actual temperature measurement."""
        temp = self._query(self._encode_command_packet(self.TYPE_READ, self.CODE_ACT_T))
        logger.info(f'Temperature reading: {temp}')
        return float(temp)

    def local_mode(self) -> None:
        """Revert device state to local mode (i.e. front panel control)."""
        logger.info('Revert device state to local mode')
        self._query(self._encode_command_packet(self.TYPE_WRITE, self.CODE_LOCAL))
