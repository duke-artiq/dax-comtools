import logging
import serial
import typing
import time

logger = logging.getLogger(__name__)


class KoradKADriver:
    """Driver for Korad KA-series programmable power supplies."""

    """Comm baud rate."""
    BAUD_RATE: int = 9600

    """Default to Channel 1"""
    DEFAULT_CHANNEL: int = 1

    _sim_output_stack: typing.List[str] = []

    def __init__(self, device: str, *,
                 write_timeout: float = 1, valid_channels=[1], read_delay: float = 0.1, simulation: bool = False,
                 **kwargs: typing.Any):
        """
        :param device: serial port device is connected to
        :param write_timeout: write timeout in seconds
        :param valid_channels: list of valid channels
        :param read_delay: delay between write and read for read commands
        :param simulation: flag to run in simulation mode (dump all received messages to stdout)
        :param kwargs: Keyword arguments passed to the serial object
        """
        assert isinstance(device, str), f'device {device} is not a string'
        assert isinstance(valid_channels, list), f'valid_channels {valid_channels} is not a list'
        assert isinstance(read_delay, (int, float)), f'read_delay {read_delay} is not a number'
        assert isinstance(simulation, bool), f'simulation {simulation} is not a boolean'
        assert isinstance(write_timeout, (int, float)), f'write_timeout {write_timeout} is not a number'

        self._valid_channels: typing.List[int] = valid_channels
        self._read_delay: float = read_delay
        self.simulation: bool = simulation

        # don't need to open connection to device when simulating
        if simulation:
            return

        self._conn = serial.Serial(port=device, baudrate=self.BAUD_RATE,
                                   bytesize=serial.EIGHTBITS, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE,
                                   write_timeout=write_timeout,
                                   **kwargs)

    # noinspection PyMethodMayBeStatic
    def ping(self):
        """Always returns true (indication that the driver is running)."""
        return True

    def _write(self, message: str):
        if self.simulation:
            self._sim_output_stack += [message]
            logger.info(message)
        else:
            try:
                self._conn.write(message.encode("ascii"))
            except serial.SerialTimeoutException:
                logger.warning("Serial Write Timeout")

    def _read(self) -> str:
        """
        Read serial buffer
        :return: full serial buffer
        """
        if not self.simulation:
            try:
                message = self._conn.read(self._conn.in_waiting).decode('utf-8')
            except Exception:
                raise Exception("Serial Read Error")
        else:  # self.simulation
            message = 'sim'

        return message

    SET_COMMAND = '{cmd}{channel}:{value}\r\n'
    GET_COMMAND = '{cmd}{channel}?\r\n'

    def set_param(self, cmd: str, value: float, channel: int = 1):
        """
        Set parameter of channel
        :param cmd: command string
        :param value: value to set parameter to
        :param channel: channel number
        """
        assert isinstance(cmd, str), f'cmd {cmd} is not a string'
        assert isinstance(channel, int), f'channel {channel} is not an integer'
        assert isinstance(value, (int, float)), f'value {value} is not a number'
        assert channel in self._valid_channels

        self._write(self.SET_COMMAND.format(cmd=cmd, channel=channel, value=value))

    def get_param(self, cmd: str, channel: int = 1) -> float:
        """
        Get parameter of channel
        :param cmd: command string
        :param channel: channel number
        :return: parameter value
        """
        assert isinstance(cmd, str), f'cmd {cmd} is not a string'
        assert isinstance(channel, int), f'channel {channel} is not an integer'
        assert channel in self._valid_channels, f'channel {channel} is not a valid channel in {self._valid_channels}'

        self._conn.flush()
        self._write(self.SET_COMMAND.format(cmd=cmd, channel=channel))
        time.sleep(self._read_delay)
        message = self._read()
        return float(message)

    """Command Dictionary"""
    SET_CURRENT_COMMAND: str = "ISET"
    SET_VOLTAGE_COMMAND: str = "VSET"
    IOUT_COMMAND: str = "IOUT"
    VOUT_COMMAND: str = "VOUT"

    def set_current(self, current: float, channel: int = 1):
        """
        Set current of channel
        :param current: current in amps
        :param channel: channel number
        """
        assert isinstance(channel, int), f'channel {channel} is not an integer'
        assert isinstance(current, (int, float)), f'current {current} is not a number'
        assert channel in self._valid_channels, f'channel {channel} is not a valid channel in {self._valid_channels}'

        self.set_param(self.SET_CURRENT_COMMAND, current, channel)

    def set_voltage(self, voltage: float, channel: int = 1):
        """
        Set voltage of channel
        :param voltage: voltage in volts
        :param channel: channel number
        """
        assert isinstance(channel, int), f'channel {channel} is not an integer'
        assert isinstance(voltage, (int, float)), f'voltage {voltage} is not a number'
        assert channel in self._valid_channels, f'channel {channel} is not a valid channel in {self._valid_channels}'

        self.set_param(self.SET_VOLTAGE_COMMAND, voltage, channel)

    def get_setpoint_voltage(self, channel: int = 1) -> float:
        """
        Get setpoint voltage of channel
        :param channel: channel number
        :return: voltage in volts
        """
        assert isinstance(channel, int), f'channel {channel} is not an integer'
        assert channel in self._valid_channels, f'channel {channel} is not a valid channel in {self._valid_channels}'

        return self.get_param(self.SET_VOLTAGE_COMMAND, channel)

    def get_set_current(self, channel: int = 1) -> float:
        """
        Get set current of channel
        :param channel: channel number
        :return: current in amps
        """
        assert isinstance(channel, int), f'channel {channel} is not an integer'
        assert channel in self._valid_channels, f'channel {channel} is not a valid channel in {self._valid_channels}'

        return self.get_param(self.SET_CURRENT_COMMAND, channel)

    def get_output_current(self, channel: int = 1) -> float:
        """
        Get output current of channel
        :param channel: channel number
        :return: current in amps
        """
        assert isinstance(channel, int), f'channel {channel} is not an integer'
        assert channel in self._valid_channels, f'channel {channel} is not a valid channel in {self._valid_channels}'

        return self.get_param(self.IOUT_COMMAND, channel)

    def get_output_voltage(self, channel: int = 1) -> float:
        """
        Get output voltage of channel
        :param channel: channel number
        :return: voltage in volts
        """
        assert isinstance(channel, int), f'channel {channel} is not an integer'
        assert channel in self._valid_channels, f'channel {channel} is not a valid channel in {self._valid_channels}'

        return self.get_param(self.VOUT_COMMAND, channel)
