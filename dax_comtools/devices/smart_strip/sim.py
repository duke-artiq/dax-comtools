import logging
import json
import collections.abc

from .driver import SmartPowerStrip as _SmartPowerStrip

__all__ = ['SmartPowerStripSim']

logger = logging.getLogger(__name__)


def update(d, u):
    """Recursive dict update."""
    for k, v in u.items():
        if isinstance(v, collections.abc.Mapping):
            d[k] = update(d.get(k, {}), v)
        else:
            d[k] = v
    return d


class SmartPowerStripSim(_SmartPowerStrip):
    _JSON_REPLY = {
        "system": {
            "get_sysinfo": {
                "deviceId": "smart_power_strip_sim",
            }
        }
    }

    def __init__(self, *args, **kwargs):
        logger.debug('Starting simulated smart power strip controller')
        super(SmartPowerStripSim, self).__init__(*args, **kwargs)

    def __send_command(self, command, *, protocol):
        logger.debug(f'{protocol} send command: {command}')
        return update(json.loads(command), self._JSON_REPLY)

    def _tcp_send_command(self, command):
        return self.__send_command(command, protocol='TCP')

    def _udp_send_command(self, command):
        return self.__send_command(command, protocol='UDP')
