from .KasaSmartPowerStrip import SmartPowerStrip as _SmartPowerStrip

__all__ = ['SmartPowerStrip']


class SmartPowerStrip(_SmartPowerStrip):

    # noinspection PyMethodMayBeStatic
    def ping(self):
        return True
