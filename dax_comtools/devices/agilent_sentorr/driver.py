"""
Agilent Varian senTorr Gauge Controller
"""

from __future__ import annotations

import logging
import typing

import pyvisa
from pyvisa.constants import Parity

__all__ = ['AgilentsenTorrIonGauge']

logger = logging.getLogger(__name__)


class AgilentsenTorrIonGauge:
    _CONN_READ_TERMINATION = '\r'
    _CONN_WRITE_TERMINATION = '\r'

    _CMD_PRESSURE = '#0102I1'

    _BAUD_RATE = 9600
    """Baud Rate, Parity, and 2digit unique bus address is set on device"""

    def __init__(self, device: str):
        assert isinstance(device, str)

        self._device: str = device

        logger.debug(f'Serial device "{self._device}"')
        self._conn: typing.Any = pyvisa.ResourceManager().open_resource(self._device, parity=Parity.none)
        self._conn.read_termination = self._CONN_READ_TERMINATION
        self._conn.write_termination = self._CONN_WRITE_TERMINATION
        self._conn.baud_rate = self._BAUD_RATE

    def _query(self, message: str) -> str:
        assert isinstance(message, str)

        logger.debug(f'Query: {message}')
        response = self._conn.query(message)
        logger.debug(f'Response: {response}')
        return response

    def close(self) -> None:
        """Close serial connection."""
        logger.debug('Closing serial connection')
        self._conn.close()

    def get_pressure(self) -> float:
        response = self._query(self._CMD_PRESSURE)
        logger.info(f'Pressure reading: {response}')
        return float(response[1:])

    def __enter__(self) -> AgilentsenTorrIonGauge:
        return self

    def __exit__(self, exc_type: typing.Any, exc_val: typing.Any, exc_tb: typing.Any) -> typing.Any:
        self.close()
