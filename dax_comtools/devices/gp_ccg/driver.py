"""
Granville Phillips 500 Series CCG Driver
Ely Novakoski, March 2022
"""

import typing
import logging

import serial

__all__ = ['GranvilleCCG']

logger = logging.getLogger(__name__)


class GranvilleCCG:
    _CONN_READ_TERMINATION = '\r'
    _CONN_WRITE_TERMINATION = '\r'
    _BAUD_RATE = 9600
    _ENCODING = 'ascii'

    DEFAULT_TIMEOUT = 3.0
    """Default serial timeout in seconds."""

    # COMMANDS
    _CMD_PRESSURE = 'RD'
    _CMD_ONOFF = 'IG'
    _CMD_VERSION = 'VER'

    def __init__(self, device: str, *, timeout: float = DEFAULT_TIMEOUT):
        assert isinstance(device, str)
        self._conn: typing.Any = serial.Serial(
            device,
            baudrate=self._BAUD_RATE,
            timeout=timeout
        )

    def flush(self):
        self._conn.flush()

    def _query(self, message: str) -> str:
        assert isinstance(message, str), f'message {message} is not a string'

        logger.debug(f'Query: {message}')
        encoded_message = f'{message}{self._CONN_WRITE_TERMINATION}'.encode(self._ENCODING)
        self._conn.write(encoded_message)
        response = self._conn.read_until(self._CONN_READ_TERMINATION).decode(self._ENCODING)
        logger.debug(f'Response: {response}')
        return response[2:-1]

    def close(self) -> None:
        """Close serial connection."""
        logger.debug('Closing serial connection')
        self._conn.close()

    def get_pressure(self) -> float:
        response = self._query(self._CMD_PRESSURE)
        logger.info(f'Pressure reading: {response}')
        return float(response)

    def get_version(self) -> typing.Sequence[str]:
        response = self._query(self._CMD_VERSION)[2:-1]
        return response.split(',')

    def is_on(self) -> bool:
        response = self._query(self._CMD_ONOFF)
        if response == '1 ON':
            return True
        elif response == '0 OFF':
            return False
        else:
            raise RuntimeError(f'Unexpected response: {response}')

    def __enter__(self):
        return self

    def set(self, state: bool):
        response = self._query(f'{self._CMD_ONOFF}  {"1" if state else "0"}')
        return response

    def __exit__(self, exc_type: typing.Any, exc_val: typing.Any, exc_tb: typing.Any) -> typing.Any:
        self.close()
