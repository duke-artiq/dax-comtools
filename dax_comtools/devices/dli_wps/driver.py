import requests
import typing
import logging

logger = logging.getLogger(__name__)


class WPS:
    """Simple HTTP driver for communicating with the [Web Power Switch](webpowerswitch.com).

    This driver is for an older version of the device. Newer versions (such as the one at the above link) have more
    sophisticated APIs available. This version, however, seems to rely on commands communicated over HTTP.

    **NOTE: this "API" is insecure for a number of reasons. First, it's HTTP, not HTTPS. Second, and more importantly,
    authentication credentials are encoded in the request. In other words, the username and password will be sent in
    PLAIN TEXT as part of the HTTP request. Do not use this on an insecure network.**
    """

    DEFAULT_NUM_OUTLETS: typing.ClassVar[int] = 8
    """Default number of outlets."""
    DEFAULT_RETRIES: typing.ClassVar[int] = 5
    """Default number of retries."""

    _url_base: str
    _num_outlets: int
    _retries: int
    _simulation: bool

    def __init__(self, wps_ip: str, user: str, password: str, *,
                 num_outlets: int = DEFAULT_NUM_OUTLETS, retries: int = DEFAULT_RETRIES, simulation: bool = False):
        """
        :param wps_ip: the IP address of the power supply
        :param user: the username for device authentication
        :param password: the password for device authentication
        :param num_outlets: the number of available outlets for remote switching, defaults to :const:`8`
        :param retries: the number of retries in case of exceptions, defaults to :const:`5`
        :param simulation: flag to run in simulation mode (dump all received messages to stdout),
            defaults to :const:`False`
        """
        assert isinstance(wps_ip, str)
        assert isinstance(user, str)
        assert isinstance(password, str)
        assert isinstance(num_outlets, int)
        assert isinstance(retries, int)
        assert isinstance(simulation, bool)

        self._url_base = f'http://{user}:{password}@{wps_ip}'
        self._num_outlets = max(1, num_outlets)
        self._retries = max(0, retries)
        self._simulation = simulation

    # noinspection PyMethodMayBeStatic
    def ping(self):
        """Always returns true (indication that the driver is running)."""
        return True

    def _write(self, url: str, data: typing.Dict[typing.Any, typing.Any]):
        if self._simulation:
            logger.info(f'Request URL: {url}\nRequest data: {data}')
        else:
            logger.debug(f'Request URL: {url}\nRequest data: {data}')
            retries = self._retries
            while retries >= 0:
                try:
                    response: requests.Response
                    with requests.post(url, data=data, timeout=1.0) as response:
                        logger.debug(f'Response: {response.status_code}')
                        if response.status_code != 200:
                            response.raise_for_status()
                except requests.exceptions.ConnectionError:
                    logger.debug(f"Failed to establish connection, retry attempts left {retries}")
                    if retries <= 0:
                        logger.error(f'Request failed after {retries} retries')
                        raise
                else:
                    break
                finally:
                    retries -= 1

    def switch(self, outlet: int, state: bool):
        """Switch the desired outlet on or off.
        Note: outlets are 1-indexed (i.e. it's 1-8, not 0-7).

        :param outlet: outlet number to switch
        :param state: on (:const:`True`) or off (:const:`False`)
        """
        # response code is still 200 for non-existing outlet, so we need to check it ourselves
        if not 0 < outlet <= self._num_outlets:
            raise IndexError(f'Index {outlet} out of bounds for {self._num_outlets} outlets')
        self._write(f'{self._url_base}/outlet', {outlet: f'{"ON" if state else "OFF"}'})

    def switch_on(self, outlet: int):
        self.switch(outlet, True)

    def switch_off(self, outlet: int):
        self.switch(outlet, False)

    def switch_all(self, state: bool):
        """Switch all outlets on or off.
        Note: this is a very slow process (30+ seconds to complete).

        :param state: on (:const:`True`) or off (:const:`False`)
        """
        self._write(f'{self._url_base}/outlet', {'a': f'{"ON" if state else "OFF"}'})

    def switch_all_on(self):
        self.switch_all(True)

    def switch_all_off(self):
        self.switch_all(False)
