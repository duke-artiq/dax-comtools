# SHT2x driver

The SHT21 driver (`sht21.py`) is copied from [this GitHub repository](https://github.com/jaques/sht21_python) and
modified to work with our Adafruit I2C driver. See the attached licence file.

According to the documentation, the driver should be compatible with all SHT2x devices (SHT20, SHT21, and SHT25).
