"""
`flow_sensor`
====================================================

This module can be used to detect the rate of flow using a hall sensor.

The output of this module has units of L/m and is calculated from the
frequency of pulses received on a RPi GPIO
"""

import time
import logging

FLOW_SENSOR_DEFAULT_BOUNCETIME: int = 4
"""Default bounce time in ms"""
FLOW_SENSOR_DEFAULT_SAMPLE_MS: int = 3000
"""Default total sampling time in ms"""
FLOW_SENSOR_DEFAULT_DIVISOR: float = 6.6
"""Default divisor for converting frequency to flow
E.g. FLOW = FREQ/6.6
The default 6.6 comes from the conversion rate for a specific flowmeter linked below
https://www.seeedstudio.com/Water-Flow-Sensor-YF-B6-p-2883.html"""

logger = logging.getLogger(__name__)


class FlowSensor:

    def __init__(self, pin, bouncetime, sample_ms, freq_divisor):
        assert isinstance(pin, int)
        assert isinstance(bouncetime, int)
        assert isinstance(sample_ms, int)
        assert isinstance(freq_divisor, float)
        self._count = 0
        self._pin = pin
        self._bouncetime = bouncetime
        self._sample_s = sample_ms/1000
        self._freq_divisor = freq_divisor

    def get_flow(self):
        """
        Calculate the flow in L/m using the incoming pulses from a hall sensor

        :return: flow of liquid passing through sensor in L/m
        """
        self._count = 0

        import RPi.GPIO as GPIO  # type: ignore[import]
        # Tell GPIO library to use GPIO references
        GPIO.setmode(GPIO.BCM)

        logger.info(f"Setup GPIO pin as input on GPIO{self._pin}")

        # Set Switch GPIO as input
        # Pull high by default
        GPIO.setup(self._pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        GPIO.add_event_detect(self._pin, GPIO.RISING, callback=self.__sensor_callback, bouncetime=self._bouncetime)

        time.sleep(self._sample_s)
        GPIO.cleanup()

        # For this device Q = Frequency/6.6 where Q is the L/m
        return (self._count/self._sample_s)/self._freq_divisor

    def __sensor_callback(self, _):
        # Increment the count
        self._count += 1
