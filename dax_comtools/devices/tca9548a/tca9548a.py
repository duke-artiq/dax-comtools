"""
This TCA9548A driver was inspired by the SparkFun Qwiic TCA9548A driver:

- https://github.com/sparkfun/Qwiic_TCA9548A_Py
- https://www.sparkfun.com/products/16784
"""

import typing

import dax_comtools.util.adafruit.i2c_device

__all__ = ['TCA9548A']


class TCA9548A:
    """TCA9548A I2C MUX driver."""

    DEFAULT_ADDRESS: typing.ClassVar[int] = 0x70
    """Default I2C address."""
    VALID_ADDRESSES: typing.ClassVar[typing.Set[int]] = set(range(DEFAULT_ADDRESS, DEFAULT_ADDRESS + 2 ** 3))
    """Valid I2C addresses."""

    NUM_CHANNELS: typing.ClassVar[int] = 8
    """Number of channels."""

    def __init__(self, i2c_bus: str, address: int = DEFAULT_ADDRESS):
        assert isinstance(i2c_bus, str)
        assert isinstance(address, int)

        if address not in self.VALID_ADDRESSES:
            raise ValueError(f'Address {address} not valid')

        # Create I2C device
        self._i2c = dax_comtools.util.adafruit.i2c_device.I2CDevice(i2c_bus, address)

    def read(self) -> bytearray:
        """Read the status."""
        buf = bytearray(1)
        with self._i2c as i2c:
            i2c.readinto(buf)
        return buf

    def write(self, register: bytes) -> None:
        """Directly write the status."""
        assert isinstance(register, (bytes, bytearray))
        assert len(register) > 0, 'Can not write empty data'
        with self._i2c as i2c:
            i2c.write(register[:1])

    def get_status(self) -> typing.Sequence[bool]:
        """Get the status of all channels as a sequence of booleans."""
        buf = self.read()
        return [bool(buf[0] & (0x01 << i)) for i in range(self.NUM_CHANNELS)]

    def _channels_to_register(self, channels: typing.Sequence[int]) -> bytearray:
        register = 0x00
        for c in channels:
            assert isinstance(c, int), 'Channel number must be an int'
            if not 0 <= c < self.NUM_CHANNELS:
                raise ValueError(f'Channel {c} out of range')
            register |= 0x01 << c
        return bytearray([register])

    def set_channels(self, *channels: int) -> None:
        """Set channels (disabling all other channels)."""
        self.write(self._channels_to_register(channels))

    def enable_channels(self, *channels: int) -> None:
        """Enable channels (leaving other channels in their current state)."""
        register = self.read()
        register[0] |= self._channels_to_register(channels)[0]
        self.write(register)

    def disable_channels(self, *channels: int) -> None:
        """Disable channels (leaving other channels in their current state)."""
        register = self.read()
        register[0] &= self._channels_to_register(channels)[0] ^ 0xFF
        self.write(register)

    def enable_all(self) -> None:
        """Enable all channels."""
        self.write(bytes([0xFF]))

    def disable_all(self) -> None:
        """Disable all channels."""
        self.write(bytes([0x00]))
