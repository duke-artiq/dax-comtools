import logging
import socket
import typing

__all__ = ['SpectronixDriver']

_logger = logging.getLogger(__name__)

# Default configuration for device communication
_MAX_RESPONSE = 1024
_MAX_ATTEMPT = 10
_END_MESSAGE = 255
_CRLF = '\r\n'

_NUM_CHANNELS = 32
"""The total number of channels."""

_S_T = typing.Dict[int, typing.Dict[str, typing.Any]]  # Status type

_DUMMY_DICT: _S_T = {
    channel: {
        'RF switch': True,
        'Rf source': 'e',
        'gain': 15,
        'internal dds': {
            'frequency': 200 * 10 ** 6,
            'phase': 0,
            'amplitude': 10000
        }
    } for channel in range(_NUM_CHANNELS)
}
"""Dummy status dictionary for simulation mode."""


class SpectronixDriver:
    """Driver for Spectronix 32-ch RF amplifier."""

    DEFAULT_IP = '192.168.1.160'
    """Default factory IP."""
    DEFAULT_PORT = 2101
    """Default factory port."""

    def __init__(self, host: str = DEFAULT_IP, port: int = DEFAULT_PORT, *,
                 timeout: float = 5.0, simulation: bool = False) -> None:
        assert isinstance(host, str)
        assert isinstance(port, int)
        assert isinstance(timeout, float) and timeout >= 0.0
        assert isinstance(simulation, bool)

        self._host = host
        self._port = port
        self._timeout = timeout

        self._is_open = False
        self._simulation = simulation

        # Open connection
        self.open()

    # noinspection PyMethodMayBeStatic
    def ping(self) -> bool:
        """ for controller communication check """
        return True

    def is_open(self) -> bool:
        """Returns :const:`True` if the connection to the device is open."""
        return self._is_open

    def open(self) -> None:
        """Opens a connection to the device."""
        if not self._simulation:
            _logger.debug(f'Opening network connection to "[{self._host}]:{self._port}"')

            # Open command line
            self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self._socket.settimeout(self._timeout)
            self._socket.connect((self._host, self._port))
        else:
            _logger.debug('Opening network connection : simulation mode')

        self._is_open = True

    def close(self) -> None:
        """Closes the network connection."""
        if not self._simulation:
            _logger.debug(f'Closing network connection to "{self._host}"')
            self._socket.close()
        else:
            _logger.debug('Closing network connection : simulation mode')

        self._is_open = False

    def _write(self, message: str) -> None:
        """Write message to the device."""
        assert isinstance(message, str)

        if self._simulation:
            _logger.debug(f'Writing message "{message}" : simulation mode')
        else:
            if not self.is_open():
                self.open()
            self._socket.send(f'{message}{_CRLF}'.encode())

    def _read(self) -> typing.Optional[str]:
        """Receive message from the device.

        :returns: message or None if no end string is found from the received message
        """
        if self._simulation:
            return ''
        else:
            if not self.is_open():
                self.open()

            result = ''
            for _ in range(_MAX_ATTEMPT):
                incoming_bytes = self._socket.recv(_MAX_RESPONSE)
                if incoming_bytes[-1] == _END_MESSAGE:
                    incoming_string = incoming_bytes[0:-1].decode('ascii')
                    result += incoming_string
                    if len(result) > 0:
                        return result[1:]  # ignore the first character of received message ('\x00')
                    else:
                        return result  # This will be ''
                else:
                    incoming_string = incoming_bytes.decode('ascii')
                    result += incoming_string

            _logger.warning(f'No end string received from the device\nReceived : {result}')
            return None

    def _query(self, message: str) -> typing.Optional[str]:
        """Composite method of write and read."""
        self._write(message)
        return self._read()

    """Device control functions"""

    def set_gain(self, channel: int, gain: int) -> None:
        """Set the gain of the specific channel amplifier.

        :param channel: amplifier channel number (0 ~ 31)
        :param gain: amplifier gain (0 ~ 23), approximately dB
        """
        assert isinstance(channel, int) and 0 <= channel < _NUM_CHANNELS
        assert isinstance(gain, int) and 0 <= gain <= 23
        self._query(f'SetGain {channel} {gain}')

    def set_rf(self, channel: int, source: str) -> None:
        """Set the input of the amplifier.

        Setting the input automatically turns on the rf switch.

        :param channel: amplifier channel number (0 ~ 31)
        :param source: 'i' for internal dds, 'e' for external source, '0' for rf switch off
        """
        assert isinstance(channel, int) and 0 <= channel < _NUM_CHANNELS
        assert isinstance(source, str) and source in {'0', 'i', 'e'}
        self._query(f'SetRF {channel} {source}')

    def set_frequency(self, channel: int, frequency: float) -> None:
        """Set internal dds frequency.

        :param channel: amplifier channel number (0 ~ 31)
        :param frequency: frequency in Hz (rounded in Hz order)
        """
        assert isinstance(channel, int) and 0 <= channel < _NUM_CHANNELS
        assert isinstance(frequency, float)
        self._query(f'SetFreq {channel} {round(frequency)}')

    def set_amplitude(self, channel: int, amplitude: float) -> None:
        """Set internal dds amplitude.

        :param channel: amplifier channel number (0 ~ 31)
        :param amplitude: dds amplitude (0 ~ 16383)
        """
        assert isinstance(channel, int) and 0 <= channel < _NUM_CHANNELS
        assert isinstance(amplitude, float) and 0 <= amplitude <= 16383
        self._query(f'SetAmp {channel} {round(amplitude)}')

    def get_status(self) -> _S_T:
        """Return dictionary of the RF driver status.

        This method reports RF switch status, source, amplifier gain, and internal dds frequency/amplitude/phase.
        """
        if self._simulation:
            return _DUMMY_DICT

        result = self._query('status')

        if result is None:
            # no end string found from the return message.
            return {}

        data_dict: _S_T = {}

        for data_str in result.split(_CRLF)[1:-1]:
            data = data_str.split(', ')
            channel = int(data[0])

            assert channel not in data_dict
            data_dict[channel] = {
                'RF switch': True if data[2] == '1' else False,
                'RF source': data[3],  # 'e' if external else 'i'
                'gain': int(data[5]),
                'internal dds': {
                    'frequency': int(data[6]),
                    'phase': int(data[7]),
                    'amplitude': int(data[8])
                }
            }

        return data_dict
