"""
Driver for the Quantel ablation laser driven by a Quantel Falcon Controller.
"""

import typing
import time
import logging

import serial

_logger = logging.getLogger(__name__)


class QuantelFalconDriver:
    _SIM_VALUES = {
        'DCURR': '0',
        'DFREQ': '0',
        'DTRIG': '0',
        'BURST': '0',
        'MAXPW': '0'
    }

    _ENCODING = 'utf-8'

    max_current = 54  # Maximum power setting for the laser as determined by the red chamber system

    def __init__(self, port_name: str, *, simulation: bool = False):
        """
            :param port_name: serial port name, e.g. 'COM##'
            :param simulation: bool, enables simulation, disables serial communication
        """
        assert isinstance(port_name, str)
        assert isinstance(simulation, bool)

        self._sim_read_buffer = ''
        self._simulation = simulation

        if not self._simulation:
            # establish serial connection
            try:
                self._ser = serial.Serial(
                    port=port_name,
                    baudrate=115200,
                    parity=serial.PARITY_NONE,
                    stopbits=serial.STOPBITS_ONE,
                    bytesize=serial.EIGHTBITS
                )
            except serial.SerialException:
                raise serial.SerialException(f"Failed to connect {port_name}")
            self._ser.timeout = 1.0
            self._ser.write_timeout = 1.0

            _logger.debug('Connected via %s', port_name)

            if self._ser.isOpen():
                _logger.debug(f'Serial open on port {self._ser.name}')
            else:
                _logger.debug("Serial not open")

    # noinspection PyMethodMayBeStatic
    def ping(self) -> bool:
        """Always returns true (indication that the driver is running)."""
        return True

    def close(self) -> None:
        self.stop()
        if not self._simulation:
            self._ser.close()

    def _write(self, command: str, value: str = '') -> None:
        """
             write message to the device via serial communication
             :param command: command to be written
             :param value: value to be set, '?' to "query" value
        """
        assert isinstance(command, str), f'command {command} is not a string'
        assert isinstance(value, str), f'value {value} is not a string'

        message = f'${command} {value} \r\n'

        _logger.debug(f'Write:{message}'.strip())

        if not self._simulation:
            if self._ser.in_waiting:
                self._flush()
            self._ser.write(message.encode(self._ENCODING))
            time.sleep(.1)
        else:  # self.simulation
            if not value == '?':
                self._SIM_VALUES[command] = value
            self._sim_read_buffer = f'{self._sim_read_buffer}${command} {self._SIM_VALUES[command]}\r\n'

        time.sleep(.1)  # Round-trip comm time

        return

    def _read(self) -> typing.List[str]:
        """
        Read serial buffer
        :return: array of strings minus '$", "\n", '\r' split by the ' ' character
        """
        if not self._simulation:
            message = self._ser.read(self._ser.in_waiting).decode(self._ENCODING)
        else:  # self.simulation
            message = self._sim_read_buffer
            self._sim_read_buffer = ''

        _logger.debug(f'Read:{message}'.strip())
        # Remove $, \n, \r characters and split by ' '
        message_list = message[1:].strip().split(' ')
        return message_list

    def _flush(self) -> None:
        """ flush input buffer """
        if not self._simulation:
            self._ser.flush()
            message = self._ser.read(self._ser.in_waiting).decode(self._ENCODING)
        else:  # self.simulation
            message = self._sim_read_buffer
            self._sim_read_buffer = ''

        _logger.debug(f'Flushed Serial Buffer:{message}')

    def _query(self, command: str) -> str:
        """
        Query Parameter
        :param command: command to be queried
        :return: message or none if message is invalid or no value is returned
        """
        assert isinstance(command, str), f'command {command} is not a string'

        _logger.debug(f'Querying: {command}')
        # write to the device
        self._write(command, '?')
        message = self._read()

        if message[0] == command:
            assert len(message) == 2, 'Unexpected message length'
            return message[1]
        else:
            self.stop()
            raise RuntimeError(f'Unexpected response from query: {command}, received {message}')

    def _set_value(self, command: str, value: typing.Any) -> None:
        """
         write message to the device via serial communication and read back to ensure expected value was returned
         :param command: command to be written
         :param value: Value to be set
        """
        assert isinstance(command, str), f'command {command} is not a string'

        value = str(value)
        _logger.debug(f"Setting Parameter:{command} Value:{value}")

        self._write(command, value)

        # Check the return message for errors
        message = self._read()

        if message[0] != command:
            self.stop()
            raise RuntimeError(f'Unexpected response from query: {command}, received {message}')

        assert len(message) == 2, 'Unexpected message length'
        if message[1] != value:
            self.stop()
            raise RuntimeError(f'Value not set for command: {command} value:{value}, received {message}')

    def _send_command(self, command: str) -> None:
        """
        write message to the device via serial communication and read back to ensure expected value was returned
        :param command: command to be written
        :param values: Array of values to be set. Currently only support lists of length 0 and 1
        """
        assert isinstance(command, str), f'command {command} is not a string'
        _logger.debug(f"Setting Parameter:{command}")

        self._write(command, '')

        message = self._read()

        assert len(message) == 1, 'Unexpected message length'

        if message[0] != command:
            self.stop()
            raise RuntimeError(f'Unexpected response from query: {command}, received {message}')

    def set_max_current(self, max_current: int) -> None:
        """ :param max_current: Maximum current setting for the laser"""
        assert isinstance(max_current, int), f'max_current {max_current} is not an integer'
        assert 1 <= max_current, f'max_current {max_current} is less than 1'
        self.max_current = max_current

    def set_current(self, current: int) -> None:
        """ :param current: diode current [1,self.max_power]"""
        assert isinstance(current, int), f'current {current} is not an integer'
        assert 1 <= current <= self.max_current, f'current {current} is out of range [1, {self.max_current}]'
        self._set_value('DCURR', current)

    def set_pulse_freq(self, frequency: int) -> None:
        """ :param frequency: trigger frequency (1-40)"""
        assert isinstance(frequency, int), f'frequency {frequency} is not an integer'
        assert 1 <= frequency <= 40, f'frequency {frequency} is out of range [1, 40]'
        self._set_value('DFREQ', frequency)

    def set_trigger_mode(self, mode: int) -> None:
        """ :param mode: 0 = Internal, 1 External """
        assert isinstance(mode, int), f'mode {mode} is not an integer'
        assert 0 <= mode <= 1, f'mode {mode} is out of range [0, 1]'
        self._set_value('DTRIG', mode)

    def set_burst_triggers(self, num_triggers: int) -> None:
        """
        :param num_triggers: 0 = Burst OFF - continuous mode, 1-4095 = Burst ON (with this number of laser pulses.)
        """
        assert isinstance(num_triggers, int), f'num_triggers {num_triggers} is not an integer'
        assert 0 <= num_triggers < 4096, f'num_triggers {num_triggers} is out of range [0, 4095]'
        self._set_value('BURST', num_triggers)

    def standby(self) -> None:
        self._send_command('STANDBY')

    def stop(self) -> None:
        self._send_command('STOP')

    def fire(self) -> None:
        # The $FIRE command would occasionally return $STANDBY. This added delay allows the controller
        # to set all internal parameters before the $FIRE command is sent
        time.sleep(.5)
        self._send_command('FIRE')

    def query_current(self) -> int:
        return int(self._query('DCURR'))

    def query_pulse_freq(self) -> int:
        return int(self._query('DFREQ'))

    def query_trigger_mode(self) -> int:
        return int(self._query('DTRIG'))

    def query_burst_triggers(self) -> int:
        return int(self._query('BURST'))

    def query_max_power(self) -> int:
        return int(self._query('MAXPW'))

    def set_all_parameters(self, current: int, pulse_frequency: int, trigger_mode: int, burst_triggers: int) -> None:
        """
        Sets all laser control parameters
        """
        self.set_current(current)
        self.set_pulse_freq(pulse_frequency)
        self.set_trigger_mode(trigger_mode)
        self.set_burst_triggers(burst_triggers)

    def query_all_parameters(self) -> typing.Tuple[int, int, int, int, int]:
        """
        Queries all laser parameters
        :return: tuple containing all parameters (current, Pulse Freq, Trigger Mode, Num Burst Triggers, Max Power)
        """
        current = self.query_current()
        pulse_freq = self.query_pulse_freq()
        trig_mode = self.query_trigger_mode()
        burst_trig = self.query_burst_triggers()
        max_power = self.query_max_power()

        return current, pulse_freq, trig_mode, burst_trig, max_power
