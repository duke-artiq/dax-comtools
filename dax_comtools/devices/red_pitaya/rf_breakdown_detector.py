import logging
import typing
import threading
import datetime

import numpy as np

from dax_comtools.devices.red_pitaya.driver import RedPitayaV1


class RfBreakdownDetector:
    """Driver for Red Pitaya RF Breakdown Detector."""
    termination_request = False
    _detection_thread = None

    def __init__(self,
                 addr: str,
                 reflection_channel: int = 1,
                 threshold_multiplier: float = 2.5,
                 simulation: bool = False,
                 **kwargs: typing.Any):
        """
        :param addr: The IP address of the Red Pitaya
        :param reflection_channel: The RedPitaya channel monitoring reflections
        :param threshold_multipler: trigger threshold = max amplitude * threshold_multiplier
        :param simulation: flag to run in simulation mode (dump all received messages to stdout)
        :param kwargs: Keyword arguments passed to the serial object
        """
        assert isinstance(addr, str)
        assert isinstance(reflection_channel, int)
        assert reflection_channel == 1 or reflection_channel == 2
        assert isinstance(threshold_multiplier, (float, int))
        assert isinstance(simulation, bool)
        logging.info('Initializing Breakdown Detector')

        self.simulation: bool = simulation
        self.reflection_channel: int = reflection_channel
        self.threshold_multiplier: float = threshold_multiplier

        self.rpt = RedPitayaV1(addr, simulation=simulation, **kwargs)

        self.clear_events()

        # don't need to open connection to device when simulating
        if simulation:
            return

        logging.info('Initialization Complete')

    # noinspection PyMethodMayBeStatic
    def ping(self):
        """Always returns true (indication that the driver is running)."""
        return True

    def _get_data(self, trigger_threshold: float = 0.0):
        '''
        Execute an acquisition and caputre data
        :param channel: the channel to capture data from
        :param trigger_threshold: the threshold for the trigger in volts
        '''
        assert isinstance(trigger_threshold, (float, int))

        logging.info(f'Capturing data with threshold {trigger_threshold}')

        self.rpt.acquisition.reset()
        self.rpt.acquisition.set_format(self.rpt.acquisition.Formats.ASCII)
        self.rpt.acquisition.set_acquisition_units(self.rpt.acquisition.Units.VOLTS)
        self.rpt.acquisition.set_trigger_level(trigger_threshold)
        self.rpt.acquisition.set_trigger_delay(0)
        self.rpt.acquisition.set_gain(self.reflection_channel, self.rpt.acquisition.Gains.HV)

        self.rpt.acquisition.start()
        if self.reflection_channel == 1:
            self.rpt.acquisition.set_trigger_mode(self.rpt.acquisition.TriggerModes.Ch1_positive_edge)
        else:
            self.rpt.acquisition.set_trigger_mode(self.rpt.acquisition.TriggerModes.Ch2_positive_edge)

        logging.info('Waiting for trigger')

        while 1:
            if self.rpt.acquisition.trigger_status == 'TD':
                break
            # Check for termination request
            if self.termination_request:
                return [0]

        logging.info('Loading buffer')
        while 1:
            if self.rpt.acquisition.buffer_filled:
                break
            # Check for termination request
            if self.termination_request:
                return [0]

        logging.info('Reading data')
        buff = self.rpt.acquisition.get_data(channel=self.reflection_channel)

        if buff == []:
            return [0]

        return buff

    def _breakdown_detection_process(self):
        # Update breakdown threshold, get data without triggering
        amplitude = np.max(self._get_data())
        breakdown_threshold = self.threshold_multiplier * amplitude
        logging.info(f'Captured amplitude: {amplitude}')
        logging.info(f'Setting threshold:  {breakdown_threshold}')

        while True:
            # Look for trigger event
            amplitude = np.max(self._get_data(trigger_threshold=breakdown_threshold))

            # Check for termination request
            if self.termination_request:
                return

            # Log breakdown event
            event_time = datetime.datetime.now()
            event_data = (str(event_time), event_time.timestamp(), amplitude, breakdown_threshold)
            logging.warning(f'Captured Breakdown Event: {event_data}')
            self._detection_events += [event_data]
            logging.info(self._detection_events)

    # Threading functions
    def start(self):
        logging.info('Starting detection')
        if self._detection_thread is not None:
            self.stop()

        # Re-create detection thread
        self._detection_thread = threading.Thread(target=self._breakdown_detection_process, daemon=True)
        self._detection_thread.start()

    def stop(self):
        logging.info('Terminating detection')
        if self._detection_thread.is_alive():
            self.termination_request = True
            self._detection_thread.join()
            self.termination_request = False
            logging.info(self._detection_events)

    def update(self):
        logging.info('Updating detection threshold')
        self.stop()
        self.start()

    def clear_events(self):
        logging.info('Clearing events list')
        self._detection_events = []

    def get_events(self):
        logging.info('Reading events list')
        return self._detection_events
