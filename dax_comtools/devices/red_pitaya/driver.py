import logging
import typing
import datetime
from enum import Enum

import numpy as np
import socket

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


class SCPI:
    """SCPI Class to communicate with the Red Pitaya"""
    DELIMITER = '\r\n'

    def __init__(self, addr: str, timeout=0.1, port=5000, simulation: bool = False, **kwargs: typing.Any):
        '''
        Initialize object and open IP connection.
        :param addr: the IP address of the Red Pitaya
        :param timeout: the timeout in seconds
        :param port: the port to connect to
        :param simulation: if `True`, will simulate the Red Pitaya conections
        :param kwargs: additional keyword arguments to pass to the scpi class
        '''
        assert isinstance(addr, str)
        assert isinstance(simulation, bool)
        assert isinstance(timeout, (float, int))
        assert isinstance(port, int)
        self._addr = addr
        self._simulation = simulation
        self._timeout = timeout
        self._port = port

        if not self._simulation:
            try:
                logging.info(f"Connecting to Red Pitaya at {self._addr}:{self._port}")
                self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

                logging.info(f"Setting timeout to {self._timeout}")
                if self._timeout is not None:
                    self._socket.settimeout(1.0)

                self._socket.connect((self._addr, self._port))

            except socket.error as e:
                logging.error('SCPI >> connect({!s:s}:{:d}) failed: {!s:s}'.format(self._addr, self._port, e))

    def close(self):
        """Close IP connection."""
        self.__del__()

    def __del__(self):
        if not self._simulation:
            if self._socket is not None:
                self._socket.close()
            self._socket = None

    # SCPI low-level commands
    def _rx_txt(self, chunksize=4096):
        """Receive text string and return it after removing the delimiter."""
        if not self._simulation:
            msg = ''
            try:
                while 1:
                    chunk = self._socket.recv(chunksize).decode('utf-8')  # Receive chunk size of 2^n preferably
                    msg += chunk
                    if (len(msg) and msg[-2:] == self.DELIMITER):
                        break
                return msg[:-2]
            except socket.timeout:
                return 'TIMEOUT!'
        else:
            return 'SIM!'

    def _tx_txt(self, msg):
        """Send text string ending and append delimiter."""
        if not self._simulation:
            return self._socket.sendall((msg + self.DELIMITER).encode('utf-8'))  # was send(().encode('utf-8'))

    def _txrx_txt(self, msg):
        """Send/receive text string."""
        self._tx_txt(msg)
        return self._rx_txt()

    # SCPI set/get functions
    def set_value(self, command: str, value, safe: bool = False):
        '''
        Set a value on the redpitaya
        :param command: the command to execute
        :param value: the value to set
        :param safe: if True, will flush the buffer after setting the value
        '''
        assert isinstance(command, str)
        assert isinstance(safe, bool)
        value_str = str(value)
        cmd = f'{command} {value_str}'
        if not self._simulation:
            self._tx_txt(cmd)
        logging.debug(f"-'{cmd}'")
        if safe:
            self.flush()

    def get_value(self, command: str, properties: str = ''):
        '''
        Get a value from the redpitaya
        :param command: the command to execute
        :param properties: additional properties to append to the command
        '''
        assert isinstance(command, str)
        assert isinstance(properties, str)
        cmd = command+'? ' + properties
        self._tx_txt(cmd)
        ret = self._rx_txt()
        if ret == 'ERR!':
            raise Exception(f'Error in command: {cmd}')
        logging.debug(f"-'{cmd}'|'{ret}'")
        return ret

    def send_cmd(self, command: str, safe: bool = False):
        '''
        Send a command to the Red Pitaya
        :param command: the command to execute
        :param safe: if True, will flush the buffer after setting the value
        '''
        assert isinstance(command, str)
        assert isinstance(safe, bool)
        cmd = command
        self._tx_txt(cmd)
        logging.debug(f"-'{cmd}'")
        if safe:
            self.flush()

    def flush(self):
        '''
        Flush the SCPI buffer
        '''
        ret = self._rx_txt()
        logging.debug(f"-f|'{ret}'")


class RPTFunctionBase:
    def __init__(self, scpi: SCPI, simulation: bool = False):
        assert isinstance(scpi, SCPI)
        assert isinstance(simulation, bool)
        self._simulation = simulation
        self._scpi = scpi


class RPTAcquisition(RPTFunctionBase):
    """A class containing Red Pitaya acquisition commands"""

    # Enumerations
    class Gains(Enum):
        HV = "HV"
        LV = "LV"

    class Couplings(Enum):
        DC = "DC"
        AC = "AC"

    class Units(Enum):
        RAW = "RAW"
        VOLTS = "VOLTS"

    class Formats(Enum):
        BIN = "BIN"
        ASCII = "ASCII"

    class States(Enum):
        ON = "ON"
        OFF = "OFF"

    class TriggerModes(Enum):
        Disabled = "DISABLED"
        Now = "NOW"
        Ch1_positive_edge = "CH1_PE"
        Ch1_negative_edge = "CH1_NE"
        Ch2_positive_edge = "CH2_PE"
        Ch2_negative_edge = "CH2_NE"
        Ext_positive_edge = "EXT_PE"
        Ext_negative_edge = "EXT_NE"
        Awg_positive_edge = "AWG_PE"
        Awg_negative_edge = "AWG_NE"

    # Constants
    base_sample_rate = 125e6

    # Acquisition Commands
    _ACQUISITION_DECIMATION_CMD = 'ACQ:DEC'
    _ACQUISITION_AVERAGE_CMD = 'ACQ:AVG'
    _ACQUISITION_GAIN_CMD = 'ACQ:SOUR{channel}:GAIN'
    _ACQUISITION_COUPLING_CMD = 'ACQ:SOUR{channel}:COUP'
    _ACQUISITION_UNITS_CMD = 'ACQ:DATA:UNITS'
    _ACQUISITION_FORMAT_CMD = 'ACQ:DATA:FORMAT'
    _ACQUISITION_BUFFER_SIZE_CMD = 'ACQ:BUF:SIZE'
    _ACQUISITION_RESET_CMD = 'ACQ:RST'
    _ACQUISITION_START_CMD = 'ACQ:START'
    _ACQUISITION_DATA_CMD = 'ACQ:SOUR{channel}:DATA?'
    _ACQUISITION_TRIGGER_CMD = 'ACQ:TRIG'
    _ACQUISITION_TRIGGER_STATUS_CMD = 'ACQ:TRIG:STAT'
    _ACQUISITION_FILLED_CMD = 'ACQ:TRIG:FILL'
    _ACQUISITION_TRIGGER_DELAY_CMD = 'ACQ:TRIG:DLY'
    _ACQUISITION_TRIGGER_LEVEL_CMD = 'ACQ:TRIG:LEV'

    def set_decimation(self, decimation_exp: int = 1):
        '''
        Set the decimation exponent
        :param decimation_exp: the decimation exponent
        '''
        assert isinstance(decimation_exp, int)
        assert decimation_exp >= 0 and decimation_exp <= 16
        self._scpi.set_value(self._ACQUISITION_DECIMATION_CMD, 2**decimation_exp)

    def set_averaging(self, averaging_state: States):
        '''
        Set the averaging state
        :param averaging_state: average the skipped data if decimating, of type Averaging
        '''
        assert averaging_state in self.States
        self._scpi.set_value(self._ACQUISITION_AVERAGE_CMD, averaging_state.value)

    def set_gain(self, channel: int, gain: Gains):
        '''
        Set the gain
        :param channel: the channel to set the gain on
        :param gain: the gain to set, of type Gains
        '''
        assert isinstance(channel, int)
        assert channel == 1 or channel == 2
        assert gain in self.Gains
        self._scpi.set_value(self._ACQUISITION_GAIN_CMD.format(channel=channel), gain.value)

    def set_coupling(self, channel: int, coupling: Couplings):
        '''
        Set the coupling
        :param channel: the channel to set the coupling on
        :param coupling: the coupling to set, of type Couplings
        '''
        assert isinstance(channel, int)
        assert channel == 1 or channel == 2
        assert coupling in self.Couplings
        self._scpi.set_value(self._ACQUISITION_COUPLING_CMD.format(channel=channel), coupling.value)

    def set_acquisition_units(self, units: Units):
        '''
        Set the units
        :param units: the units to set, of type Units
        '''
        assert units in self.Units
        self._scpi.set_value(self._ACQUISITION_UNITS_CMD, units.value)

    def set_format(self, format: Formats):
        '''
        Set the format
        :param format: the format to set, of type Formats
        '''
        assert format in self.Formats
        self._scpi.set_value(self._ACQUISITION_FORMAT_CMD, format.value)

    def set_trigger_mode(self, trigger: TriggerModes):
        '''
        Set the trigger mode
        :param trigger: the trigger to set, of type Triggering
        '''
        assert trigger in self.TriggerModes
        self._scpi.set_value(self._ACQUISITION_TRIGGER_CMD, trigger.value)

    def set_trigger_delay(self, delay: float):
        '''
        Set the trigger delay
        :param delay: the trigger delay to set in seconds
        '''
        assert isinstance(delay, (float, int))
        self._scpi.set_value(self._ACQUISITION_TRIGGER_DELAY_CMD, str(delay))

    def set_trigger_level(self, level: float):
        '''
        Set the trigger level
        :param level: the trigger level to set in Volts
        '''
        assert isinstance(level, (float, int))
        self._scpi.set_value(self._ACQUISITION_TRIGGER_LEVEL_CMD, str(level))

    @property
    def decimation(self):
        '''
        Get the acquisition decimation
        '''
        return int(self._scpi.get_value(self._ACQUISITION_DECIMATION_CMD))

    @property
    def decimation_exp(self):
        '''
        Get the acquisition decimation exponent
        '''
        return np.log2(self.decimation)

    @property
    def averaging_state(self):
        '''
        Get the acquisition averaging state
        '''
        return self._scpi.get_value(self._ACQUISITION_AVERAGE_CMD)

    @property
    def gain(self):
        '''
        Get the acquisition gain
        '''
        ch1_gain = self._scpi.get_value(self._ACQUISITION_GAIN_CMD.format(channel=1))
        ch2_gain = self._scpi.get_value(self._ACQUISITION_GAIN_CMD.format(channel=2))
        return ch1_gain, ch2_gain

    def coupling(self, channel: int):
        '''
        Get the acquisition coupling
        :param channel: the channel to get the coupling from
        '''
        assert isinstance(channel, int)
        assert channel == 1 or channel == 2
        return self._scpi.get_value(self._ACQUISITION_COUPLING_CMD.format(channel=channel))

    @property
    def units(self):
        '''
        Get the acquisition units
        '''
        return self._scpi.get_value(self._ACQUISITION_UNITS_CMD)

    @property
    def format(self):
        '''
        Get the acquisition format
        '''
        return self._scpi.get_value(self._ACQUISITION_FORMAT_CMD)

    @property
    def buffer_size(self):
        '''
        Get the acquisition buffer size
        '''
        return int(self._scpi.get_value(self._ACQUISITION_BUFFER_SIZE_CMD))

    @property
    def trigger_mode(self):
        '''
        Get the acquisition trigger mode
        '''
        return self._scpi.get_value(self._ACQUISITION_TRIGGER_CMD)

    @property
    def trigger_delay(self):
        '''
        Get the acquisition trigger delay
        '''
        return float(self._scpi.get_value(self._ACQUISITION_TRIGGER_DELAY_CMD))

    @property
    def trigger_level(self):
        '''
        Get the acquisition trigger level
        '''
        return float(self._scpi.get_value(self._ACQUISITION_TRIGGER_LEVEL_CMD))

    @property
    def trigger_status(self):
        '''
        Get the trigger status
        '''
        return self._scpi.get_value(self._ACQUISITION_TRIGGER_STATUS_CMD)

    @property
    def buffer_filled(self):
        '''
        Get the acquisition buffer filled state
        '''
        filled_state = self._scpi.get_value(self._ACQUISITION_FILLED_CMD)
        if filled_state == '1':
            return True
        else:
            return False

    @property
    def sample_rate(self):
        '''
        Get the acquisition sample rate
        '''
        return self.base_sample_rate / 2**self.decimation_exp

    def reset(self):
        '''
        Reset the acquisition
        '''
        self._scpi.send_cmd(self._ACQUISITION_RESET_CMD)

    def start(self):
        '''
        Start the acquisition
        '''
        self._scpi.send_cmd(self._ACQUISITION_START_CMD)

    # Additional functions
    def get_data(self, channel: int):
        '''
        Get the fast-acquisition data from a specific channel
        :param channel: the channel to get the data from
        '''
        assert isinstance(channel, int)
        assert channel == 1 or channel == 2
        # Use direct SCPI commands to get the data
        buff_string = '0'
        if not self._simulation:
            self._scpi._tx_txt(self._ACQUISITION_DATA_CMD.format(channel=channel))
            buff_string = self._scpi._rx_txt()
        buff_array = buff_string.strip('{}\n\r').replace("  ", "").split(',')
        buff = list(map(float, buff_array))
        return buff

    def wait_for_trigger(self, timeout: float = 0.0):
        '''
        Wait for the acquisition to trigger. Returns `True` if triggered, `False` if timed out.
        :param timeout: the timeout in seconds
        '''
        assert isinstance(timeout, float)
        assert timeout >= 0.0
        start_time = datetime.datetime.now()
        while True:
            if self.trigger_status == 'TD':
                return True
            if (datetime.datetime.now() - start_time).total_seconds() > timeout:
                return False

    def wait_for_buffer_load(self, timeout: float = 0.0):
        '''
        Wait for the acquisition buffer to load. Returns `True` if loaded, `False` if timed out.
        :param timeout: the timeout in seconds
        '''
        assert isinstance(timeout, float)
        assert timeout >= 0.0
        start_time = datetime.datetime.now()
        while True:
            if self.buffer_filled:
                return True
            if (datetime.datetime.now() - start_time).total_seconds() > timeout:
                return False

    def simple_acquisition(self, channel: int, gain: Gains = Gains.HV):
        '''
        Perform a simple acquisition on a channel
        :param channel: the channel to acquire from
        :param gain: the gain to set
        '''
        assert isinstance(channel, int)
        assert channel == 1 or channel == 2
        self.reset()
        # Set acquisition Parameters
        self.set_format(self.Formats.ASCII)
        self.set_acquisition_units(self.Units.VOLTS)
        self.set_trigger_delay(0)
        self.set_gain(channel, gain=gain)

        # Acquire and Trigger
        self.start()
        self.set_trigger_mode(self.TriggerModes.Now)

        # Wait for Data
        self.wait_for_trigger()
        self.wait_for_buffer_load

        buff = self.get_data(channel=channel)
        return buff


class RPTGPIO(RPTFunctionBase):
    """A class containing Red Pitaya GPIO commands"""
    # Enumerations
    class GPIO_DIRECTIONS(Enum):
        In = "IN"
        Out = "OUT"

    class GPIO_POLARITIES(Enum):
        Pos = "P"
        Neg = "N"

    class PIN_TYPE(Enum):
        GPIO = "GPIO"
        LED = "LED"

    # GPIO Commands
    _GPIO_RESET_CMD = "GPIO:RST"
    _GPIO_DIRECTION_CMD = "GPIO:DIR"
    _GPIO_STATE_CMD = "GPIO:PIN"
    _GPIO_STRING = "GPIO{pin_number}:{polarity}"
    _LED_STRING = "LED{pin_number}"

    def set_gpio_direction(self, pin_number: int, polarity: GPIO_POLARITIES, direction: GPIO_DIRECTIONS):
        '''
        Set the GPIO direction
        :param pin_number: the pin number to set the direction on [0, 7]
        :param polarity: the polarity of the pin, of type GPIO_POLARITIES
        :param direction: the direction to set, of type GPIO_DIRECTIONS
        '''
        assert isinstance(pin_number, int)
        assert isinstance(polarity, self.GPIO_POLARITIES)
        assert isinstance(direction, self.GPIO_DIRECTIONS)
        assert pin_number >= 0 and pin_number <= 7
        self._scpi.set_value(self._GPIO_DIRECTION_CMD,
                             f'{direction.value},' +
                             f'{self._GPIO_STRING.format(pin_number=pin_number, polarity=polarity.value)}')

    def set_gpio_state(self, pin_number: int, polarity: GPIO_POLARITIES, state: bool):
        '''
        Set the GPIO state
        :param pin_number: the pin number to set the state on [0, 7]
        :param polarity: the polarity of the pin, of type GPIO_POLARITIES
        :param state: the state to set, of type bool
        '''
        assert isinstance(pin_number, int)
        assert isinstance(polarity, self.GPIO_POLARITIES)
        assert isinstance(state, bool)
        assert pin_number >= 0 and pin_number <= 7
        self._scpi.set_value(self._GPIO_STATE_CMD,
                             f'{self._GPIO_STRING.format(pin_number=pin_number, polarity=polarity.value)}' +
                             f',{int(state)}')

    def set_led_state(self, led_lumber: int, state: bool):
        '''
        Set the LED state
        :param led_number: the led number to set the state on [0, 7]
        :param state: the state to set, of type bool
        '''
        assert isinstance(led_lumber, int)
        assert isinstance(state, bool)
        assert led_lumber >= 0 and led_lumber <= 7
        self._scpi.set_value(self._GPIO_STATE_CMD, f'{self._LED_STRING.format(pin_number=led_lumber)},{int(state)}')

    def reset(self):
        '''
        Reset the GPIO
        '''
        self._scpi.send_cmd(self._GPIO_RESET_CMD)

    def direction(self, pin_number: int, polarity: GPIO_POLARITIES):
        '''
        Get the GPIO direction
        :param pin_number: the pin number to get the direction from [0, 7]
        :param polarity: the polarity of the pin, of type GPIO_POLARITIES
        '''
        assert isinstance(pin_number, int)
        assert isinstance(polarity, self.GPIO_POLARITIES)
        assert pin_number >= 0 and pin_number <= 7
        return self._scpi.get_value(self._GPIO_DIRECTION_CMD,
                                    self._GPIO_STRING.format(pin_number=pin_number, polarity=polarity.value))

    def state(self, pin_number: int, polarity: GPIO_POLARITIES):
        '''
        Get the GPIO state
        :param pin_number: the pin number to get the state from [0, 7]
        :param polarity: the polarity of the pin, of type GPIO_POLARITIES
        '''
        assert isinstance(pin_number, int)
        assert isinstance(polarity, self.GPIO_POLARITIES)
        assert pin_number >= 0 and pin_number <= 7
        return self._scpi.get_value(self._GPIO_STATE_CMD,
                                    self._GPIO_STRING.format(pin_number=pin_number, polarity=polarity.value))

    def led_state(self, led_number: int):
        '''
        Get the LED state
        :param led_number: the led number to get the state from [0, 7]
        '''
        assert isinstance(led_number, int)
        assert led_number >= 0 and led_number <= 7
        return self._scpi.get_value(self._GPIO_STATE_CMD, self._LED_STRING.format(pin_number=led_number))


class RPTAnalogIO(RPTFunctionBase):
    """Red Pitaya Analog IO commands"""
    # Analog IO Commands
    _ANALOG_IN_STRING = "AIN{channel}"
    _ANALOG_OUT_STRING = "AOUT{channel}"
    _ANALOG_RESET_CMD = "ANALOG:RST"
    _ANALOG_READ_WRITE_CMD = "ANALOG:PIN"

    def set_analog_out(self, channel: int, value: float):
        '''
        Set the analog output
        :param channel: the channel to set the output on [0, 3]
        :param value: the value to set the output to in volts
        '''
        assert isinstance(channel, int)
        assert isinstance(value, (float, int))
        assert 0 <= channel <= 3
        assert 0 <= value <= 1.8
        self._scpi.set_value(self._ANALOG_READ_WRITE_CMD,
                             f'{self._ANALOG_OUT_STRING.format(channel=channel)},{value}')

    def analog_in(self, channel: int):
        '''
        Get the analog input
        :param channel: the channel to get the input from [0, 3]
        :return: the analog output in Volts
        '''
        assert isinstance(channel, int)
        assert 0 <= channel <= 3
        return float(self._scpi.get_value(self._ANALOG_READ_WRITE_CMD,
                                          self._ANALOG_IN_STRING.format(channel=channel)))

    def analog_out(self, channel: int):
        '''
        Get the analog output
        :param channel: the channel to get the output from [0, 3]
        :return: the analog output in Volts
        '''
        assert isinstance(channel, int)
        assert 0 <= channel <= 3
        return float(self._scpi.get_value(self._ANALOG_READ_WRITE_CMD,
                                          self._ANALOG_OUT_STRING.format(channel=channel)))

    def reset(self):
        '''
        Reset the analog IO
        '''
        self._scpi.send_cmd(self._ANALOG_RESET_CMD)


class RPTGenerator(RPTFunctionBase):
    # Enumerations
    class States(Enum):
        ON = "ON"
        OFF = "OFF"

    class TriggerModes(Enum):
        Ext_positive_edge = "EXT_PE"
        Ext_negative_edge = "EXT_NE"
        Gated = "GATED"
        Internal = "INT"

    class Waveforms(Enum):
        Sine = "SINE"
        Square = "SQUARE"
        Triangle = "TRIANGLE"
        Saw_Up = "SAWU"
        Saw_Down = "SAWD"
        PWM = "PWM"
        Arbitrary = "ARBITRARY"
        DC = "DC"
        DC_Negative = "DC_NEG"

    class Modes(Enum):
        Burst = "BURST"
        Continuous = "CONTINUOUS"

    _GENERATOR_RESET_CMD = "GEN:RST"
    _GENERATOR_ENABLE_CHANNEL_CMD = "OUTPUT{channel}:STATE"
    _GENERATOR_ENABLE_ALL_CMD = "OUTPUT:STATE"
    _GENERATOR_TRIGGER_MODE_CHANNEL_CMD = "SOUR{channel}:TRig:SOUR"
    _GENERATOR_TRIGGER_MODE_ALL_CMD = "SOUR:TRig:SOUR"
    _GENERATOR_TRIGGER_CHANNEL_CMD = "SOUR{channel}:TRig:INT"
    _GENERATOR_TRIGGER_ALL_CMD = "SOUR:TRig:INT"
    _GENERATOR_TRIGGER_DEBOUNCE_CMD = "SOUR:TRig:EXT:DEBouncerUS"
    _GENERATOR_FUNCTION_CMD = "SOUR{channel}:FUNC"
    _GENERATOR_FREQUENCY_CMD = "SOUR{channel}:FREQ:FIX"
    _GENERATOR_AMPLITUDE_CMD = "SOUR{channel}:VOLT"
    _GENERATOR_OFFSET_CMD = "SOUR{channel}:VOLT:OFFS"
    _GENERATOR_PHASE_CMD = "SOUR{channel}:PHAS"
    _GENERATOR_PHASE_ALIGN_CMD = "PHAS:ALIGN"
    _GENERATOR_DUTY_CYCLE_CMD = "SOUR{channel}:DCYC"
    _GENERATOR_AWG_DATA_CMD = "SOUR{channel}:TRAC:DATA:DATA"
    _GENERATOR_BURST_MODE_CMD = "SOUR{channel}:BURS:STAT"
    _GENERATOR_CYCLES_PER_BURST_CMD = "SOUR{channel}:BURS:NCYC"
    _GENERATOR_BURST_REPETITIONS_CMD = "SOUR{channel}:BURS:NOR"
    _GENERATOR_BURST_PERIOD_CMD = "SOUR{channel}:BURS:INT:PER"
    _GENERATOR_BURST_LAST_VALUE_CMD = "SOUR{channel}:BURS:LASTValue"
    _GENERATOR_BURST_INIT_VALUE_CMD = "SOUR{channel}:INITValue"

    def enable_channel(self, channel: int, state: States = States.ON):
        '''
        Set the generator state
        :param channel: the channel to set the state on
        :param state: the state to set, of type States
        '''
        assert isinstance(channel, int)
        assert channel == 1 or channel == 2
        if state is True:
            state = self.States.ON
        elif state is False:
            state = self.States.OFF
        assert state in self.States

        self._scpi.set_value(self._GENERATOR_ENABLE_CHANNEL_CMD.format(channel=channel), state.value)

    def enable(self, state: States = States.ON):
        '''
        Set the generator state
        :param state: the state to set, of type States
        '''
        if state is True:
            state = self.States.ON
        elif state is False:
            state = self.States.OFF
        assert state in self.States or state in [True, False]
        self._scpi.set_value(self._GENERATOR_ENABLE_ALL_CMD, state.value)

    def set_trigger_mode(self, channel: int, mode: TriggerModes):
        '''
        Set the generator trigger mode
        :param channel: the channel to set the mode on
        :param mode: the mode to set, of type OutputTriggerModes
        '''
        assert isinstance(channel, int)
        assert channel == 1 or channel == 2
        assert mode in self.TriggerModes
        self._scpi.set_value(self._GENERATOR_TRIGGER_MODE_CHANNEL_CMD.format(channel=channel), mode.value)

    def set_trigger_debounce(self, debounce: float):
        '''
        Set the generator trigger debounce
        :param debounce: the debounce to set in seconds
        '''
        assert isinstance(debounce, (float, int))
        self._scpi.set_value(self._GENERATOR_TRIGGER_DEBOUNCE_CMD, debounce)

    def set_waveform(self, channel: int, waveform: Waveforms):
        '''
        Set the generator waveform
        :param channel: the channel to set the waveform on
        :param waveform: the waveform to set, of type OutputWaveforms
        '''
        assert isinstance(channel, int)
        assert channel == 1 or channel == 2
        assert waveform in self.Waveforms
        self._scpi.set_value(self._GENERATOR_FUNCTION_CMD.format(channel=channel), waveform.value)

    def set_frequency(self, channel: int, frequency: float):
        '''
        Set the generator frequency
        :param channel: the channel to set the frequency on
        :param frequency: the frequency to set in Hz [0, 62.5e6]
        '''
        assert isinstance(channel, int)
        assert channel == 1 or channel == 2
        assert isinstance(frequency, int)
        assert 0 <= frequency <= 62.5e6
        self._scpi.set_value(self._GENERATOR_FREQUENCY_CMD.format(channel=channel), frequency)

    def set_amplitude(self, channel: int, amplitude: float):
        '''
        Set the generator amplitude
        :param channel: the channel to set the amplitude on
        :param amplitude: the amplitude to set in Volts [-1, 1]
        '''
        assert isinstance(channel, int)
        assert channel == 1 or channel == 2
        assert isinstance(amplitude, (float, int))
        assert -1 <= amplitude + self.offset(channel) <= 1
        self._scpi.set_value(self._GENERATOR_AMPLITUDE_CMD.format(channel=channel), amplitude, safe=True)
        # Safe because this function returns an error for some reason

    def set_offset(self, channel: int, offset: float):
        '''
        Set the generator offset
        :param channel: the channel to set the offset on
        :param offset: the offset to set in Volts [-1, 1]
        '''
        assert isinstance(channel, int)
        assert channel == 1 or channel == 2
        assert isinstance(offset, (float, int))
        assert -1 <= offset + self.amplitude(channel) <= 1
        self._scpi.set_value(self._GENERATOR_OFFSET_CMD.format(channel=channel), offset, safe=True)
        # Safe because this function returns an error for some reason

    def set_phase(self, channel: int, phase: float):
        '''
        Set the generator phase
        :param channel: the channel to set the phase on
        :param phase: the phase to set in degrees [-360, 360]
        '''
        assert isinstance(channel, int)
        assert channel == 1 or channel == 2
        assert isinstance(phase, (float, int))
        assert -360 <= phase <= 360
        self._scpi.set_value(self._GENERATOR_PHASE_CMD.format(channel=channel), phase)

    def set_duty_cycle(self, channel: int, duty_cycle: float):
        '''
        Set the generator duty cycle
        :param channel: the channel to set the duty cycle on
        :param duty_cycle: the duty cycle to set [0, 1]
        '''
        assert isinstance(channel, int)
        assert channel == 1 or channel == 2
        assert isinstance(duty_cycle, (float, int))
        assert 0 <= duty_cycle <= 1
        self._scpi.set_value(self._GENERATOR_DUTY_CYCLE_CMD.format(channel=channel), duty_cycle)

    def set_awg_data(self, channel: int, data: list):
        '''
        Set the generator AWG data
        :param channel: the channel to set the AWG data on
        :param data: the data to set
        '''
        assert isinstance(channel, int)
        assert channel == 1 or channel == 2
        assert all(isinstance(x, (float, int)) for x in data)
        assert len(data) <= 16384
        # convert data to a csv string
        data_str = ','.join(map(str, data))
        self._scpi.set_value(self._GENERATOR_AWG_DATA_CMD.format(channel=channel), data_str)

    def set_mode(self, channel: int, mode: Modes):
        '''
        Set the generator mode
        :param channel: the channel to set the mode on
        :param mode: the mode to set, of type Modes
        '''
        assert isinstance(channel, int)
        assert channel == 1 or channel == 2
        assert mode in self.Modes
        self._scpi.set_value(self._GENERATOR_BURST_MODE_CMD.format(channel=channel), mode.value)

    def set_cycles_per_burst(self, channel: int, n_cycles: int):
        '''
        Set the number of cycles per burst
        :param channel: the channel to set the number of cycles on
        :param n_cycles: the number of cycles to set
        '''
        assert isinstance(channel, int)
        assert channel == 1 or channel == 2
        assert isinstance(n_cycles, int)
        assert 0 <= n_cycles
        self._scpi.set_value(self._GENERATOR_CYCLES_PER_BURST_CMD.format(channel=channel), n_cycles)

    def set_burst_repetitions(self, channel: int, n_reps: int):
        '''
        Set the number of repeated bursts
        :param channel: the channel to set the number of repetitions on
        :param n_reps: the number of repetitions to set [0, 65536]
        '''
        assert isinstance(channel, int)
        assert channel == 1 or channel == 2
        assert isinstance(n_reps, int)
        assert 0 <= n_reps <= 65536
        self._scpi.set_value(self._GENERATOR_BURST_REPETITIONS_CMD.format(channel=channel), n_reps)

    def set_burst_period(self, channel: int, period: int):
        '''
        Set the burst period
        :param channel: the channel to set the burst period on
        :param period: the period to set in micro seconds
        '''
        assert isinstance(channel, int)
        assert channel == 1 or channel == 2
        assert isinstance(period, int)
        assert 1 <= period <= 1000000
        self._scpi.set_value(self._GENERATOR_BURST_PERIOD_CMD.format(channel=channel), period)

    def set_burst_final_voltage(self, channel: int, voltage: float):
        '''
        Set the burst final voltage
        :param channel: the channel to set the final voltage on
        :param voltage: the voltage to set in Volts [-1, 1]
        '''
        assert isinstance(channel, int)
        assert channel == 1 or channel == 2
        assert isinstance(voltage, (float, int))
        assert -1 <= voltage <= 1
        self._scpi.set_value(self._GENERATOR_BURST_LAST_VALUE_CMD.format(channel=channel), voltage)

    def set_burst_initial_voltage(self, channel: int, voltage: float):
        '''
        Set the burst initial voltage
        :param channel: the channel to set the initial voltage on
        :param voltage: the voltage to set in Volts [-1, 1]
        '''
        assert isinstance(channel, int)
        assert channel == 1 or channel == 2
        assert isinstance(voltage, (float, int))
        assert -1 <= voltage <= 1
        self._scpi.set_value(self._GENERATOR_BURST_INIT_VALUE_CMD.format(channel=channel), voltage)

    def output_state(self, channel: int):
        '''
        Get the generator output state
        :param channel: the channel to get the state from
        '''
        assert isinstance(channel, int)
        assert channel == 1 or channel == 2
        return self._scpi.get_value(self._GENERATOR_ENABLE_CHANNEL_CMD.format(channel=channel))

    def trigger_mode(self, channel: int):
        '''
        Get the generator trigger mode
        :param channel: the channel to get the mode from
        '''
        assert isinstance(channel, int)
        assert channel == 1 or channel == 2
        return self._scpi.get_value(self._GENERATOR_TRIGGER_MODE_CHANNEL_CMD.format(channel=channel))

    def trigger_debounce(self, channel, int):
        '''
        Get the generator trigger debounce time
        :param channel: the channel to get the debounce from
        '''
        assert isinstance(channel, int)
        assert channel == 1 or channel == 2
        return int(self._scpi.get_value(self._GENERATOR_TRIGGER_DEBOUNCE_CMD))

    def waveform(self, channel: int):
        '''
        Get the generator waveform
        :param channel: the channel to get the waveform from
        '''
        assert isinstance(channel, int)
        assert channel == 1 or channel == 2
        return self._scpi.get_value(self._GENERATOR_FUNCTION_CMD.format(channel=channel))

    def frequency(self, channel: int):
        '''
        Get the generator frequency
        :param channel: the channel to get the frequency from
        '''
        assert isinstance(channel, int)
        assert channel == 1 or channel == 2
        return int(float(self._scpi.get_value(self._GENERATOR_FREQUENCY_CMD.format(channel=channel))))

    def amplitude(self, channel: int):
        '''
        Get the generator amplitude
        :param channel: the channel to get the amplitude from
        '''
        assert isinstance(channel, int)
        assert channel == 1 or channel == 2
        return float(self._scpi.get_value(self._GENERATOR_AMPLITUDE_CMD.format(channel=channel)))

    def offset(self, channel: int):
        '''
        Get the generator offset
        :param channel: the channel to get the offset from
        '''
        assert isinstance(channel, int)
        assert channel == 1 or channel == 2
        return float(self._scpi.get_value(self._GENERATOR_OFFSET_CMD.format(channel=channel)))

    def phase(self, channel: int):
        '''
        Get the generator phase
        :param channel: the channel to get the phase from
        '''
        assert isinstance(channel, int)
        assert channel == 1 or channel == 2
        return float(self._scpi.get_value(self._GENERATOR_PHASE_CMD.format(channel=channel)))

    def duty_cycle(self, channel: int):
        '''
        Get the generator duty cycle
        :param channel: the channel to get the duty cycle from
        '''
        assert isinstance(channel, int)
        assert channel == 1 or channel == 2
        return float(self._scpi.get_value(self._GENERATOR_DUTY_CYCLE_CMD.format(channel=channel)))

    def awg_data(self, channel: int):
        '''
        Get the generator AWG data
        :param channel: the channel to get the AWG data from
        '''
        assert isinstance(channel, int)
        assert channel == 1 or channel == 2
        return self._scpi.get_value(self._GENERATOR_AWG_DATA_CMD.format(channel=channel))

    # Commands

    def reset(self):
        '''
        Reset the generator
        '''
        self._scpi.send_cmd(self._GENERATOR_RESET_CMD)

    def trigger_channel(self, channel: int):
        '''
        Trigger the generator
        :param channel: the channel to trigger
        '''
        assert isinstance(channel, int)
        assert channel == 1 or channel == 2
        self._scpi.send_cmd(self._GENERATOR_TRIGGER_CHANNEL_CMD.format(channel=channel))

    def trigger(self):
        '''
        Trigger the generator
        '''
        self._scpi.send_cmd(self._GENERATOR_TRIGGER_ALL_CMD)

    def sync_phase(self):
        '''
        Sync the generator phase
        '''
        self._scpi.send_cmd(self._GENERATOR_PHASE_ALIGN_CMD)


class RedPitayaV1:
    """Driver for Red Pitaya v1 OS"""
    # Enumerations
    class LogModes(Enum):
        Off = "OFF"
        Console = "CONSOLE"
        Syslog = "SYSLOG"

    def __init__(self,
                 addr: str,
                 simulation: bool = False,
                 **kwargs: typing.Any):
        """
        :param addr: The IP address of the Red Pitaya
        :param simulation: flag to run in simulation mode (dump all received messages to stdout)
        :param kwargs: Keyword arguments passed to the serial object
        """
        assert isinstance(addr, str)
        assert isinstance(simulation, bool)
        logging.debug('Initializing Red Pitaya Driver')

        self.simulation: bool = simulation

        # Open Communications
        self._rp_comm = SCPI(addr=addr, simulation=simulation, **kwargs)

        # Red Pitaya Functions
        self.acquisition = RPTAcquisition(self._rp_comm)
        self.gpio = RPTGPIO(self._rp_comm)
        self.analog_io = RPTAnalogIO(self._rp_comm)
        self.generator = RPTGenerator(self._rp_comm)

        logging.debug('Initialization Complete')

    # Red Pitaya Commands
    _RP_LOG_MODE_CMD = "RP:LOGmode"
    _RP_VERSION_CMD = "SYST:VERS"

    # Parameter Sets
    def set_log_mode(self, mode: LogModes):
        '''
        Set the logging mode
        :param mode: the logging mode to set, of type LogModes
        '''
        assert mode in self.LogModes
        self._rp_comm.set_value(self._RP_LOG_MODE_CMD, mode.value)

    # Properties
    @property
    def log_mode(self):
        '''
        Get the logging mode
        '''
        return self._rp_comm.get_value(self._RP_LOG_MODE_CMD)

    @property
    def version(self):
        '''
        Get the Red Pitaya version
        '''
        return self._rp_comm.get_value(self._RP_VERSION_CMD)


class RedPitayaV2(RedPitayaV1):
    """Additional Commands for Red Pitaya OS v2.0"""

    _RP_SYSTEM_TIME_CMD = "SYSTem:TIME"
    _RP_SYSTEM_DATE_CMD = "SYSTem:DATE"
    _RP_BOARD_ID_CMD = "SYSTem:BRD:ID"
    _RP_BOARD_NAME_CMD = "SYSTem:BRD:Name"

    def set_system_time(self, time: datetime.time):
        '''
        Set the system time
        :param time: the time to set, of type datetime.time
        '''
        assert isinstance(time, datetime.time)
        self._rp_comm.set_value(self._RP_SYSTEM_TIME_CMD, time.strftime('%H:%M:%S'))

    def set_system_date(self, date: datetime.date):
        '''
        Set the system date
        :param date: the date to set, of type datetime.date
        '''
        assert isinstance(date, datetime.date)
        self._rp_comm.set_value(self._RP_SYSTEM_DATE_CMD, date.strftime('%Y-%m-%d'))

    @property
    def system_time(self):
        '''
        Get the system time
        :return: the system time as a datetime.time object
        '''
        time_str = self._rp_comm.get_value(self._RP_SYSTEM_TIME_CMD)
        time = datetime.datetime.strptime(time_str, '%H:%M:%S').time()
        return time

    @property
    def system_date(self):
        '''
        Get the system date
        :return: the system date as a datetime.date object
        '''
        date_str = self._rp_comm.get_value(self._RP_SYSTEM_DATE_CMD)
        date = datetime.datetime.strptime(date_str, '%Y-%m-%d').date()
        return date

    @property
    def board_id(self):
        '''
        Get the board id
        '''
        return self._rp_comm.get_value(self._RP_BOARD_ID_CMD)

    @property
    def board_name(self):
        '''
        Get the board name
        '''
        return self._rp_comm.get_value(self._RP_BOARD_NAME_CMD)

# Helper funciton to get the spectrum of time series data


def fft(samples: list, sample_rate: float):
    '''
    Get the spectrum of a data set
    :param samples: the samples to get the spectrum of
    :param sample_rate: the sample rate of the data
    '''
    assert isinstance(samples, list)
    assert isinstance(sample_rate, (float, int))

    # Calculate Power Spectrum from FFT
    fft = np.fft.rfft(samples)
    ps = 20*np.log10(np.abs(fft))

    # Calculate Frequencies based on sample rate
    n_samples = len(samples)
    freqs = np.fft.fftfreq(n_samples, d=1/sample_rate)

    """ np.fft returns the positive and negative spectrum
    To make things more fun, for an even dataset,
            the negative numbers hold more values
            Also, we want to discard the DC entry
    Since we have real data, the spectrum is symmetric
    Here we cast the arrays to just the positive half"""
    last_sample = int(np.ceil((n_samples+1)/2))
    ps = ps[1:last_sample]
    samples = samples[1:last_sample]
    freqs = freqs[1:last_sample]
    freqs = abs(freqs)

    return freqs, ps
