# ADS1x15 driver

The Adafruit CircuitPython ADS1x15 drivers are copied from
[the Adafruit GitHub page](https://github.com/adafruit/Adafruit_CircuitPython_ADS1x15) and modified to work with our
implementation of the CircuitPython API. See the attached licence file.
