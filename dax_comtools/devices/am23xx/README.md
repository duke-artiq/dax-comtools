# AM23xx driver

The Adafruit CircuitPython AM2320 driver (`adafruit_am2320.py`) is copied
from [the Adafruit GitHub page](https://github.com/adafruit/Adafruit_CircuitPython_AM2320) and modified to work with our
implementation of the CircuitPython API. See the attached licence file.

The AM2320 driver is compatible with the [AM2315](https://www.adafruit.com/product/1293) and
the [AM2320](https://www.adafruit.com/product/3721).
