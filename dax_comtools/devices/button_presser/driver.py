import logging
import serial
import typing

from dax_comtools.util.recurring_task import RecurringTaskBase

logger = logging.getLogger(__name__)


class ButtonPresserDriver:
    """Driver for button presser created by STAQ team to reset ablation laser."""

    DEFAULT_BAUD_RATE: int = 115200
    """Default baud rate."""

    _sim_output_stack: typing.List[str] = []

    def __init__(self, device: str, *,
                 baud_rate: int = DEFAULT_BAUD_RATE, write_timeout: float = 1, simulation: bool = False,
                 **kwargs: typing.Any):
        """
        :param device: serial port device is connected to
        :param baud_rate: serial port baud rate
        :param simulation: flag to run in simulation mode (dump all received messages to stdout)
        :param kwargs: Keyword arguments passed to the serial object
        """
        assert isinstance(device, str)
        assert isinstance(baud_rate, int)
        assert isinstance(simulation, bool)
        assert isinstance(write_timeout, (int, float))

        self.simulation: bool = simulation

        # don't need to open connection to device when simulating
        if simulation:
            return

        if baud_rate not in serial.Serial.BAUDRATES:
            raise ValueError('Invalid baud rate')
        self._conn = serial.Serial(port=device, baudrate=baud_rate,
                                   bytesize=serial.EIGHTBITS, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE,
                                   write_timeout=write_timeout,
                                   **kwargs)

    # noinspection PyMethodMayBeStatic
    def ping(self):
        """Always returns true (indication that the driver is running)."""
        return True

    def _write(self, message: str):
        if self.simulation:
            self._sim_output_stack += [message]
            logger.info(message)
        else:
            try:
                self._conn.write(message.encode("ascii"))
            except serial.SerialTimeoutException:
                logger.warning("Serial Write Timeout")

    def on(self):
        """Turn on."""
        self._write("S")

    def off(self):
        """Turn off."""
        self._write("E")

    def close(self):
        """Close serial connection."""
        if not self.simulation:
            self._conn.close()


class ButtonPresserDriverV2(ButtonPresserDriver, RecurringTaskBase):
    """
    Driver for button presser created by STAQ team to reset ablation laser.
    Modified for V2 to include better watchdog and fault performance
    See https://gitlab.com/duke-artiq/misc/button_pusher_software
    """

    DEFAULT_BAUD_RATE: int = 115200
    """Default baud rate."""

    laser_on = False

    _message_dict = {
        'N': "On",
        'S': "Off-Stopping",
        'F': "Off",
        'W': "Off-Watchdog",
        'X': "Unknown"
    }

    def __init__(self, device: str, *,
                 baud_rate: int = DEFAULT_BAUD_RATE, simulation: bool = False, recurring_update: bool = False,
                 write_interval: int = 1, **kwargs: typing.Any):
        """
        :param device: serial port device is connected to
        :param baud_rate: serial port baud rate
        :param simulation: flag to run in simulation mode (dump all received messages to stdout)
        :param recurring_update: Continuously send the `On` command at the rate specified by the `write_interval`
        :param write_interval: The rate at which to send serial updates to the device
        :param kwargs: Keyword arguments passed to the serial object
        """
        assert isinstance(device, str), f'device {device} is not a string'
        assert isinstance(baud_rate, int), f'baud_rate {baud_rate} is not an integer'
        assert isinstance(simulation, bool), f'simulation {simulation} is not a boolean'
        assert isinstance(recurring_update, bool), f'recurring_update {recurring_update} is not a boolean'
        assert isinstance(write_interval, (int, float)), f'write_interval {write_interval} is not a number'

        super().__init__(device=device, baud_rate=baud_rate, simulation=simulation, **kwargs)

        # Init Recurring Task, start writing
        if recurring_update:
            RecurringTaskBase.__init__(self, interval=1.0, start=True, daemon=True)

    def _read(self) -> str:
        """
        Read serial buffer
        :return: Most recent status character
        (N: On)
        (S: Off-Stopping)
        (F: Off)
        (W: Off-Watchdog)
        (X: Unknown)
        """
        if not self.simulation:
            try:
                message = self._conn.read(self._conn.in_waiting).decode('utf-8')[-1]
            except Exception:
                message = 'X'
        else:  # self.simulation
            message = 'N' if self.laser_on else 'F'

        return message

    def task(self):
        if (self.laser_on):
            self._write("N")

    def on(self):
        """Turn on."""
        self.laser_on = True
        self._write("N")

    def off(self):
        """Turn off."""
        self.laser_on = False
        self._write("F")

    def get_state(self) -> str:
        """Get the state string of the system"""
        return self._read()

    def get_state_msg(self) -> str:
        """Get the state string of the system as a human-readable message"""
        message = self._read()

        try:
            return self._message_dict[message]
        except Exception:
            logger.error(f"Message '{message}' does not appear in lookup dictionary: {self._message_dict}")
            return "Invalid"
