# SHT31-D driver

The Adafruit CircuitPython SHT31-D driver (`adafruit_sht31d.py`) is copied
from [the Adafruit GitHub page](https://github.com/adafruit/Adafruit_CircuitPython_SHT31D) and modified to work with our
implementation of the CircuitPython API. See the attached licence file.
