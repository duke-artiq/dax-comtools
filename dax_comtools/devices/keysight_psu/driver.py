import logging
import pyvisa
from pyvisa.constants import Parity, StopBits
import typing

# Import psu classes from psu directory
from dax_comtools.devices.keysight_psu.psu.base import KeysightPowerSupplyBase
from dax_comtools.devices.keysight_psu.psu.base import (
    KeysightPowerSupplyConnectionError,
)
from dax_comtools.devices.keysight_psu.psu.autodetector import (
    KeysightPowerSupplyAutoDetector,
)
from dax_comtools.devices.keysight_psu.psu.offline import KeysightOfflinePowerSupply
from dax_comtools.devices.keysight_psu.psu.sim import KeysightPowerSupplySim
from dax_comtools.devices.keysight_psu.psu.e3631a import AgilentPowerSupplyE3631A
from dax_comtools.devices.keysight_psu.psu.e3633a import AgilentPowerSupplyE3633A
from dax_comtools.devices.keysight_psu.psu.e3634a import AgilentPowerSupplyE3634A
from dax_comtools.devices.keysight_psu.psu.e36102b import KeysightPowerSupplyE36102B
from dax_comtools.devices.keysight_psu.psu.e36313a import KeysightPowerSupplyE36313A
from dax_comtools.devices.keysight_psu.psu.e36231a import KeysightPowerSupplyE36231A
from dax_comtools.devices.keysight_psu.psu.n6700c import KeysightPowerSupplyN6700C4xN6731B

__all__ = ["KeysightPowerSupplyDriver"]

logger = logging.getLogger(__name__)
"""The module logger."""

_PSU_CLASSES: typing.Sequence[typing.Type[KeysightPowerSupplyBase]] = [
    KeysightOfflinePowerSupply,
    KeysightPowerSupplySim,
    AgilentPowerSupplyE3631A,
    AgilentPowerSupplyE3633A,
    AgilentPowerSupplyE3634A,
    KeysightPowerSupplyE36102B,
    KeysightPowerSupplyE36313A,
    KeysightPowerSupplyE36231A,
    KeysightPowerSupplyN6700C4xN6731B,
]
"""List of available PSU classes."""


class KeysightPowerSupplyDriver:
    """Driver for Keysight power supplies."""

    VALID_MODELS: typing.ClassVar[typing.FrozenSet[str]] = frozenset(
        cls.get_device_model() for cls in _PSU_CLASSES
    )
    """Valid agilent model numbers."""

    _CONN_READ_TERMINATION: typing.ClassVar[str] = "\n"
    """Read termination character for serial communication to PSU"""
    _CONN_WRITE_TERMINATION: typing.ClassVar[str] = "\n"
    """Write termination character for serial communication to PSU"""

    VALID_BAUD_RATES: typing.ClassVar[typing.FrozenSet[int]] = frozenset(
        [
            300,
            600,
            1200,
            2400,
            4800,
            9600,
        ]
    )
    """Valid baud rates."""

    def __init__(
        self,
        device: str,
        *,
        model: str = "",
        bypass_remote_mode: bool = False,
        connection_optional: bool = False,
        simulation: bool = False,
        **kwargs: typing.Any,
    ):
        """Construct a new driver object.

        :param device: serial port device is connected to
        :param model: power supply model name, e.g. "E3631A"
        :param bypass_remote_mode: flag to bypass setting device in remote mode
        :param connection_optional: if True an exception will be thrown if the psu is not connected and reachable
        :param simulation: flag to run in simulation mode (dump all received messages to stdout)
        :param kwargs: keyword arguments to be used to change instrument attributes after establishing connection
        """
        assert isinstance(device, str)
        assert isinstance(model, str)
        assert isinstance(bypass_remote_mode, bool)
        assert isinstance(connection_optional, bool)
        assert isinstance(simulation, bool)

        # If a model was passed in, make sure that it is in the valid PSU classes list
        if model and model.lower() not in self.VALID_MODELS:
            raise ValueError(
                f"Invalid model number, valid options are {self.VALID_MODELS}."
            )

        # Store attributes
        self._device: str = device
        self._model: str = model.lower()
        self._simulation: bool = simulation
        self._connection_optional: bool = connection_optional
        self._bypass_remote_mode: bool = bypass_remote_mode
        # Set connection default as None for simulation
        self._conn: typing.Any = None

        # Set default Kwargs
        kwargs.setdefault("read_termination", self._CONN_READ_TERMINATION)
        kwargs.setdefault("write_termination", self._CONN_WRITE_TERMINATION)
        kwargs.setdefault("timeout", 5000)
        # Set serial kwargs only if asrl is specified
        if self._device.casefold().startswith("asrl"):
            kwargs.setdefault("parity", Parity.none)
            kwargs.setdefault("stop_bits", StopBits.two)
            baud_rate = kwargs.setdefault("baud_rate", max(self.VALID_BAUD_RATES))
            if baud_rate not in self.VALID_BAUD_RATES:
                raise ValueError(
                    f"Invalid baud rate, valid options are {self.VALID_BAUD_RATES}"
                )

        if not self._simulation:
            # Set up connection
            logger.debug(f'Setting up connection on device "{self._device}"')
            self._conn = pyvisa.ResourceManager().open_resource(
                self._device,
                **kwargs,
            )
        else:
            # Create simulation variables
            logger.info("Simulation enabled")

        self._psu = self._get_psu()
        # If a specific psu device is specified then create an instance of that device
        # otherwise get the psu id and attempt to create a psu instance based on the id

    def _get_psu(self) -> KeysightPowerSupplyBase:
        """Instantiates an KeysightPowerSupply class for the specified model. If no model is provided it attempts to
        identify the model using the autodetection feature.

        :returns: An instance of an implemented KeysightPowerSupplyBase class for the specified (or autodetected) model.
        """

        if not self._model:
            # If a model was not passed try to run autodetection
            autodetector = KeysightPowerSupplyAutoDetector(
                conn=self._conn,
                simulation=self._simulation,
                connection_optional=self._connection_optional,
                bypass_remote_mode=self._bypass_remote_mode,
            )
            self._model = autodetector.get_model_from_device().lower()

        # Try to instantiate the specified psu model
        try:
            if (
                self._simulation
                and self._model
                == KeysightPowerSupplyAutoDetector.get_device_model().lower()
            ):
                # If the system uses auto-detection and is in simulation mode, autodetector class will return
                #  own model name. Use generic simulation PSU in this case
                cls: typing.Type[KeysightPowerSupplyBase] = KeysightPowerSupplySim
            else:
                cls = {cls.get_device_model(): cls for cls in _PSU_CLASSES}[self._model]
        except KeyError:
            # If the provided model (or the autodetected model) is not valid then raise exception
            raise ValueError(
                f"Invalid model number. Received {self._model}. "
                f"Valid options are {self.VALID_MODELS}"
            ) from None
        else:
            try:
                return cls(
                    conn=self._conn,
                    simulation=self._simulation,
                    connection_optional=self._connection_optional,
                    bypass_remote_mode=self._bypass_remote_mode,
                )
            except KeysightPowerSupplyConnectionError as error:
                # In the case that the check_connection check fails (when a psu model is explicitly specified along
                #   with connection_optional) instantiate the OfflinePowerSupply class.
                if self._connection_optional:
                    return KeysightOfflinePowerSupply(
                        conn=self._conn,
                        simulation=self._simulation,
                        connection_optional=self._connection_optional,
                        bypass_remote_mode=self._bypass_remote_mode,
                    )
                else:
                    raise error

    # noinspection PyMethodMayBeStatic
    def ping(self) -> bool:
        """Always returns true (indication that the driver is running)."""
        return True

    def get_device_id(self, fast: bool = False) -> str:
        """Gets the device ID from the PSU.

        :param fast: If True gets id from class, if False queries device for id
        :returns: The device id string
        """
        if fast:
            return self._psu.get_device_id()
        else:
            return self._psu.get_id_from_device()

    def get_device_model(self, fast: bool = False) -> str:
        """Gets the device model from the PSU.

        :param fast: If True gets model from class, if False queries device for model
        :returns: The device model string
        """
        if fast:
            return self._psu.get_device_model()
        else:
            return self._psu.get_model_from_device()

    def check_connection(self) -> None:
        """Check the connection, raises an exception in case of problems. Take into account if using the offline psu
        driver, in which case no problems are raised

        :raises KeysightPowerSupplyConnectionError: Raised in case there is a bad connection when something is expected
        """
        self._psu.check_connection()

    def enable_remote_mode(self) -> None:
        """Configure the power supply to be in REMOTE mode. Sending or receiving data over the
        RS-232 interface when not configured for remote operation can cause unpredictable results."""
        self._psu.enable_remote_mode()

    def set_target_output(self, target_output: str) -> None:
        """Sets the output target of the power supply.

        :param target_output: The name of the output on the power supply (name is model dependent).
        """
        self._psu.set_target_output(target_output)

    def toggle_output(self, status: bool, target_output: str = "") -> None:
        """Sets the status of a specified output on the power supply.
        Multi-output models require a target_output to be sepecified.

        :param status: Enables output if True, disables if False
        :param target_output: Name of the target output on a multi output device
        """
        self._psu.toggle_output(status=status, target_output=target_output)

    def toggle_all_outputs(self, status: bool) -> None:
        """Sets the status of all outputs on the power supply.

        :param status: Enables outputs if True, disables if False
        """
        self._psu.toggle_all_outputs(status)

    def apply(
        self,
        voltage: float,
        current: typing.Optional[float] = None,
        target_output: typing.Optional[str] = "",
    ) -> None:
        """Sets the voltage and current on a target output of the power supply.
        Target output must be specified for multi output devices.

        :param voltage: The desired voltage
        :param current: The desired current
        :param target_output: The name of the output on the power supply (name is model dependent)
        """
        self._psu.apply(voltage=voltage, current=current, target_output=target_output)

    def get_output_settings(
        self, target_output: str = ""
    ) -> typing.Tuple[float, float]:
        """Returns the voltage and current values for the specified target output.
        Target output must be specified for multi output devices.

        :param target_output: The target output
        :returns: A tuple of settings (voltage, current) for the target output
        """
        return self._psu.get_output_settings(target_output)

    def get_output_measurements(
        self, target_output: str = ""
    ) -> typing.Tuple[float, float]:
        """Returns the voltage and current values measured on a specified target output.
        Target output must be specified for multi output devices.

        :param target_output: The target output
        :returns: A tuple of measured values (voltage, current) on the target output
        """
        return self._psu.get_output_measurements(target_output)

    def clear(self) -> None:
        """Clears the event register and error array of the power supply."""
        self._psu.clear()

    def reset(self) -> None:
        """Resets the power supply to default settings"""
        self._psu.reset()

    def close(self) -> None:
        """Close connection."""
        logger.debug("Closing connection")
        if not self._simulation:
            self._conn.close()

    def is_online(self) -> bool:
        """True if the device is online."""
        return self._psu.is_online()
