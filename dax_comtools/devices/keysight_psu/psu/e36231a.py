import typing
import logging

from dax_comtools.devices.keysight_psu.psu.base import (
    KeysightPowerSupplyBase,
    KeysightPowerSupplySingleOutputError,
    OutputRanges,
)

logger = logging.getLogger(__name__)
"""The module logger."""


class KeysightPowerSupplyE36231A(KeysightPowerSupplyBase):
    """An implementation of the KeysightPowerSupplyBase for the Keysight E36231A PSU."""

    _OUTPUT_RANGES = {
        "e36231a": OutputRanges(
            MIN_VOLTAGE=0.0, MAX_VOLTAGE=30.9, MIN_CURRENT=0.0, MAX_CURRENT=20.6
        )
    }
    """Dictionary that stores the E36231A power supply's available outputs and the min/max values for those outputs.
                            Each output has a different min/max range for voltage and current."""

    _DEVICE_ID = "Keysight Technologies,E36231A,MY61001034,1.0.0-1.0.2-1.00"
    """The device identifier."""

    _DEVICE_MODEL = "e36231a"

    def set_target_output(self, target_output: str) -> None:
        """Sets the target output of a multi ouptut power supply.

        :param target_output: The name of the output on the power supply (name is model dependent).
        """
        raise KeysightPowerSupplySingleOutputError(self._DEVICE_MODEL)

    def toggle_output(self, status: bool, target_output: str = "") -> None:
        """Sets the status of a specified output on the power supply.
        Multi-output models require a target_output to be sepecified.

        :param status: Enables output if True, disables if False
        :param target_output: Name of the target output on a multi output device
        """
        assert isinstance(status, bool), "Status must be of type bool."
        if target_output:
            raise KeysightPowerSupplySingleOutputError(self._DEVICE_MODEL)
        status_str = "ON" if status else "OFF"
        self._write(f"OUTP {status_str}")

    def toggle_all_outputs(self, status: bool) -> None:
        """Sets the status of all outputs on the power supply.

        :param status: Enables outputs if True, disables if False
        """
        assert isinstance(status, bool), "Status must be of type bool."
        status_str = "ON" if status else "OFF"
        self._write(f"OUTP {status_str}")

    def apply(
        self,
        voltage: float,
        current: typing.Optional[float] = None,
        target_output: typing.Optional[str] = "",
    ) -> None:
        """Sets the voltage and current on a target output of the power supply.
        Target output must be specified for multi output devices.

        :param voltage: The desired voltage
        :param current: The desired current
        :param target_output: The name of the output on the power supply (name is model dependent)
        """
        if target_output:
            raise KeysightPowerSupplySingleOutputError(self._DEVICE_MODEL)

        cmd_base = "APPL "  # Add space for correct cmd generation in helper function
        self._apply(cmd_base, "e36231a", voltage, current)

    def get_output_settings(
        self, target_output: str = ""
    ) -> typing.Tuple[float, float]:
        """Returns the voltage and current values for the specified target.

        :param target_output: The target output
        :returns: A tuple of settings for the target output
        """

        if target_output:
            raise KeysightPowerSupplySingleOutputError(self._DEVICE_MODEL)

        cmd = "APPL?"
        return self._get_output_settings(cmd, "e36231a")

    def get_output_measurements(
        self, target_output: str = ""
    ) -> typing.Tuple[float, float]:
        """Returns the voltage and current values measured on a specified target output.
        Target output must be specified for multi output devices.

        :param target_output: The target output
        :returns: A tuple of measured values (voltage, current) on the target output
        """
        if target_output:
            raise KeysightPowerSupplySingleOutputError(self._DEVICE_MODEL)

        return self._get_output_measurements("e36231a", append=False)
