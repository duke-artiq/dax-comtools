import typing

from dax_comtools.devices.keysight_psu.psu.base import (
    KeysightPowerSupplyError,
    KeysightPowerSupplyBase,
)


class KeysightPowerSupplyOfflineError(KeysightPowerSupplyError):
    """Error used when device is set to offline using connection optional."""

    def __init__(self):
        super().__init__("No power supply connected.")


class KeysightOfflinePowerSupply(KeysightPowerSupplyBase):
    """An implementation of the KeysightPowerSupplyBase for use whenever there is no power supply connected. Whenever
    the connection_optional flag is set, this will be the power supply used - so that any important method calls will
    result in an error being thrown
    """

    _DEVICE_ID = "offline_psu"

    _DEVICE_MODEL = "offline_psu"

    def __init__(
        self,
        conn: typing.Any,
        *,
        bypass_remote_mode: bool = False,
        connection_optional: bool = False,
        simulation: bool = False,
    ):
        # Do not set up any of the connection parameters. Skip everything here
        pass

    def check_connection(self) -> None:
        """Check the connection, raises an exception in case of problems. Takes into account connection-optional.

        :raises AgilentPowerSupplyDeviceTypeError: Raised in case of a device ID mismatch
        """
        # Override: If using the Offline psu object, then it is in a correct state
        pass

    def set_target_output(self, target_output: str) -> None:
        """Sets the target output of a multi ouptut power supply.

        :param target_output: The name of the output on the power supply (name is model dependent).
        """
        raise KeysightPowerSupplyOfflineError

    def is_online(self) -> bool:
        """True if the device is online."""
        return False

    def toggle_output(self, status: bool, target_output: str = "") -> None:
        """Sets the status of a specified output on the power supply.
        Multi-output models require a target_output to be sepecified.

        :param status: Enables output if True, disables if False
        :param target_output: Name of the target output on a multi output device
        """
        raise KeysightPowerSupplyOfflineError

    def toggle_all_outputs(self, status: bool) -> None:
        """Sets the status of all outputs on the power supply.

        :param status: Enables outputs if True, disables if False
        """
        raise KeysightPowerSupplyOfflineError

    def apply(
        self,
        voltage: float,
        current: typing.Optional[float] = None,
        target_output: typing.Optional[str] = "",
    ) -> None:
        """Sets the voltage and current on a target output of the power supply.
        Target output must be specified for multi output devices.

        :param voltage: The desired voltage
        :param current: The desired current
        :param target_output: The name of the output on the power supply (name is model dependent)
        """
        raise KeysightPowerSupplyOfflineError

    def get_output_settings(
        self, target_output: str = ""
    ) -> typing.Tuple[float, float]:
        """Returns the voltage and current values for the specified target output.
        Target output must be specified for multi output devices.

        :param target_output: The target output
        :returns: A tuple of settings for the target output
        """
        raise KeysightPowerSupplyOfflineError

    def get_output_measurements(
        self, target_output: str = ""
    ) -> typing.Tuple[float, float]:
        """Returns the voltage and current values measured on a specified target output.
        Target output must be specified for multi output devices.

        :param target_output: The target output
        :returns: A tuple of measured values (voltage, current) on the target output
        """
        raise KeysightPowerSupplyOfflineError
