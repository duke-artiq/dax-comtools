import abc
import time
import logging
import typing
import dataclasses
import re
import pyvisa

__all__ = [
    "KeysightPowerSupplyError",
    "KeysightPowerSupplyConnectionError",
    "KeysightPowerSupplyBase",
]

logger = logging.getLogger(__name__)
"""The module logger."""


class KeysightPowerSupplyError(RuntimeError):
    """Error raised when there are issues with connecting or communicating with the device."""

    pass


class KeysightPowerSupplyConnectionError(RuntimeError):
    """Error used only in specific cases to differentiate in cases when the connection_optional flag matters."""

    pass


class KeysightPowerSupplySingleOutputError(KeysightPowerSupplyError):
    """Error used when target output is specified on single output devices."""

    def __init__(self, model):
        super().__init__(
            f"The {model} model is a single output PSU."
            f" The output target cannot be set."
        )


class KeysightPowerSupplyMultiOutputError(KeysightPowerSupplyError):
    """Error used when target output is not specified on multi output devices."""

    def __init__(self, model):
        super().__init__(
            f"The {model} model is a multi output PSU."
            f" Target output must be specified on multi output devices."
        )


class KeysightPowerSupplyTargetOutputError(KeysightPowerSupplyError):
    """Error used when an invailid target output is specified."""

    def __init__(self, target_output, output_ranges):
        super().__init__(
            f"Specified target output '{target_output}' is not supported. "
            f"Supported outputs: {', '.join(output_ranges)}"
        )


@dataclasses.dataclass(frozen=True)
class OutputRanges:
    """Dataclass to store output range information."""

    MIN_VOLTAGE: float
    MAX_VOLTAGE: float
    MIN_CURRENT: float
    MAX_CURRENT: float

    def voltage_in_range(self, voltage: float) -> bool:
        """True if the given voltage is in range."""
        return self.MIN_VOLTAGE <= voltage <= self.MAX_VOLTAGE

    def current_in_range(self, current: float) -> bool:
        """True if the given current is in range."""
        return self.MIN_CURRENT <= current <= self.MAX_CURRENT


@dataclasses.dataclass
class _SimOutput:
    """Represents a simulated device output for simulation mode."""

    _voltage: float = 0.0
    _current: float = 0.0

    def apply(self, voltage: float, current: typing.Optional[float] = None) -> None:
        """Applies specfied values to the simulated output.

        :param voltage: The voltage to be set on the simulated output
        :param current: The current to be set on the simulated output
        """
        self._voltage = voltage
        if current is not None:
            self._current = current

    def get_output_settings(self) -> typing.Tuple[float, float]:
        """Gets the current settings of the sim output.

        :returns: Tuple of (voltage, current)
        """
        return self._voltage, self._current

    def reset(self) -> None:
        """Resets the voltage and current of the output."""
        self._voltage = 0.0
        self._current = 0.0


class KeysightPowerSupplyBase(abc.ABC):
    """Base driver for the family of Keysight (and Agilent) power supplies."""

    _OUTPUT_RANGES: typing.ClassVar[typing.Dict[str, OutputRanges]]
    """Dictionary that stores the power supply's available outputs and the min/max values for those outputs.
            Each output has a different min/max range for voltage and current."""
    _DEVICE_ID: typing.ClassVar[str]
    """The device identifier."""
    _DEVICE_MODEL: typing.ClassVar[str]
    """The device model."""

    OUTPUT_SETTINGS_PATTERN: typing.ClassVar[typing.Pattern[str]] = re.compile('"(.*)"')
    """Regex pattern to parse output settings"""

    def __init__(
        self,
        conn: typing.Any,
        *,
        bypass_remote_mode: bool = False,
        connection_optional: bool = False,
        simulation: bool = False,
    ):
        """Base initialization for a Keysight power supply driver.

        :param conn: The PyVisa connection to the power supply.
        :param bypass_remote_mode: flag to bypass setting device in remote mode
        :param connection_optional: if True an exception will be thrown if the awg is not connected and reachable
        :param simulation: flag to run in simulation mode (dump all received messages to stdout)
        """
        assert isinstance(
            self._OUTPUT_RANGES, dict
        ), "Output sources must be of type dict"
        assert isinstance(self._DEVICE_ID, str), "Device ID must be of type str"
        assert isinstance(self._DEVICE_MODEL, str), "Device model must be of type str"

        self._conn: typing.Any = conn
        self._simulation: bool = simulation
        self._connection_optional: bool = connection_optional
        self._bypass_remote_mode: bool = bypass_remote_mode
        self._target_output: typing.Optional[str] = None

        # Some devices have unexpected behavior when remote mode is enabled. If the bypass_remote_mode flag
        # is set then we will skip setting the device in remote mode to avoid the unexpected behavior.
        if self._bypass_remote_mode:
            logger.warning(
                "Device was not placed in Remote Mode. Sending or receiving data when not configured "
                "for remote operation can cause unpredictable results."
            )
        else:  # Default behavior is to enable remote mode
            self.enable_remote_mode()

        if self._simulation:
            # Set up outputs for simulation mode
            self._sim_outputs: typing.Dict[str, _SimOutput] = {
                output: _SimOutput() for output in self._OUTPUT_RANGES
            }
        else:
            # Insert a delay
            time.sleep(1.0)

        # Ensure that the device is properly connected
        self.check_connection()

    def _write(self, message: str) -> None:
        """Wrapper code for write functionality to take into account simulations."""
        logger.debug(f"Write: {message}")
        if not self._simulation:
            self._conn.write(message)

    def _query(self, message: str) -> str:
        """Wrapper code for query functionality to take into account simulations."""
        logger.debug(f"Query: {message}")
        if not self._simulation:
            return self._conn.query(message)
        else:
            raise RuntimeError(
                f'Reached a query in simulation mode with message "{message}"'
            )

    def _connected(self) -> bool:
        """Check the connection, return boolean if the correct device is connected or not.

        :return True if there is a physical device connected, False if there is none
        """
        connected = True
        try:
            # Validate that the model we think we are is indeed the model returned by the Agilent device.
            model = self.get_model_from_device()
            if model.lower() != self.get_device_model().lower():
                # If the device ID of from the device does not match the expected value, set bad_connection
                connected = False
        except ConnectionError:
            # If an exception is raised when trying to connect to the device then set bad_connection
            connected = False
        finally:
            return connected

    def _apply(
        self,
        cmd_base: str,
        target_output: str,
        voltage: float,
        current: typing.Optional[float],
    ) -> None:
        """Helper for writing abstract apply function for each PSU class.

        :param cmd_base: The base command string that will be passed to the device
        :param target_output: The target output on the power supply
        :param voltage: The desired voltage
        :param current: The desired current
        """
        assert isinstance(cmd_base, str), "Command base must be of type str."
        assert isinstance(target_output, str), "Target output must be of type str."
        assert isinstance(voltage, float), "Voltage must be of type float."
        assert current is None or isinstance(
            current, float
        ), "Current must be None or of type float."

        output_ranges = self._OUTPUT_RANGES[target_output]
        # List with components of the command that will be joined together later
        cmd_components = []
        # validate voltage and current values are within range for given output
        if output_ranges.voltage_in_range(voltage):
            cmd_components.append(f"{voltage}")
        else:
            raise KeysightPowerSupplyError(
                f"Voltage of {voltage} is out of range. Value must be between "
                f"{output_ranges.MIN_VOLTAGE} and {output_ranges.MAX_VOLTAGE}."
            )

        if current is not None:
            if output_ranges.current_in_range(current):
                cmd_components.append(f"{current}")
            else:
                raise KeysightPowerSupplyError(
                    f"Current of {current} is out of range. Value must be between "
                    f"{output_ranges.MIN_CURRENT} and {output_ranges.MAX_CURRENT}."
                )

        cmd = "".join([cmd_base, ",".join(cmd_components)])

        # Apply voltage and current values to the output specified in the given cmd_base
        if self._simulation:
            self._sim_outputs[target_output].apply(voltage, current)
            logger.info(cmd)
        else:
            self._write(cmd)

    def _get_output_settings(
        self, cmd: str, target_output: str
    ) -> typing.Tuple[float, float]:
        """Returns the voltage and current values reviced from querying the specified command.

        :param cmd: Command string to be sent to the device
        :param target_output: The target output on the power supply
        :returns: A tuple of settings (voltage, current) for the target output
        """
        assert isinstance(cmd, str), "Command must be of type str."
        assert isinstance(target_output, str), "Target output must be of type str."

        if not self._simulation:
            # Parse output from PSU
            settings_raw = self._query(cmd)
            search_result = self.OUTPUT_SETTINGS_PATTERN.search(settings_raw)
            if search_result:
                settings = search_result.group(1).split(",")
                if len(settings) == 2:
                    try:
                        return float(settings[0]), float(settings[1])
                    except ValueError:
                        raise KeysightPowerSupplyError(
                            f"Unable to parse response from power supply: {settings}."
                        )
                else:
                    raise KeysightPowerSupplyError(
                        f"Unexpected response received from power supply. Expected two "
                        f"values, received {len(settings)}."
                    )
            else:
                raise KeysightPowerSupplyError(
                    f"Unexpected response received from power supply: {settings_raw}."
                )
        else:
            return self._sim_outputs[target_output].get_output_settings()

    def _get_output_measurements(
        self,
        target_output: str,
        append: bool,
    ) -> typing.Tuple[float, float]:
        """Returns the voltage and current values reviced from querying the specified command.

        :param target_output: The target output on the power supply
        :param append: If True, appends the target_output on the end of the commands send to the device
        (Used for multi ouput devices). If False, sends command with no target_output (Used for single output devices)
        :returns: A tuple of measured values (voltage, current) for the target output
        """
        assert isinstance(target_output, str), "Target output must be of type str."
        assert isinstance(append, bool), "Append must be of type bool."

        if not self._simulation:
            voltage_cmd = "MEAS:VOLT?"
            current_cmd = "MEAS:CURR?"
            if append:
                voltage_cmd = f"{voltage_cmd} {target_output}"
                current_cmd = f"{current_cmd} {target_output}"
            measured_voltage = self._query(voltage_cmd)
            measured_current = self._query(current_cmd)
            logger.debug(f"Voltage query: {voltage_cmd}, Current query: {current_cmd}")
            return (float(measured_voltage), float(measured_current))
        else:
            return self._sim_outputs[target_output].get_output_settings()

    @classmethod
    def get_device_id(cls) -> str:
        """Getter for the device identifier."""
        if cls._DEVICE_ID:
            return cls._DEVICE_ID
        else:
            raise NameError("_DEVICE_ID is not set.")

    @classmethod
    def get_device_model(cls) -> str:
        """Getter for the device model."""
        if cls._DEVICE_MODEL:
            return cls._DEVICE_MODEL
        else:
            raise NameError("_DEVICE_MODEL is not set.")

    @property
    def output_ranges(self) -> typing.Dict[str, OutputRanges]:
        """Getter for the device output ranges"""
        if self._OUTPUT_RANGES:
            return self._OUTPUT_RANGES
        else:
            raise NameError("_OUTPUT_RANGES is not set.")

    def get_id_from_device(self) -> str:
        """Gets the device ID from the Keysight PSU"""
        if self._simulation:
            return self.get_device_id()
        else:
            return self._query("*IDN?")

    def get_model_from_device(self) -> str:
        """Gets the device model from the Keysight PSU"""
        if self._simulation:
            return self.get_device_model()
        else:
            try:
                parts = self.get_id_from_device().split(",")
            # If can't establish a connection to the device, check if the connection is optional
            except pyvisa.errors.VisaIOError as error:
                # When in the specific case that the connection is optional, return the name of the Offline class
                if self._connection_optional:
                    logger.warning(
                        "No power supply is connected. Will be unable to handle power supply functions."
                    )
                    return "offline_psu"  # Offline psu model
                else:
                    raise error

            if len(parts) > 1:
                return parts[1]
            else:
                raise KeysightPowerSupplyError("Unable to parse device identifier.")

    def check_connection(self) -> None:
        """Check the connection, raises an exception in case there is not a correct physical device connected.

        :raises KeysightPowerSupplyDeviceTypeError: Raised in case of a device ID mismatch
        """
        if not self._connected():
            raise KeysightPowerSupplyConnectionError(
                "Device is either unavailable or an unrecognized device is connected."
            )

    def enable_remote_mode(self) -> None:
        """Configure the power supply to be in REMOTE mode. Sending or receiving data over the
        RS-232 interface when not configured for remote operation can cause unpredictable results."""
        self._write("SYST:REM")

    def _is_valid_target(self, target_output: str) -> bool:
        """True if the given target is valid.

        :param target_output: The target to check
        :returns: True if the given target is valid
        """
        return target_output in self._OUTPUT_RANGES

    def set_target_output(self, target_output: str) -> None:
        """Sets the target output of a multi ouptut power supply.

        :param target_output: The name of the output on the power supply (name is model dependent).
        """
        if self._is_valid_target(target_output):
            self._target_output = target_output
            if not self._simulation:
                self._write(f"INST {target_output}")
        else:
            raise KeysightPowerSupplyError("Specified target output is not supported.")

    def clear(self) -> None:
        """Clears the event register and error array of the power supply."""
        self._write("*CLS")

    def reset(self) -> None:
        """Resets the power supply to default settings"""
        # Reset stored values for each sim output
        if self._simulation:
            for output in self.output_ranges:
                self._sim_outputs[output].reset()
        self._write("*RST")

    def is_online(self) -> bool:
        """True if the device is online."""
        return True

    @abc.abstractmethod
    def toggle_output(self, status: bool, target_output: str = "") -> None:
        """Sets the status of a specified output on the power supply.
        Multi-output models require a target_output to be sepecified.

        :param status: Enables output if True, disables if False
        :param target_output: Name of the target output on a multi output device
        """
        pass

    @abc.abstractmethod
    def toggle_all_outputs(self, status: bool) -> None:
        """Sets the status of all outputs on the power supply.

        :param status: Enables outputs if True, disables if False
        """
        pass

    @abc.abstractmethod
    def apply(
        self,
        voltage: float,
        current: typing.Optional[float] = None,
        target_output: typing.Optional[str] = "",
    ) -> None:
        """Sets the voltage and current on a target output of the power supply.
        Target output must be specified for multi output devices.

        :param voltage: The desired voltage
        :param current: The desired current
        :param target_output: The name of the output on the power supply (name is model dependent)
        """
        pass

    @abc.abstractmethod
    def get_output_settings(
        self, target_output: str = ""
    ) -> typing.Tuple[float, float]:
        """Returns the voltage and current values for the specified target output.
        Target output must be specified for multi output devices.

        :param target_output: The target output
        :returns: A tuple of settings (votlage, current) for the target output
        """
        pass

    @abc.abstractmethod
    def get_output_measurements(
        self, target_output: str = ""
    ) -> typing.Tuple[float, float]:
        """Returns the voltage and current values measured on a specified target output.
        Target output must be specified for multi output devices.

        :param target_output: The target output
        :returns: A tuple of measured values (voltage, current) on the target output
        """
        pass
