import typing
import logging

from dax_comtools.devices.keysight_psu.psu.base import (
    KeysightPowerSupplyBase,
    KeysightPowerSupplyTargetOutputError,
    KeysightPowerSupplyMultiOutputError,
    OutputRanges,
)

logger = logging.getLogger(__name__)
"""The module logger."""


class KeysightPowerSupplySim(KeysightPowerSupplyBase):
    """An implementation of the KeysightPowerSupplyBase for use in simulation when the physical system uses auto
    detection.
    """

    _DEVICE_ID = "sim_psu"

    _DEVICE_MODEL = "sim_psu"

    _OUTPUT_RANGES = {
        "e36102b": OutputRanges(
            MIN_VOLTAGE=0.0, MAX_VOLTAGE=6.0, MIN_CURRENT=0.0, MAX_CURRENT=5.0
        ),
        "e36231a": OutputRanges(
            MIN_VOLTAGE=0.0, MAX_VOLTAGE=30.9, MIN_CURRENT=0.0, MAX_CURRENT=20.6
        ),
        "P6V": OutputRanges(
            MIN_VOLTAGE=0.0, MAX_VOLTAGE=6.18, MIN_CURRENT=0.0, MAX_CURRENT=5.15
        ),
        "P8V": OutputRanges(
            MIN_VOLTAGE=0.0, MAX_VOLTAGE=8.24, MIN_CURRENT=0.0, MAX_CURRENT=20.6
        ),
        "P25V": OutputRanges(
            MIN_VOLTAGE=0.0, MAX_VOLTAGE=25.75, MIN_CURRENT=0.0, MAX_CURRENT=1.03
        ),
        "P50V": OutputRanges(
            MIN_VOLTAGE=0.0, MAX_VOLTAGE=51.5, MIN_CURRENT=0.0, MAX_CURRENT=4.12
        ),
        "N25V": OutputRanges(
            MIN_VOLTAGE=-27.75, MAX_VOLTAGE=0.0, MIN_CURRENT=0.0, MAX_CURRENT=1.03
        ),
    }

    def toggle_output(self, status: bool, target_output: str = "") -> None:
        """Sets the status of a specified output on the power supply.
        Multi-output models require a target_output to be sepecified.

        :param status: Enables output if True, disables if False
        :param target_output: Name of the target output on a multi output device
        """
        logger.warning(
            "Simulation PSU only allows for toggle all outputs. Calling toggle_all_outputs()..."
        )
        self.toggle_all_outputs(status)

    def toggle_all_outputs(self, status: bool) -> None:
        """Sets the status of all outputs on the power supply.

        :param status: Enables outputs if True, disables if False
        """
        # Set the status of all outputs
        self.status = "ON" if status else "OFF"

    def apply(
        self,
        voltage: float,
        current: typing.Optional[float] = None,
        target_output: typing.Optional[str] = "",
    ) -> None:
        """Sets the voltage and current on a target output of the power supply.
        Target output must be specified for multi output devices.

        :param voltage: The desired voltage
        :param current: The desired current
        :param target_output: The name of the output on the power supply (name is model dependent)
        """

        if not target_output:
            raise KeysightPowerSupplyMultiOutputError(self._DEVICE_MODEL)

        if self._is_valid_target(target_output):
            cmd_base = f"APPL {target_output}"
            self._apply(cmd_base, target_output, voltage, current)
        else:
            raise KeysightPowerSupplyTargetOutputError(
                target_output, self.output_ranges
            )

    def get_output_settings(
        self, target_output: str = ""
    ) -> typing.Tuple[float, float]:
        """Returns the voltage and current values for the specified target.

        :param target_output: The target output
        :returns: A tuple of settings for the target output
        """
        if not target_output:
            raise KeysightPowerSupplyMultiOutputError(self._DEVICE_MODEL)

        if self._is_valid_target(target_output):
            cmd = f"APPL? {target_output}"
            return self._get_output_settings(cmd, target_output)
        else:
            raise KeysightPowerSupplyTargetOutputError(
                target_output, self.output_ranges
            )

    def get_output_measurements(
        self, target_output: str = ""
    ) -> typing.Tuple[float, float]:
        """Returns the voltage and current values measured on a specified target output.
        Target output must be specified for multi output devices.

        :param target_output: The target output
        :returns: A tuple of measured values (voltage, current) on the target output
        """
        if not target_output:
            raise KeysightPowerSupplyMultiOutputError(self._DEVICE_MODEL)

        if self._is_valid_target(target_output):
            return self._get_output_measurements(target_output, append=True)
        else:
            raise KeysightPowerSupplyTargetOutputError(
                target_output, self.output_ranges
            )
