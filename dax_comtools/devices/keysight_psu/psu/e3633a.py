import typing
import logging

from dax_comtools.devices.keysight_psu.psu.base import (
    KeysightPowerSupplyError,
    KeysightPowerSupplyBase,
    KeysightPowerSupplyTargetOutputError,
    KeysightPowerSupplyMultiOutputError,
    OutputRanges,
)

logger = logging.getLogger(__name__)
"""The module logger."""


class AgilentPowerSupplyE3633A(KeysightPowerSupplyBase):
    """An implementation of the KeysightPowerSupplyBase for the Agilent E3633A PSU."""

    # TODO: update output ranges for the e3633a
    _OUTPUT_RANGES = {
        "P8V": OutputRanges(
            MIN_VOLTAGE=0.0, MAX_VOLTAGE=8.24, MIN_CURRENT=0.0, MAX_CURRENT=20.6
        ),
        "P20V": OutputRanges(
            MIN_VOLTAGE=0.0, MAX_VOLTAGE=20.6, MIN_CURRENT=0.0, MAX_CURRENT=10.3
        ),
    }
    """Dictionary that stores the E3633A power supply's available outputs and the min/max values for those outputs.
                    Each output has a different min/max range for voltage and current."""

    _DEVICE_ID = "HEWLETT-PACKARD,E3633A,0,2.1-5.0-1.0\r"
    """The device identifier."""

    _DEVICE_MODEL = "e3633a"
    """The device model."""

    def set_target_output(self, target_output: str) -> None:
        """Sets the target output of a multi ouptut power supply.

        :param target_output: The name of the output on the power supply (name is model dependent).
        """
        # Overrides base function to use correct command for the older model
        if self._is_valid_target(target_output):
            self._target_output = target_output
            if not self._simulation:
                self._write(f"VOLT:RANGE {target_output}")
        else:
            raise KeysightPowerSupplyTargetOutputError(
                target_output, self.output_ranges
            )

    def toggle_output(self, status: bool, target_output: str = "") -> None:
        """Sets the status of a specified output on the power supply.
        Multi-output models require a target_output to be sepecified.

        :param status: Enables output if True, disables if False
        :param target_output: Name of the target output on a multi output device
        """
        raise KeysightPowerSupplyError(
            "The E3633A model only supports toggling all outputs. Please use toggle_all_outputs()."
        )

    def toggle_all_outputs(self, status: bool) -> None:
        """Sets the status of all outputs on the power supply.

        :param status: Enables outputs if True, disables if False
        """
        assert isinstance(status, bool), "Status must be of type bool."
        status_str = "ON" if status else "OFF"
        self._write(f"OUTP {status_str}")

    def apply(
        self,
        voltage: float,
        current: typing.Optional[float] = None,
        target_output: typing.Optional[str] = "",
    ) -> None:
        """Sets the voltage and current on a target output of the power supply.
        Target output must be specified for multi output devices.

        :param voltage: The desired voltage
        :param current: The desired current
        :param target_output: The name of the output on the power supply (name is model dependent)
        """
        if not target_output:
            raise KeysightPowerSupplyMultiOutputError(self._DEVICE_MODEL)

        # Model requires target output to be set
        self.set_target_output(target_output)
        cmd_base = "APPL "  # Add space for correct cmd generation in helper function
        self._apply(cmd_base, target_output, voltage, current)

    def get_output_settings(
        self, target_output: str = ""
    ) -> typing.Tuple[float, float]:
        """Returns the voltage and current values for the specified target.

        :param target_output: The target output
        :returns: A tuple of settings for the target output
        """
        # Added warning and commented updated code to preseve backwards compatability with old AgilentPowerSupplyDriver
        if target_output:
            logger.warning(
                "The E3633A PSU does not accept a target output when querying the output settings. "
                'Ignoring parameter "target_output"'
            )

        # New Keysight driver should require target output for multi output models
        # assert target_output, "Target output must be specified on multi output devices."
        # self.set_target_output(target_output)

        if self._target_output:
            cmd = "APPL?"
            return self._get_output_settings(cmd, self._target_output)
        else:
            raise KeysightPowerSupplyError(
                "Target output not set. Target output must be set with set_target_output()"
            )

    def get_output_measurements(
        self, target_output: str = ""
    ) -> typing.Tuple[float, float]:
        """Returns the voltage and current values measured on a specified target output.
        Target output must be specified for multi output devices.

        :param target_output: The target output
        :returns: A tuple of measured values (voltage, current) on the target output
        """
        if not target_output:
            raise KeysightPowerSupplyMultiOutputError(self._DEVICE_MODEL)

        self.set_target_output(target_output)
        return self._get_output_measurements(target_output, append=False)
