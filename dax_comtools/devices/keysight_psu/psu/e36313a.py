import typing
import logging
from dax_comtools.devices.keysight_psu.psu.base import (
    KeysightPowerSupplyBase,
    KeysightPowerSupplyTargetOutputError,
    KeysightPowerSupplyMultiOutputError,
    OutputRanges,
)


logger = logging.getLogger(__name__)
"""The module logger."""


class KeysightPowerSupplyE36313A(KeysightPowerSupplyBase):
    """An implementation of the KeysightPowerSupplyBase for the Keysight E36313A PSU."""

    _OUTPUT_RANGES = {
        "P6V": OutputRanges(
            MIN_VOLTAGE=0.0, MAX_VOLTAGE=6.18, MIN_CURRENT=0.0, MAX_CURRENT=10.3
        ),
        "P25V": OutputRanges(
            MIN_VOLTAGE=0.0, MAX_VOLTAGE=25.75, MIN_CURRENT=0.0, MAX_CURRENT=2.06
        ),
        "N25V": OutputRanges(
            MIN_VOLTAGE=0.0, MAX_VOLTAGE=25.75, MIN_CURRENT=0.0, MAX_CURRENT=2.06
        ),
    }
    """Dictionary that stores the E36313A power supply's available outputs and the min/max values for those outputs.
                        Each output has a different min/max range for voltage and current."""

    _DEVICE_ID = "Keysight Technologies,E36313A,MY61002227,2.1.0-1.0.4-1.12"
    """The device identifier."""

    _DEVICE_MODEL = "e36313a"
    """ The device model."""

    def toggle_output(self, status: bool, target_output: str = "") -> None:
        """Sets the status of a specified output on the power supply.
        Multi-output models require a target_output to be sepecified.

        :param status: Enables output if True, disables if False
        :param target_output: Name of the target output on a multi output device
        """
        assert isinstance(status, bool), "Status must be of type bool."

        if not target_output:
            raise KeysightPowerSupplyMultiOutputError(self._DEVICE_MODEL)

        if self._is_valid_target(target_output):
            status_str = "ON" if status else "OFF"
            # Translate target output to channel list parameter using index of output source
            output_number = int(list(self.output_ranges).index(target_output)) + 1
            self._write(f"OUTP {status_str},(@{output_number})")
        else:
            raise KeysightPowerSupplyTargetOutputError(
                target_output, self.output_ranges
            )

    def toggle_all_outputs(self, status: bool) -> None:
        """Sets the status of all outputs on the power supply.

        :param status: Enables outputs if True, disables if False
        """
        assert isinstance(status, bool), "Status must be of type bool."
        status_str = "ON" if status else "OFF"
        self._write(f"OUTP {status_str},(@1,2,3)")

    def apply(
        self,
        voltage: float,
        current: typing.Optional[float] = None,
        target_output: typing.Optional[str] = "",
    ) -> None:
        """Sets the voltage and current on a target output of the power supply.
        Target output must be specified for multi output devices.

        :param voltage: The desired voltage
        :param current: The desired current
        :param target_output: The name of the output on the power supply (name is model dependent)
        """
        if not target_output:
            raise KeysightPowerSupplyMultiOutputError(self._DEVICE_MODEL)

        if self._is_valid_target(target_output):
            cmd_base = f"APPL {target_output},"  # Add , for correct cmd generation in helper function
            self._apply(cmd_base, target_output, voltage, current)
        else:
            raise KeysightPowerSupplyTargetOutputError(
                target_output, self.output_ranges
            )

    def get_output_settings(
        self, target_output: str = ""
    ) -> typing.Tuple[float, float]:
        """Returns the voltage and current values for the specified target.

        :param target_output: The target output
        :returns: A tuple of settings for the target output
        """
        if not target_output:
            raise KeysightPowerSupplyMultiOutputError(self._DEVICE_MODEL)

        if self._is_valid_target(target_output):
            cmd = f"APPL? {target_output}"
            return self._get_output_settings(cmd, target_output)
        else:
            raise KeysightPowerSupplyTargetOutputError(
                target_output, self.output_ranges
            )

    def get_output_measurements(
        self, target_output: str = ""
    ) -> typing.Tuple[float, float]:
        """Returns the voltage and current values measured on a specified target output.
        Target output must be specified for multi output devices.

        :param target_output: The target output
        :returns: A tuple of measured values (voltage, current) on the target output
        """
        if not target_output:
            raise KeysightPowerSupplyMultiOutputError(self._DEVICE_MODEL)

        if self._is_valid_target(target_output):
            return self._get_output_measurements(target_output, append=True)
        else:
            raise KeysightPowerSupplyTargetOutputError(
                target_output, self.output_ranges
            )
