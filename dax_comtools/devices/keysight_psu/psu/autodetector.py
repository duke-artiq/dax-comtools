# mypy: no_warn_unused_ignores

import typing
import logging
import pyvisa

from dax_comtools.devices.keysight_psu.psu.base import (
    KeysightPowerSupplyBase,
    KeysightPowerSupplyError,
)
from dax_comtools.devices.keysight_psu.psu.n6700c import KeysightPowerSupplyN6700C

logger = logging.getLogger(__name__)
"""The module logger."""


class KeysightPowerSupplyAutoDetector(KeysightPowerSupplyBase):
    """An implementation of the KeysightPowerSupplyBase that assists in the autodetection of the PSU model."""

    _OUTPUT_RANGES = {}  # type: ignore[var-annotated]

    _DEVICE_ID = "autodetector"

    _DEVICE_MODEL = "autodetector"

    _UNSUPPORTED_PSUS = {KeysightPowerSupplyN6700C._DEVICE_MODEL}
    """PSU models that are not supported for auto detection."""

    def _parse_model(self, model: str):
        """Parse model string to see if it is supported for auto detection."""

        if model.lower() in self._UNSUPPORTED_PSUS:
            raise KeysightPowerSupplyError(
                f"Model: {model} was detected. This model is currently unsuppored for autodetection.")

    def toggle_output(self, status: bool, target_output: str = "") -> None:
        """Sets the status of a specified output on the power supply.
        Multi-output models require a target_output to be sepecified.

        :param status: Enables output if True, disables if False
        :param target_output: Name of the target output on a multi output device
        """
        raise NotImplementedError

    def toggle_all_outputs(self, status: bool) -> None:
        """Sets the status of all outputs on the power supply.

        :param status: Enables outputs if True, disables if False
        """
        raise NotImplementedError

    def apply(
        self,
        voltage: float,
        current: typing.Optional[float] = None,
        target_output: typing.Optional[str] = "",
    ) -> None:
        """Sets the voltage and current on a target output of the power supply.
        Target output must be specified for multi output devices.

        :param voltage: The desired voltage
        :param current: The desired current
        :param target_output: The name of the output on the power supply (name is model dependent)
        """
        raise NotImplementedError

    def get_output_settings(
        self, target_output: str = ""
    ) -> typing.Tuple[float, float]:
        """Returns the voltage and current values for the specified target output.
        Target output must be specified for multi output devices.

        :param target_output: The target output
        :returns: A tuple of settings for the target output
        """
        raise NotImplementedError

    def get_output_measurements(
        self, target_output: str = ""
    ) -> typing.Tuple[float, float]:
        """Returns the voltage and current values measured on a specified target output.
        Target output must be specified for multi output devices.

        :param target_output: The target output
        :returns: A tuple of measured values (voltage, current) on the target output
        """
        raise NotImplementedError

    def check_connection(self) -> None:
        """Override of the base check_connection method. Implemented so that when checking connection in the init
        method nothing will go wrong.
        """
        pass

    def get_model_from_device(self) -> str:
        """Gets the device model from the Keysight PSU"""
        if self._simulation:
            return self.get_device_model()
        else:
            try:
                parts = self.get_id_from_device().split(",")
            # If can't establish a connection to the device, check if the connection is optional
            except pyvisa.errors.VisaIOError as error:
                # When in the specific case that the connection is optional, return the name of the Offline class
                if self._connection_optional:
                    logger.warning(
                        "No power supply is connected. Will be unable to handle power supply functions."
                    )
                    return "offline_psu"  # Offline psu model
                else:
                    raise error

            if len(parts) > 1:
                model = parts[1]
                self._parse_model(model)
                return model
            else:
                raise KeysightPowerSupplyError("Unable to parse device identifier.")
