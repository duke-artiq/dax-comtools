import typing
import logging

from dax_comtools.devices.keysight_psu.psu.base import (
    KeysightPowerSupplyError,
    KeysightPowerSupplyBase,
    KeysightPowerSupplyTargetOutputError,
    KeysightPowerSupplyMultiOutputError,
    OutputRanges,
)

logger = logging.getLogger(__name__)
"""The module logger."""


class AgilentPowerSupplyE3631A(KeysightPowerSupplyBase):
    """An implementation of the KeysightPowerSupplyBase for the Agilent E3631A PSU."""

    _OUTPUT_RANGES = {
        "P6V": OutputRanges(
            MIN_VOLTAGE=0.0, MAX_VOLTAGE=6.18, MIN_CURRENT=0.0, MAX_CURRENT=5.15
        ),
        "P25V": OutputRanges(
            MIN_VOLTAGE=0.0, MAX_VOLTAGE=25.75, MIN_CURRENT=0.0, MAX_CURRENT=1.03
        ),
        "N25V": OutputRanges(
            MIN_VOLTAGE=-27.75, MAX_VOLTAGE=0.0, MIN_CURRENT=0.0, MAX_CURRENT=1.03
        ),
    }
    """Dictionary that stores the E3631A power supply's available outputs and the min/max values for those outputs.
                Each output has a different min/max range for voltage and current."""

    _DEVICE_ID = "HEWLETT-PACKARD,E3631A,0,2.1-5.0-1.0\r"
    """The device identifier."""

    _DEVICE_MODEL = "e3631a"
    """The device model."""

    def set_target_output(self, target_output: str) -> None:
        """Sets the target output of a multi output power supply.

        :param target_output: The name of the output on the power supply (name is model dependent).
        """
        # Overrides base function to use correct command for the older model
        if self._is_valid_target(target_output):
            self._target_output = target_output
            if not self._simulation:
                self._write(f"APPL {target_output}")
        else:
            raise KeysightPowerSupplyTargetOutputError(
                target_output, self.output_ranges
            )

    def toggle_output(self, status: bool, target_output: str = "") -> None:
        """Sets the status of a specified output on the power supply.
        Multi-output models require a target_output to be sepecified.

        :param status: Enables output if True, disables if False
        :param target_output: Name of the target output on a multi output device
        """
        raise KeysightPowerSupplyError(
            "The E3631A model only supports toggling all outputs. Please use toggle_all_outputs()."
        )

    def toggle_all_outputs(self, status: bool) -> None:
        """Sets the status of all outputs on the power supply.

        :param status: Enables outputs if True, disables if False
        """
        assert isinstance(status, bool), "Status must be of type bool."
        status_str = "ON" if status else "OFF"
        self._write(f"OUTP:STAT {status_str}")

    def apply(
        self,
        voltage: float,
        current: typing.Optional[float] = None,
        target_output: typing.Optional[str] = "",
    ) -> None:
        """Sets the voltage and current on a target output of the power supply.
        Target output must be specified for multi output devices.

        :param voltage: The desired voltage
        :param current: The desired current
        :param target_output: The name of the output on the power supply (name is model dependent)
        """
        if not target_output:
            raise KeysightPowerSupplyMultiOutputError(self._DEVICE_MODEL)

        if self._is_valid_target(target_output):
            cmd_base = f"APPL {target_output},"  # Add , for correct cmd generation in helper function
            self._apply(cmd_base, target_output, voltage, current)
        else:
            raise KeysightPowerSupplyTargetOutputError(
                target_output, self.output_ranges
            )

    def get_output_settings(
        self, target_output: str = ""
    ) -> typing.Tuple[float, float]:
        """Returns the voltage and current values for the specified target.

        :param target_output: The target output
        :returns: A tuple of settings for the target output
        """
        if not target_output:
            raise KeysightPowerSupplyMultiOutputError(self._DEVICE_MODEL)

        if self._is_valid_target(target_output):
            cmd = f"APPL? {target_output}"
            return self._get_output_settings(cmd, target_output)
        else:
            raise KeysightPowerSupplyTargetOutputError(
                target_output, self.output_ranges
            )

    def get_output_measurements(
        self, target_output: str = ""
    ) -> typing.Tuple[float, float]:
        """Returns the voltage and current values measured on a specified target output.
        Target output must be specified for multi output devices.

        :param target_output: The target output
        :returns: A tuple of measured values (voltage, current) on the target output
        """
        if not target_output:
            raise KeysightPowerSupplyMultiOutputError(self._DEVICE_MODEL)

        if self._is_valid_target(target_output):
            return self._get_output_measurements(target_output, append=True)
        else:
            raise KeysightPowerSupplyTargetOutputError(
                target_output, self.output_ranges
            )
