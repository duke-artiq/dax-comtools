import typing
import logging
import abc

from dax_comtools.devices.keysight_psu.psu.base import (
    KeysightPowerSupplyBase,
    KeysightPowerSupplyError,
    KeysightPowerSupplyTargetOutputError,
    KeysightPowerSupplyMultiOutputError,
    OutputRanges,
)


logger = logging.getLogger(__name__)
"""The module logger."""


class N6731BOutputRanges(OutputRanges):
    """Implementation of :class:`OutputRanges` for the N6731B module."""

    def __init__(self):
        super().__init__(MIN_VOLTAGE=0.0, MAX_VOLTAGE=5.0, MIN_CURRENT=0.0, MAX_CURRENT=10.0)


class KeysightPowerSupplyN6700C(KeysightPowerSupplyBase, abc.ABC):
    """Generic implementation of the KeysightPowerSupplyBase for the Keysight N6700C modular PSU.

    Usage: This class should be implemented based on the modular configuration of the Keysight N6700C.
    """

    _DEVICE_ID = "Keysight Technologies,N6700C,MY56004854,E.01.01.2780"
    """The device identifier."""

    _DEVICE_MODEL = "n6700c"
    """ The device model."""

    _MODULE_MODELS: str
    """String to describe the configuration of modules in the device."""

    def _apply_separate(self, target: str, voltage: float, current: typing.Optional[float] = None):
        """Helper function for applying voltage and current on an output with separate commands.

        :param target: Target output to apply settings on.
        :param voltage: Voltage to be set on the given output.
        :param current: Current to be set on the given output.
        """
        output_ranges = self._OUTPUT_RANGES[target]

        # Check and voltage and current
        if not output_ranges.voltage_in_range(voltage):
            raise KeysightPowerSupplyError(
                f"Voltage of {voltage} is out of range. Value must be between "
                f"{output_ranges.MIN_VOLTAGE} and {output_ranges.MAX_VOLTAGE}."
            )

        if current is not None:
            if not output_ranges.current_in_range(current):
                raise KeysightPowerSupplyError(
                    f"Current of {current} is out of range. Value must be between "
                    f"{output_ranges.MIN_CURRENT} and {output_ranges.MAX_CURRENT}."
                )

        # Write commands to sim output or device
        if self._simulation:
            self._sim_outputs[target].apply(voltage, current)
        else:
            voltage_cmd = f"VOLT {voltage}, (@{target})"
            self._write(voltage_cmd)
            logger.debug(f"Voltage query: {voltage_cmd}")

            if current is not None:
                current_cmd = f"CURR {current}, (@{target})"
                self._write(current_cmd)
                logger.debug(f"Current query: {current_cmd}")

    def _query_separate(self, target: str, volt_query: str, curr_query: str) -> typing.Tuple[float, float]:
        """Helper function for sending voltage and current queries for an output with separate commands.

        :param target: Target output to apply settings on.
        :param volt_query: Query command for voltage.
        :param curr_query: Query commmand for current.
        """

        # Write commands to sim output or device
        if self._simulation:
            return self._sim_outputs[target].get_output_settings()
        else:
            voltage_cmd = f"{volt_query} (@{target})"
            current_cmd = f"{curr_query} (@{target})"
            voltage = self._query(voltage_cmd)
            current = self._query(current_cmd)
            logger.debug(f"Voltage query: {voltage_cmd}, Current query: {current_cmd}")
            return (float(voltage), float(current))

    def toggle_output(self, status: bool, target_output: str = "") -> None:
        """Sets the status of a specified output on the power supply.
        Multi-output models require a target_output to be sepecified.

        :param status: Enables output if True, disables if False
        :param target_output: Name of the target output on a multi output device
        """
        assert isinstance(status, bool), "Status must be of type bool."

        if not target_output:
            raise KeysightPowerSupplyMultiOutputError(self._DEVICE_MODEL)

        if self._is_valid_target(target_output):
            status_str = "ON" if status else "OFF"
            self._write(f"OUTP {status_str},(@{target_output})")
        else:
            raise KeysightPowerSupplyTargetOutputError(
                target_output, self.output_ranges
            )

    def toggle_all_outputs(self, status: bool) -> None:
        """Sets the status of all outputs on the power supply.

        :param status: Enables outputs if True, disables if False
        """
        assert isinstance(status, bool), "Status must be of type bool."
        status_str = "ON" if status else "OFF"
        self._write(f"OUTP {status_str},(@1:4)")

    def apply(
        self,
        voltage: float,
        current: typing.Optional[float] = None,
        target_output: typing.Optional[str] = "",
    ) -> None:
        """Sets the voltage and current on a target output of the power supply.
        Target output must be specified for multi output devices.

        :param voltage: The desired voltage
        :param current: The desired current
        :param target_output: The name of the output on the power supply (name is model dependent)
        """
        if not target_output:
            raise KeysightPowerSupplyMultiOutputError(self._DEVICE_MODEL)

        if self._is_valid_target(target_output):
            self._apply_separate(target_output, voltage, current)
        else:
            raise KeysightPowerSupplyTargetOutputError(
                target_output, self.output_ranges
            )

    def get_output_settings(
        self, target_output: str = ""
    ) -> typing.Tuple[float, float]:
        """Returns the voltage and current values for the specified target.

        :param target_output: The target output
        :returns: A tuple of settings for the target output
        """
        if not target_output:
            raise KeysightPowerSupplyMultiOutputError(self._DEVICE_MODEL)

        if self._is_valid_target(target_output):
            return self._query_separate(target_output, "VOLT?", "CURR?")
        else:
            raise KeysightPowerSupplyTargetOutputError(
                target_output, self.output_ranges
            )

    def get_output_measurements(
        self, target_output: str = ""
    ) -> typing.Tuple[float, float]:
        """Returns the voltage and current values measured on a specified target output.
        Target output must be specified for multi output devices.

        :param target_output: The target output
        :returns: A tuple of measured values (voltage, current) on the target output
        """
        if not target_output:
            raise KeysightPowerSupplyMultiOutputError(self._DEVICE_MODEL)

        if self._is_valid_target(target_output):
            return self._query_separate(target_output, "MEAS:VOLT?", "MEAS:CURR?")
        else:
            raise KeysightPowerSupplyTargetOutputError(
                target_output, self.output_ranges
            )


class KeysightPowerSupplyN6700C4xN6731B(KeysightPowerSupplyN6700C):
    """An implementation of the KeysightPowerSupplyN6700C for the modular Keysight N6700C PSU.

    ***NOTE***: The N6700C is a modular power supply model.
    This driver is configured to support a N6700C psu which contains 4 N6731B modules.

    """
    _OUTPUT_RANGES = {
        "1": N6731BOutputRanges(),
        "2": N6731BOutputRanges(),
        "3": N6731BOutputRanges(),
        "4": N6731BOutputRanges(),
    }

    _MODULE_MODELS: str = "4x n6731b"

    def get_model_from_device(self) -> str:
        """Gets the device model from the Keysight PSU.

        NOTE: Overridden to append module models to the device model.
        """
        model = super(KeysightPowerSupplyN6700C4xN6731B, self).get_model_from_device()
        return f"{model}, {self._MODULE_MODELS.upper()}"

    def _connected(self) -> bool:
        """Check the connection, return boolean if the correct device is connected or not.

        NOTE: Overridden for modules in device model.
        """
        connected = True
        try:
            model = self.get_model_from_device()
            # Grab device model for check, ignore appended modules
            if model.lower().split(",")[0] != self.get_device_model().lower():
                connected = False
        except ConnectionError:
            connected = False
        finally:
            return connected
