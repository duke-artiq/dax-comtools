import logging
import pyvisa
import typing
import time
from enum import Enum
import dataclasses

__all__ = ['Agilent3320AAWG', 'Agilent3320AFunction', 'Agilent3320ATrigger']

logger = logging.getLogger(__name__)
"""The module logger."""


class Agilent3320AAWGError(RuntimeError):
    pass


class Agilent3320ATrigger(Enum):
    """Enum that lists the available triggers that can be used on the AWG."""
    EXTERNAL = 'EXT'
    IMMEDIATE = 'IMM'
    BUS = 'BUS'

    def __str__(self) -> str:
        return self.value


class Agilent3320AFunction(Enum):
    """This limited interface can generate sinusoid, square and ramp functions.

    The Agilent AWG supports additional functions which can be added to this interface at a later time, if needed.
    """
    SINUSOID = 'SIN'
    SQUARE = 'SQU'
    RAMP = 'RAMP'

    def __str__(self) -> str:
        return self.value


@dataclasses.dataclass(frozen=True)
class _FunctionRanges:
    """Dataclass to store output ranges."""

    min_frequency: float
    max_frequency: float
    min_amplitude: float
    max_amplitude: float
    min_dcycle: typing.Optional[float] = None
    max_dcycle: typing.Optional[float] = None
    min_symmetry: typing.Optional[float] = None
    max_symmetry: typing.Optional[float] = None

    def check_frequency(self, frequency: float) -> None:
        """Raises exception if frequency is ouf range."""

        if not self.min_frequency <= frequency <= self.max_frequency:
            raise Agilent3320AAWGError('Given frequency is out of range.')

    def check_amplitude(self, amplitude: float) -> None:
        """Raises exception if amplitude is out of range."""
        if not self.min_amplitude <= amplitude <= self.max_amplitude:
            raise Agilent3320AAWGError('Given amplitude is out of range.')

    def check_dcycle(self, dcycle: float) -> None:
        if self.min_dcycle is None or self.max_dcycle is None:
            raise TypeError('This function range can not be checked for duty cycle')

        """Raise exception if duty cycle is out of range."""
        if not self.min_dcycle <= dcycle <= self.max_dcycle:
            raise Agilent3320AAWGError('Given duty cycle is out of range.')

    def check_symmetry(self, symmetry: float) -> None:
        """Raises exception if symmetry is out of range."""
        if self.min_symmetry is None or self.max_symmetry is None:
            raise TypeError('This function range can not be checked for symmetry')

        if not self.min_symmetry <= symmetry <= self.max_symmetry:
            raise Agilent3320AAWGError('Given symmetry is out of range.')


class Agilent3320AAWG:
    _FUNCTION_RANGES: typing.Dict[Agilent3320AFunction, _FunctionRanges] = {
        Agilent3320AFunction.SINUSOID: _FunctionRanges(min_frequency=10.0e-6, max_frequency=20.0 * 10.0e6,
                                                       min_amplitude=10.0e-3, max_amplitude=10.0),
        Agilent3320AFunction.SQUARE: _FunctionRanges(min_frequency=10.0e-6, max_frequency=20.0 * 10.0e6,
                                                     min_amplitude=10.0e-3, max_amplitude=10.0, min_dcycle=0.2,
                                                     max_dcycle=0.8),
        Agilent3320AFunction.RAMP: _FunctionRanges(min_frequency=10.0e-6, max_frequency=200.0 * 10.0e3,
                                                   min_amplitude=10.0e-3, max_amplitude=10.0, min_symmetry=0.0,
                                                   max_symmetry=1.0)
    }
    """Dictionary that stores the arbitrary waveform generator's available functions and the min/max values
    for those functions. Each function has a different min/max range for frequency, amplitude, duty cycle
    (where applicable) and symmetry (where applicable).

    The min/max amplitude values are in VPP and are dependent on the load impedance.
    These values are the extreme min/max values. See device documentation for specifics related to load impedance.

    The min/max duty cycle values for the square function vary based on frequency.
    The values above are the extreme min/max value.
    Device specifications should be checked for valid duty cycles per frequency range."""

    _DEVICE_ID: str = 'Agilent Technologies,33220A,MY44023364,2.02-2.02-22-2'
    """The device identifier."""

    _CONN_READ_TERMINATION = '\n'
    """Read termination character for serial communication to AWG"""
    _CONN_WRITE_TERMINATION = '\n'
    """Write termination character for serial communication to AWG"""

    _MIN_BURST_COUNT = 1
    _MAX_BURST_COUNT = 50000
    """Min/Max burst rate for the device. The selected function does not impact these values."""

    def __init__(self, device: str, *, connection_optional: bool = False, simulation: bool = False):
        """Construct a new driver object.

        :param device: serial port device is connected to
        :param simulation: flag to run in simulation mode (dump all received messages to stdout)
        :param connection_optional: if True an exception will be thrown if the awg is not connected and reachable
        """
        assert isinstance(device, str)
        assert isinstance(connection_optional, bool)
        assert isinstance(simulation, bool)

        # Store attributes
        self._device: str = device
        self._simulation: bool = simulation
        self._connection_optional: bool = connection_optional
        self._offline: bool = False

        if not self._simulation:
            # Set up connection
            logger.debug(f'Setting up connection on serial device "{self._device}"')
            self._conn: typing.Any = pyvisa.ResourceManager().open_resource(self._device)  # Typed as any
            # Set device mode and verify connection
            self._conn.read_termination = self._CONN_READ_TERMINATION
            self._conn.write_termination = self._CONN_WRITE_TERMINATION

            # check if the device is connected (this will set offline to True if it is not connected)
            self.check_connection()

            # if the device is connected then enable remote mode
            if not self._offline:
                time.sleep(1.0)
                self._enable_remote_mode()

        else:
            # Create simulation variables
            logger.info('Simulation enabled')
            self._target_output: str = 'P6V'
            self._voltage: float = 0.0
            self._current: typing.Optional[float] = None

    # noinspection PyMethodMayBeStatic
    def ping(self) -> bool:
        """Always returns true (indication that the driver is running)."""
        return True

    def _enable_remote_mode(self) -> None:
        """Configure the AWG to be in `REMOTE` mode

        Sending or receiving data to the device when not configured for remote operation
        can cause unpredictable results.
        """
        self._write("SYST:REM")

    def get_device_id(self) -> str:
        """Gets the device ID from the Agilent AWG"""

        if self._simulation:
            return self._DEVICE_ID
        else:
            return self._query('*IDN?')

    def check_connection(self) -> None:
        """Check the connection, raises an exception in case of problems.

        :raises Agilent3320AAWGError: Raised in case of a device ID mismatch
        """
        try:
            if self.get_device_id() != self._DEVICE_ID:
                # if the device ID of from the device does not match the expected value set offline
                self._offline = True
        except ConnectionError:
            # if an exception is raised when trying to connect to the device then set offline
            self._offline = True
        finally:
            # if offline for any reason show warning or raise error (based on --connection-optional flag)
            if self._offline:
                # if offline then close the connection
                self.close()
                if self._connection_optional:
                    logger.warning('Device is either unavailable or an unrecognized device is connected.')
                else:
                    raise Agilent3320AAWGError('Device is either unavailable or an unrecognized device is connected.')

    def _write(self, message: str) -> None:
        """Wrapper code for write functionality to take into account simulations."""
        logger.debug(f'Write: {message}')
        if not self._simulation:
            if self._offline:
                raise Agilent3320AAWGError(f'Device is offline. Cannot write message "{message}".')
            else:
                self._conn.write(message)

    def _query(self, message: str) -> str:
        """Wrapper code for query functionality to take into account simulations."""
        logger.debug(f'Query: {message}')
        if not self._simulation:
            if self._offline:
                raise Agilent3320AAWGError(f'Device is offline. Cannot send query with message "{message}"')
            else:
                return self._conn.query(message)
        else:
            raise RuntimeError(f'Reached a query in simulation mode with message "{message}"')

    def config_trigger(self, trigger: Agilent3320ATrigger) -> None:
        """Configures the AWG to use the specified trigger. The trigger causes the AWG to output a signal.

        :param trigger: the trigger type that will cause the AWG to output a signal.
        """
        self._write(f'TRIG:SOUR {trigger}')

    def config_burst(self, burst_count: int = 1, is_enabled: bool = True) -> None:
        """Configures the burst settings on the AWG.

        :param burst_count: the number of bursts that will be output when the AWG is triggered.
        :param is_enabled: turns burst mode on/off.
        """
        status = 'ON' if is_enabled else 'OFF'

        # only set burst count if burst is enabled
        if is_enabled:
            assert self._MIN_BURST_COUNT <= burst_count <= self._MAX_BURST_COUNT
            self._write(f'BURS:NCYC {burst_count}')

        self._write(f'BURS:STAT {status}')

    def config_sinusoid(self, frequency: float, amplitude: float) -> None:
        """Configures the sinusoid signal settings on the AWG.

        :param frequency: the frequency of the sinusoidal signal.
        :param amplitude: the amplitude of the sinusoidal signal.
        """
        func = Agilent3320AFunction.SINUSOID
        func_range = self._FUNCTION_RANGES[func]

        # set the frequency and amplitude for the sinusoid signal
        self._config_function(func, func_range, frequency, amplitude)

    def config_square(self, frequency: float, amplitude: float, dcycle: float) -> None:
        """Configures the square signal settings on the AWG

        :param frequency: the frequency of the square signal.
        :param amplitude: the amplitude of the square signal.
        :param dcycle: the duty cycle of the square signal.
        """
        func = Agilent3320AFunction.SQUARE
        func_range = self._FUNCTION_RANGES[func]

        # validate the duty cycle value
        func_range.check_dcycle(dcycle)

        # set the frequency and amplitude for the square signal
        self._config_function(func, func_range, frequency, amplitude)

        # set the duty cycle
        self._write(f'FUNC:SQU:DCYCLE {dcycle}')

    def config_ramp(self, frequency: float, amplitude: float, symmetry: float) -> None:
        """Configures the ramp signal settings on the AWG.

        :param frequency: the frequency of the ramp signal.
        :param amplitude: the amplitude of the ramp signal.
        :param symmetry: the symmetry of the ramp signal. The symmetry represents the amount of time per cycle that
        the ramp wave is rising (assuming it is not inverted).
        """

        func = Agilent3320AFunction.RAMP
        func_range = self._FUNCTION_RANGES[func]

        # validate the symmetry value
        func_range.check_symmetry(symmetry)

        # set the frequency and amplitude for the ramp signal
        self._config_function(func, func_range, frequency, amplitude)

        # set the symmetry value
        self._write(f'FUNC:RAMP:SYMM {symmetry}')

    def _config_function(self, func: Agilent3320AFunction, func_range: _FunctionRanges, frequency: float,
                         amplitude: float) -> None:
        """Configures the AWG to use the given function, frequency and amplitude.

        :param func: the function to configure on the AWG. See Agilent3320AFunction enum for supported types.
        :param func_range: object that contains range values and validation functions for specified func.
        :param frequency: the frequency of the signal defined by func.
        :param amplitude: the amplitude of the signal defined by func.
        """

        # validate the frequency and amplitude values
        func_range.check_frequency(frequency)
        func_range.check_amplitude(amplitude)

        # set the frequency, amplitude and function
        self._config_frequency(frequency)
        self._config_amplitude(amplitude)
        self._write(f'FUNC {func}')

    def _config_frequency(self, frequency: float):
        """Writes the frequency value to the AWG.

        :param frequency: the frquency value to write to the AWG.
        """
        self._write(f'FREQ {frequency}')

    def _config_amplitude(self, amplitude: float):
        """Writes the amplitude value to the AWG.

        :param amplitude: the amplitude value to write to the AWG.
        """
        self._write(f'VOLT {amplitude}')

    def close(self) -> None:
        """Close serial connection."""
        logger.debug('Closing serial connection')
        if not self._simulation:
            self._conn.close()
