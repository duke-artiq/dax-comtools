"""
This module implements the I2C bus device.
https://circuitpython.readthedocs.io/projects/busdevice/en/latest/api.html
"""

from __future__ import annotations

import os
import fcntl
import typing

__all__ = ['I2CDevice']


class I2CDevice:
    I2C_TARGET: typing.ClassVar[int] = 0x0703
    """I2C target (from /linux/i2c-dev.h)."""

    def __init__(self, i2c: str, device_address: int, probe: bool = True):
        """Initialize a I2C device.

        :param i2c: Path to the I2C bus (e.g. ``"/dev/i2c-1"``)
        :param device_address: I2C address of the device
        :param probe: Probe for the device upon object creation (ignored)
        """

        # Store the I2C device path and the I2C address
        self._i2c = i2c
        self._address = device_address

        # Reference to file descriptor
        self._fd: typing.Optional[int] = None

        if probe:
            self.__probe_for_device()

    def _open(self) -> None:
        """Open the i2c bus in read/write mode as a file object"""
        if self._fd is None:
            self._fd = os.open(self._i2c, os.O_RDWR)
            fcntl.ioctl(self._fd, self.I2C_TARGET, self._address)
        else:
            raise RuntimeError('I2C bus was already opened')

    def _close(self) -> None:
        """Close the file object."""
        if self._fd is None:
            raise RuntimeError('I2C bus is not open')
        else:
            os.close(self._fd)
            self._fd = None

    def readinto(self, buf: bytearray, *, start: int = 0, end: typing.Optional[int] = None) -> int:
        if self._fd is None:
            raise RuntimeError('I2C bus is not open')
        else:
            if start != 0 or end is not None:
                raise NotImplementedError('Parameters start and end are not implemented')
            return os.readv(self._fd, [buf])

    def write(self, data: bytes, *, start: int = 0, end: typing.Optional[int] = None) -> int:
        if self._fd is None:
            raise RuntimeError('I2C bus is not open')
        else:
            if start != 0 or end is not None:
                data = data[start:len(data) if end is None else end]
            return os.write(self._fd, data)

    def write_then_readinto(self, out_buffer: bytes, in_buffer: bytearray, *,
                            out_start: int = 0, out_end: typing.Optional[int] = None,
                            in_start: int = 0, in_end: typing.Optional[int] = None) -> None:
        self.write(out_buffer, start=out_start, end=out_end)
        self.readinto(in_buffer, start=in_start, end=in_end)

    def __enter__(self) -> I2CDevice:
        self._open()
        return self

    def __exit__(self, exc_type: typing.Any, exc_val: typing.Any, exc_tb: typing.Any) -> typing.Any:
        self._close()
        return False

    def __probe_for_device(self) -> None:
        pass
