# Adafruit CircuitPython implementation

This package contains pure-Python implementations of some Adafruit CircuitPython API's. Similar to Adafruit's
own [Blinka library](https://github.com/adafruit/Adafruit_Blinka), but much lighter and more limited. With these
CircuitPython API implementations we can use the CircuitPython drivers for Adafruit sensors on a Raspberry Pi with
minimal modifications.
