"""Convenience functions for arguments commonly used argument groups using ``ArgumentParser``."""

import argparse
import logging
import typing

__all__ = [
    'str_to_hex',
    'influx_args', 'get_influx_args',
    'monitoring_tags_args', 'get_monitoring_tags_args',
    'logger_args', 'process_logger_args'
]


def str_to_hex(v: str) -> int:
    """Convert a string in hex notation to an integer."""
    v = v.lower()
    if v.startswith('0x'):
        v = v[2:]
    return int(v, 16)


def influx_args(parser: argparse.ArgumentParser) -> None:
    """Add connection arguments for InfluxDB."""
    idb = parser.add_argument_group("InfluxDB arguments")
    idb.add_argument("--db", type=str, default="dax_data_store",
                     help="Database name (default: %(default)s)")
    idb.add_argument("--server-db", type=str, default="localhost",
                     help="IP address of InfluxDB server (default: %(default)s)")
    idb.add_argument("--port-db", type=int, default=8086,
                     help="Port for InfluxDB HTTP Server (default: %(default)s)")
    idb.add_argument("--user-db", type=str, default="root",
                     help="User name for InfluxDB (default: %(default)s)")
    idb.add_argument("--password-db", type=str, default="root",
                     help="Password for InfluxDB (default: %(default)s)")


def get_influx_args(args: argparse.Namespace) -> typing.Dict[str, typing.Any]:
    """Extract InfluxDB arguments.

    :param args: The parsed arguments
    :return: The InfluxDB arguments as a dict
    """
    return {k: getattr(args, k) for k in ['db', 'server_db', 'port_db', 'user_db', 'password_db']}


def monitoring_tags_args(parser: argparse.ArgumentParser, *,
                         source: typing.Optional[str] = None,
                         device_type: typing.Optional[str] = None,
                         building: typing.Optional[str] = None,
                         room: typing.Optional[str] = None,
                         required: bool = False) -> None:
    """Add arguments for tags used in the 'monitoring' measurement in InfluxDB.

    :param parser: The argument parser
    :param source: Default value for source
    :param device_type: Default value for device type
    :param building: Default value for building
    :param room: Default value for room
    :param required: Make arguments required if no default is set
    """
    tags: typing.Dict[str, typing.Tuple[typing.Optional[str], str]] = {
        '--source-tag': (source, 'Fine grained source of information'),
        '--device-type-tag': (device_type, 'Type of the device'),
        '--building-tag': (building, 'Building of source'),
        '--room-tag': (room, 'Room inside building'),
    }

    group = parser.add_argument_group("InfluxDB monitoring measurement tags")

    for name, (default, help_) in tags.items():
        kwargs: typing.Dict[str, typing.Any] = {
            'type': str,
            'required': required and default is None,
            'help': help_ if default is None else f'{help_} (default: %(default)s)',
        }
        if default is not None:
            kwargs['default'] = default
        group.add_argument(name, **kwargs)

    group.add_argument('--system-tag', type=str, default=None, help='Identifier for system/setup')


def get_monitoring_tags_args(args: argparse.Namespace) -> typing.Dict[str, typing.Any]:
    """Extract monitoring tags arguments.

    :param args: The parsed arguments
    :return: The arguments as a dict
    """
    keys = ['source', 'device_type', 'building', 'room', 'system']
    tags = (f'{k}_tag' for k in keys)
    return {k: getattr(args, t) for k, t in zip(keys, tags) if t in args}


def logger_args(parser: argparse.ArgumentParser) -> None:
    """Add arguments for logging verbosity, also see :func:`process_logger_args`."""
    log = parser.add_argument_group('Logging')
    log.add_argument('-q', '--quiet', default=0, action='count', help='Decrease verbosity')
    log.add_argument('-v', '--verbose', default=0, action='count', help='Increase verbosity')


def process_logger_args(args: argparse.Namespace, *,
                        basic_config: bool = True,
                        default_level: int = logging.WARNING) -> None:
    """Process the logger arguments.

    :param args: The parsed arguments
    :param basic_config: Initialize the logging module by calling `basicConfig()`
    :param default_level: The default logging level
    """
    assert isinstance(default_level, int), 'Default level must be of type int'

    if basic_config:
        logging.basicConfig()

    root_logger = logging.getLogger()
    root_logger.setLevel(default_level + logging.DEBUG * (args.quiet - args.verbose))
