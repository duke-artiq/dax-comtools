from __future__ import annotations

import influxdb
import influxdb.exceptions
import requests.exceptions
import logging
import typing
import time

__all__ = ['InfluxDBUtil']

logger = logging.getLogger(__name__)

_FV_T = typing.Union[int, float, str, bool, None]  # Field value type (None is automatically ignored by influxdb)
_F_T = typing.Dict[str, _FV_T]  # Field type
_T_T = typing.Dict[str, str]  # Tags type
_P_T = typing.Dict[str, typing.Union[str, int, _T_T, _F_T]]  # Point type


class InfluxDBUtil:
    """Utility class for controllers to write to InfluxDB."""

    def __init__(self, db: str = 'dax_data_store', server_db: str = 'localhost', port_db: int = 8086,
                 user_db: str = 'root', password_db: str = 'root', *,
                 buffer_size: int = 0,
                 time_precision: typing.Optional[str] = None,
                 timeout: typing.Optional[float] = 10.0,
                 **kwargs):
        """Create a new InfluxDB util class.

        :param db: name of the InfluxDB database to write to
        :param server_db: IP address of the InfluxDB server
        :param port_db: port the InfluxDB server is listening on
        :param user_db: username for database login
        :param password_db: password for the database login
        :param buffer_size: buffer size for points (no buffering if `buffer_size<=1`)
        :param time_precision: either ‘s’, ‘m’, ‘ms’ or ‘u’. Defaults to None.
        :param timeout: Connection timeout in seconds or :const:`None` for no timeout
        :param kwargs: Other keyword arguments passed to the `InfluxDBClient`
        """
        assert isinstance(db, str)
        assert isinstance(server_db, str)
        assert isinstance(port_db, int)
        assert isinstance(user_db, str)
        assert isinstance(password_db, str)
        assert isinstance(buffer_size, int)
        assert time_precision is None or time_precision in {'s', 'm', 'ms', 'u'}
        assert timeout is None or isinstance(timeout, float)

        # Create the InfluxDB client object
        self._client: influxdb.InfluxDBClient
        self._connect(host=server_db, port=port_db, database=db, username=user_db, password=password_db,
                      timeout=timeout, **kwargs)
        # Ping the client to detect early connection problems
        self._client.ping()
        logger.info('InfluxDB client connected to database %s on [%s]:%i', db, server_db, port_db)

        # Create the buffer and store the buffer size
        self._buffer: typing.List[_P_T] = []
        self._buffer_size: int = buffer_size
        # Store the time precision
        self._time_precision: typing.Optional[str] = time_precision

    def _connect(self, host: str, port: int, database: str, username: str, password: str,
                 timeout: typing.Optional[float], **kwargs: typing.Any) -> None:
        """Connect to the database."""
        self._client = influxdb.InfluxDBClient(host=host, port=port, database=database,
                                               username=username, password=password, timeout=timeout, **kwargs)

    def write_point_monitoring(self, source: str, device_type: str, building: str, room: str,
                               system: typing.Optional[str] = None, timestamp: typing.Union[str, int, None] = None,
                               *,
                               fields: typing.Optional[_F_T] = None,
                               **kwargs: _FV_T) -> None:
        """Wrapper function for writing a single point to the 'monitoring' table in InfluxDB.

        :param source: fine-grained source of information (e.g. `table_sensor_3`, `bottom_pressure_sensor`)
        :param device_type: type of the device (e.g. `rpi_v3b`)
        :param building: building of source (e.g. `chesterfield`)
        :param room: room inside building (e.g. `suite_400`)
        :param system: identifier for system/setup (e.g. `staq`, `red_chamber`, or empty for 'room wide sensors')
        :param timestamp: timestamp to use. If not provided, InfluxDB will automatically create one at write time.
            Note: InfluxDB uses UTC time by default, so make sure provided timestamps are UTC.
            Example code to fetch a valid InfluxDB timestamp (in UTC):
            ``time.time_ns()`` or ``datetime.datetime.utcnow().isoformat()``.
        :param fields: fields (i.e. the actual data) to write to InfluxDB
        :param kwargs: fields (i.e. the actual data) to write to InfluxDB (updates `fields`)
        """
        assert isinstance(source, str)
        assert isinstance(device_type, str)
        assert isinstance(building, str)
        assert isinstance(room, str)
        assert system is None or isinstance(system, str)
        assert timestamp is None or isinstance(timestamp, (str, int))

        if fields is None:
            # Set default value for fields
            fields = {}
        else:
            assert isinstance(fields, dict), 'Fields must be of type dict'
            assert all(isinstance(k, str) for k in fields), 'Keys of the fields dict must be of type str'
            fields = fields.copy()  # Copy arguments to make sure the dict is not mutated

        # Merge fields
        fields.update(kwargs)

        if not fields:
            return  # No fields to write

        # Assemble tags
        tags: _T_T = {
            'source': source,
            'device_type': device_type,
            'building': building,
            'room': room
        }
        if system:
            tags['system'] = system

        # Assemble point
        point: _P_T = {
            'measurement': 'monitoring',
            'tags': tags,
            'fields': fields
        }
        if timestamp is not None:
            # Add the given timestamp
            point['time'] = timestamp
        elif self._buffer_size > 1:
            # Buffering enabled, construct a timestamp
            point['time'] = time.time_ns()

        # Add the point to the buffer
        self._buffer.append(point)
        if len(self._buffer) >= self._buffer_size:
            # Flush
            self.flush()

    def flush(self) -> None:
        """Flush the buffer."""
        if self._buffer:
            try:
                # Write points
                self._client.write_points(self._buffer, time_precision=self._time_precision)
            except (influxdb.exceptions.InfluxDBClientError, influxdb.exceptions.InfluxDBServerError) as e:
                logger.exception('Influx error: %s', e)
            except requests.exceptions.ConnectionError as e:
                logger.exception('Connection error: %s', e)
            except Exception as e:
                logger.exception('Unknown error: %s', e)
            else:
                # Clear the buffer
                logger.debug('Wrote %d point(s) to InfluxDB', len(self._buffer))
            finally:
                # Data will also be dropped if the connection fails, preventing backlogs and bad timestamps
                self._buffer.clear()

    def query_monitoring(self, source: typing.Optional[str] = None, device_type: typing.Optional[str] = None,
                         building: typing.Optional[str] = None, room: typing.Optional[str] = None,
                         system: typing.Optional[str] = None, limit: typing.Optional[int] = None) -> list:
        """Query points from the monitoring table, with optional filtering by tags.

        :param source: fine-grained source of information
        :param device_type: type of the device
        :param building: building of source
        :param room: room inside building
        :param system: identifier for system/setup
        :param limit: maximum number of points to return
        :return: a list of points, where each point is a dictionary containing the corresponding tags and fields
        """
        # set up bind params for where clause
        bind_params = {'source': source, 'device_type': device_type, 'building': building, 'room': room,
                       'system': system}

        # set up query
        query = ['SELECT * FROM monitoring']
        # where clause
        if any(bind_params.values()):
            query.append(f'WHERE {" AND ".join(f"{k}=${k}" for k, v in bind_params.items() if v is not None)}')
        # limit
        if limit is not None:
            query.append(f'LIMIT {limit:d}')

        rs = self._client.query(' '.join(query), bind_params=bind_params)
        return [p for p in rs.get_points()]

    def close(self) -> None:
        """Flush and close connection to InfluxDB."""
        self.flush()
        self._client.close()
        logger.info('Connection to InfluxDB closed')

    def __enter__(self) -> InfluxDBUtil:
        return self

    def __exit__(self, exc_type: typing.Any, exc_val: typing.Any, exc_tb: typing.Any) -> None:
        self.close()
