import threading
import abc
import typing
import time

__all__ = ['RecurringTimer', 'RecurringTaskBase']


class RecurringTimer:
    """The recurring timer calls the given function periodically until it is stopped.

    Once :func:`stop` is called, this timer cannot be run again.

    In case a periodic call is missed due to a long running task,
    the timer will drop the call and wait for the next period.
    """

    def __init__(self, func: typing.Callable[[], None], interval: float, *, start: bool = True, daemon: bool = False):
        """Initialize a new recurring timer.

        :param func: The function to periodically call
        :param interval: The interval between function calls in seconds
        :param start: Start the timer immediately if `True`
        :param daemon: Start the thread as a daemon
        """

        assert callable(func)
        assert isinstance(interval, float)

        # Store attributes
        self._func = func
        self._interval = interval

        # Create an event and a thread
        self._event = threading.Event()
        self._thread = threading.Thread(target=self._run, daemon=daemon)

        if start:
            # Start the thread
            self._thread.start()

    def _run(self) -> None:
        # Capture the start time to lock the timer
        start_time: float = time.time()
        # Loop for recurring function call
        while not self._event.wait(self._interval - ((time.time() - start_time) % self._interval)):
            self._func()

    def start(self) -> None:
        """Start the timer.

        :raise RuntimeError: Raised if the timer is started more than once.
        """
        self._thread.start()

    def stop(self) -> None:
        """Stop the timer.

        This function can be called multiple times without problems.
        """
        if self.is_alive():
            self._event.set()
            self._thread.join()

    def is_alive(self) -> bool:
        """Check the timer status.

        :return: True if timer is still running (never called stop), False if timer has stopped.
        """
        return self._thread.is_alive()


class RecurringTaskBase(abc.ABC):
    """Base class for a recurring tasks with fixed interval.

    Override the :func:`task` method with the desired functionality.
    """

    def __init__(self, interval: float, *, start: bool = True, daemon: bool = False):
        """Initialize a Recurring Task.

        :param interval: The interval between function calls in seconds
        :param start: Start the timer immediately if `True`
        :param daemon: Start the thread as a daemon
        """
        assert isinstance(start, bool)

        # The timer attribute
        self._timer: typing.Optional[RecurringTimer] = None

        # Process parameters
        self.set_interval(interval)
        self.set_daemon(daemon)
        if start:
            self.start()

    @abc.abstractmethod
    def task(self) -> None:
        """Override this function with the desired periodic functionality."""
        pass

    def set_interval(self, interval: float) -> None:
        assert isinstance(interval, float) and interval > 0.0
        self._recurring_interval = interval

    def get_interval(self) -> float:
        return self._recurring_interval

    def set_daemon(self, daemon: bool) -> None:
        assert isinstance(daemon, bool)
        self._daemon = daemon

    def get_daemon(self) -> float:
        return self._daemon

    def start(self) -> None:
        """Start the recurring task."""
        if self._timer is None or not self._timer.is_alive():
            self._timer = RecurringTimer(self.task, self._recurring_interval, daemon=self._daemon)

    def stop(self) -> None:
        """Stop the recurring task."""
        if self._timer is not None and self._timer.is_alive():
            self._timer.stop()

    def is_running(self) -> bool:
        if self._timer is None:
            return False
        else:
            return self._timer.is_alive()
