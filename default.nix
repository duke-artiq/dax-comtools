{ pkgs ? import <nixpkgs> { }
, artiqpkgs ? import <artiq-full> { inherit pkgs; }
, pymoku ? pkgs.python3Packages.callPackage ./vendor/pymoku/default.nix { }
}:

pkgs.python3Packages.callPackage ./derivation.nix { inherit pymoku; inherit (artiqpkgs) sipyco; }
