# pymoku

pymoku is software originally published by Liquid Instruments with an MIT license:

1. GitHub: <https://github.com/liquidinstruments/pymoku>
1. PyPi: <https://pypi.org/project/pymoku/>
1. Docs: <https://pymoku.readthedocs.io/en/2.8.3/>

End of 2024, Liquid Instruments has removed this software from public channels.
In September, the [GitHub project got removed](https://gitlab.com/duke-artiq/dax-comtools/-/issues/47).
In November, the [PiPy project got removed](https://gitlab.com/duke-artiq/dax-comtools/-/issues/48).

When the GitHub project got removed, we archived the pymoku 2.8.3 tarball from PyPi just to be sure.
With the lack of public sources, we have now added that tarball here as part of this vendor package.

```
SHA256: 5791041054b236d2210d1d099da9973402c092755d8f2d2eaf136458617b34f4
MD5: 700ab13e88e958777a44c886aa5a37ac
BLAKE2b-256: ec12e2333abb86a1bdbaa0633dfd621347639a0a124e1c488f63fd43f76aa5e8
```
