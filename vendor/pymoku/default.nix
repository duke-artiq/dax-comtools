{ buildPythonPackage
, stdenv
, future
, pyzmq
, requests
}:

let
  version = "2.8.3";
  pymoku-src = stdenv.mkDerivation {
    pname = "pymoku-src";
    inherit version;

    src = ./. + "/pymoku-${version}.tar.gz";

    phases = [ "installPhase" ];

    installPhase = ''
      mkdir $out && cd $out
      tar -xzf $src
    '';
  };
in
buildPythonPackage {
  pname = "pymoku";
  inherit version;

  src = "${pymoku-src}/pymoku-${version}";

  doCheck = false; # relies on DNSSD library

  propagatedBuildInputs = [
    future
    pyzmq
    requests
  ];

  condaDependencies = [
    "python>=3.6"
    "future"
    "pyzmq>=15.3.0"
    "requests>=2.18.0"
  ];
}
