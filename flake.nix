{
  inputs = {
    nixpkgs.url = github:NixOS/nixpkgs/nixos-22.05;
    flake-utils.url = "github:numtide/flake-utils";
    sipyco = {
      url = github:m-labs/sipyco;
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, flake-utils, sipyco }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
        pymoku = pkgs.python3Packages.callPackage ./vendor/pymoku/default.nix { };
        version = "${builtins.toString self.sourceInfo.lastModifiedDate or 0}+${self.sourceInfo.shortRev or "unknown"}";
        dax-comtools = pkgs.python3Packages.callPackage ./derivation.nix {
          inherit pymoku version;
          inherit (sipyco.packages.${system}) sipyco;
        };

        # sipyco tests fail in GitLab CI because of docker, make an alternative package just for the CI devshell
        dax-comtools-ci = pkgs.python3Packages.callPackage ./derivation.nix {
          inherit pymoku version;
          sipyco = sipyco.packages.${system}.sipyco.overridePythonAttrs (_: { doCheck = false; });
        };

        # Function to make a devshell for testing
        mkDevShell = daxComtoolsPkg: pkgs.mkShell {
          packages = [
            (pkgs.python3.withPackages (ps: [
              # Packages required for testing
              ps.pytest
              ps.mypy
              ps.coverage
              ps.flake8
              ps.types-requests
            ] ++ daxComtoolsPkg.propagatedBuildInputs))
          ];
        };

      in
      {
        packages = {
          inherit dax-comtools dax-comtools-ci pymoku;
          default = dax-comtools;
        };

        devShells = {
          default = mkDevShell dax-comtools;
          ci = mkDevShell dax-comtools-ci;
        };

        # enables use of `nix fmt`
        formatter = pkgs.nixpkgs-fmt;
      }
    );

  nixConfig = {
    extra-trusted-public-keys = [
      "nixbld.m-labs.hk-1:5aSRVA5b320xbNvu30tqxVPXpld73bhtOeH6uAjRyHc="
    ];
    extra-substituters = [ "https://nixbld.m-labs.hk" ];
  };
}
