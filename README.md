# DAX comtools

Lightweight DAX communication tools.

This repository contains controllers and standalone tools for [DAX](https://gitlab.com/duke-artiq/dax)
and [ARTIQ](https://github.com/m-labs/artiq). More information can be found in
the [DAX comtools wiki](https://gitlab.com/duke-artiq/dax-comtools/-/wikis/home).

## Installation

The DAX comtools package requires Python 3.7 or newer.

### Nix Flakes

When using Nix Flakes, a shell with the DAX comtools package can be directly opened from the terminal.

```shell
nix shell gitlab:duke-artiq/dax-comtools
```

The repository can also be added as an input in your own Nix Flake.

```nix
{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-22.05";
    dax-comtools = {
      url = "gitlab:duke-artiq/dax-comtools";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = {
    # Package available as dax-comtools.packages.${system}.dax-comtools
  };
}
```

### Nix

It is possible to build the package directly from source in your Nix shell if you have access to the
[M-labs Nix channel](https://www.m-labs.hk/artiq/manual-legacy/installing.html).

```nix
let
  pkgs = import <nixpkgs> { };
  artiqpkgs = import <artiq-full> { inherit pkgs; }; # We assume the M-labs Nix channel is already configured
  dax-comtools = pkgs.callPackage (import (builtins.fetchGit "https://gitlab.com/duke-artiq/dax-comtools.git")) { inherit pkgs artiqpkgs; };
in
pkgs.mkShell {
  buildInputs = [
    (pkgs.python3.withPackages (ps: [
      dax-comtools
    ]))
  ];
}
```

It is also possible to build the DAX comtools Nix package using the Nix scripts in the repository.

```shell
git clone https://gitlab.com/duke-artiq/dax-comtools.git
cd dax-comtools/
nix-shell
```

### Conda

For Conda users, DAX comtools can be installed using pip, but dependencies have to be installed manually using Conda and
the [M-labs Conda channel](https://www.m-labs.hk/artiq/manual-legacy/installing.html).
See the `condaDependencies` defined in [derivation.nix](./derivation.nix).

## Testing

The code in this repository can be tested by executing the following commands in the root directory of the project.

```shell
pytest  # Requires pytest to be installed
mypy    # Requires mypy to be installed
flake8  # Requires flake8 to be installed
```
