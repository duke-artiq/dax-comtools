{ pkgs ? import <nixpkgs> { }
}:
let
  dax-comtools = import ./default.nix { inherit pkgs; };
in
(pkgs.python3.withPackages (ps: [
  dax-comtools
])).env
